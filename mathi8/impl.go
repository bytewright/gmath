package mathi8

import "math"

const epsilon = 0.000001

func implSqrt(i int8) int8 {
	return int8(math.Sqrt(float64(i)))
}
