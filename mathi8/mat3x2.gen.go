package mathi8

import (
	"gitlab.com/bytewright/gmath/math32"
	"gitlab.com/bytewright/gmath/math64"
	"gitlab.com/bytewright/gmath/mathi"
	"gitlab.com/bytewright/gmath/mathi16"
	"gitlab.com/bytewright/gmath/mathi32"
	"gitlab.com/bytewright/gmath/mathi64"
	"gitlab.com/bytewright/gmath/mathui16"
	"gitlab.com/bytewright/gmath/mathui32"
)

type Mat3x2 struct {
	A00 int8
	A10 int8
	A20 int8
	A01 int8
	A11 int8
	A21 int8
}

func (v Mat3x2) Sub(o Mat3x2) Mat3x2 {
	return Mat3x2{v.A00 - o.A00, v.A10 - o.A10, v.A20 - o.A20, v.A01 - o.A01, v.A11 - o.A11, v.A21 - o.A21}
}

func (v *Mat3x2) PSub(o Mat3x2) {
	v.A00 -= o.A00
	v.A10 -= o.A10
	v.A20 -= o.A20
	v.A01 -= o.A01
	v.A11 -= o.A11
	v.A21 -= o.A21
}

func (v Mat3x2) Add(o Mat3x2) Mat3x2 {
	return Mat3x2{v.A00 + o.A00, v.A10 + o.A10, v.A20 + o.A20, v.A01 + o.A01, v.A11 + o.A11, v.A21 + o.A21}
}

func (v *Mat3x2) PAdd(o Mat3x2) {
	v.A00 += o.A00
	v.A10 += o.A10
	v.A20 += o.A20
	v.A01 += o.A01
	v.A11 += o.A11
	v.A21 += o.A21
}

func (v Mat3x2) Div(d int8) Mat3x2 {
	return Mat3x2{v.A00 / d, v.A10 / d, v.A20 / d, v.A01 / d, v.A11 / d, v.A21 / d}
}

func (v *Mat3x2) PDiv(d int8) {
	v.A00 /= d
	v.A10 /= d
	v.A20 /= d
	v.A01 /= d
	v.A11 /= d
	v.A21 /= d
}

func (v Mat3x2) Mul(m int8) Mat3x2 {
	return Mat3x2{v.A00 * m, v.A10 * m, v.A20 * m, v.A01 * m, v.A11 * m, v.A21 * m}
}

func (v *Mat3x2) PMul(m int8) {
	v.A00 *= m
	v.A10 *= m
	v.A20 *= m
	v.A01 *= m
	v.A11 *= m
	v.A21 *= m
}

func (v Mat3x2) Negative() Mat3x2 {
	return Mat3x2{-v.A00, -v.A10, -v.A20, -v.A01, -v.A11, -v.A21}
}

func (v *Mat3x2) PNegative() {
	v.A00 = -v.A00
	v.A10 = -v.A10
	v.A20 = -v.A20
	v.A01 = -v.A01
	v.A11 = -v.A11
	v.A21 = -v.A21
}

func (v Mat3x2) Abs() Mat3x2 {
	if v.A00 < 0 {
		v.A00 = -v.A00
	}
	if v.A10 < 0 {
		v.A10 = -v.A10
	}
	if v.A20 < 0 {
		v.A20 = -v.A20
	}
	if v.A01 < 0 {
		v.A01 = -v.A01
	}
	if v.A11 < 0 {
		v.A11 = -v.A11
	}
	if v.A21 < 0 {
		v.A21 = -v.A21
	}

	return v
}

func (v *Mat3x2) PAbs() {
	if v.A00 < 0 {
		v.A00 = -v.A00
	}
	if v.A10 < 0 {
		v.A10 = -v.A10
	}
	if v.A20 < 0 {
		v.A20 = -v.A20
	}
	if v.A01 < 0 {
		v.A01 = -v.A01
	}
	if v.A11 < 0 {
		v.A11 = -v.A11
	}
	if v.A21 < 0 {
		v.A21 = -v.A21
	}

}

func (v Mat3x2) Mul1(o Vec2) Vec3 {
	return Vec3{
		v.A00*o.X + v.A01*o.Y,
		v.A10*o.X + v.A11*o.Y,
		v.A20*o.X + v.A21*o.Y,
	}
}

func (v Mat3x2) Mul2(o Mat2) Mat3x2 {
	return Mat3x2{
		v.A00*o.A00 + v.A01*o.A10,
		v.A10*o.A00 + v.A11*o.A10,
		v.A20*o.A00 + v.A21*o.A10,
		v.A00*o.A01 + v.A01*o.A11,
		v.A10*o.A01 + v.A11*o.A11,
		v.A20*o.A01 + v.A21*o.A11,
	}
}

func (v Mat3x2) Mul3(o Mat2x3) Mat3 {
	return Mat3{
		v.A00*o.A00 + v.A01*o.A10,
		v.A10*o.A00 + v.A11*o.A10,
		v.A20*o.A00 + v.A21*o.A10,
		v.A00*o.A01 + v.A01*o.A11,
		v.A10*o.A01 + v.A11*o.A11,
		v.A20*o.A01 + v.A21*o.A11,
		v.A00*o.A02 + v.A01*o.A12,
		v.A10*o.A02 + v.A11*o.A12,
		v.A20*o.A02 + v.A21*o.A12,
	}
}

func (v Mat3x2) Mul4(o Mat2x4) Mat3x4 {
	return Mat3x4{
		v.A00*o.A00 + v.A01*o.A10,
		v.A10*o.A00 + v.A11*o.A10,
		v.A20*o.A00 + v.A21*o.A10,
		v.A00*o.A01 + v.A01*o.A11,
		v.A10*o.A01 + v.A11*o.A11,
		v.A20*o.A01 + v.A21*o.A11,
		v.A00*o.A02 + v.A01*o.A12,
		v.A10*o.A02 + v.A11*o.A12,
		v.A20*o.A02 + v.A21*o.A12,
		v.A00*o.A03 + v.A01*o.A13,
		v.A10*o.A03 + v.A11*o.A13,
		v.A20*o.A03 + v.A21*o.A13,
	}
}

func (v Mat3x2) Uint16() mathui16.Mat3x2 {
	return mathui16.Mat3x2{
		uint16(v.A00),
		uint16(v.A10),
		uint16(v.A20),
		uint16(v.A01),
		uint16(v.A11),
		uint16(v.A21),
	}
}

func Mat3x2FromUint16(v mathui16.Mat3x2) Mat3x2 {
	return Mat3x2{
		int8(v.A00),
		int8(v.A10),
		int8(v.A20),
		int8(v.A01),
		int8(v.A11),
		int8(v.A21),
	}
}

func (v Mat3x2) Int16() mathi16.Mat3x2 {
	return mathi16.Mat3x2{
		int16(v.A00),
		int16(v.A10),
		int16(v.A20),
		int16(v.A01),
		int16(v.A11),
		int16(v.A21),
	}
}

func Mat3x2FromInt16(v mathi16.Mat3x2) Mat3x2 {
	return Mat3x2{
		int8(v.A00),
		int8(v.A10),
		int8(v.A20),
		int8(v.A01),
		int8(v.A11),
		int8(v.A21),
	}
}

func (v Mat3x2) Uint32() mathui32.Mat3x2 {
	return mathui32.Mat3x2{
		uint32(v.A00),
		uint32(v.A10),
		uint32(v.A20),
		uint32(v.A01),
		uint32(v.A11),
		uint32(v.A21),
	}
}

func Mat3x2FromUint32(v mathui32.Mat3x2) Mat3x2 {
	return Mat3x2{
		int8(v.A00),
		int8(v.A10),
		int8(v.A20),
		int8(v.A01),
		int8(v.A11),
		int8(v.A21),
	}
}

func (v Mat3x2) Int32() mathi32.Mat3x2 {
	return mathi32.Mat3x2{
		int32(v.A00),
		int32(v.A10),
		int32(v.A20),
		int32(v.A01),
		int32(v.A11),
		int32(v.A21),
	}
}

func Mat3x2FromInt32(v mathi32.Mat3x2) Mat3x2 {
	return Mat3x2{
		int8(v.A00),
		int8(v.A10),
		int8(v.A20),
		int8(v.A01),
		int8(v.A11),
		int8(v.A21),
	}
}

func (v Mat3x2) Int() mathi.Mat3x2 {
	return mathi.Mat3x2{
		int(v.A00),
		int(v.A10),
		int(v.A20),
		int(v.A01),
		int(v.A11),
		int(v.A21),
	}
}

func Mat3x2FromInt(v mathi.Mat3x2) Mat3x2 {
	return Mat3x2{
		int8(v.A00),
		int8(v.A10),
		int8(v.A20),
		int8(v.A01),
		int8(v.A11),
		int8(v.A21),
	}
}

func (v Mat3x2) Int64() mathi64.Mat3x2 {
	return mathi64.Mat3x2{
		int64(v.A00),
		int64(v.A10),
		int64(v.A20),
		int64(v.A01),
		int64(v.A11),
		int64(v.A21),
	}
}

func Mat3x2FromInt64(v mathi64.Mat3x2) Mat3x2 {
	return Mat3x2{
		int8(v.A00),
		int8(v.A10),
		int8(v.A20),
		int8(v.A01),
		int8(v.A11),
		int8(v.A21),
	}
}

func (v Mat3x2) Float32() math32.Mat3x2 {
	return math32.Mat3x2{
		float32(v.A00),
		float32(v.A10),
		float32(v.A20),
		float32(v.A01),
		float32(v.A11),
		float32(v.A21),
	}
}

func Mat3x2FromFloat32(v math32.Mat3x2) Mat3x2 {
	return Mat3x2{
		int8(v.A00),
		int8(v.A10),
		int8(v.A20),
		int8(v.A01),
		int8(v.A11),
		int8(v.A21),
	}
}

func (v Mat3x2) Float64() math64.Mat3x2 {
	return math64.Mat3x2{
		float64(v.A00),
		float64(v.A10),
		float64(v.A20),
		float64(v.A01),
		float64(v.A11),
		float64(v.A21),
	}
}

func Mat3x2FromFloat64(v math64.Mat3x2) Mat3x2 {
	return Mat3x2{
		int8(v.A00),
		int8(v.A10),
		int8(v.A20),
		int8(v.A01),
		int8(v.A11),
		int8(v.A21),
	}
}
