package mathi16

import "math"

const epsilon = 0.000001

func implSqrt(i int16) int16 {
	return int16(math.Sqrt(float64(i)))
}
