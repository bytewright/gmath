package mathi16

import (
	"gitlab.com/bytewright/gmath/math32"
	"gitlab.com/bytewright/gmath/math64"
	"gitlab.com/bytewright/gmath/mathi"
	"gitlab.com/bytewright/gmath/mathi32"
	"gitlab.com/bytewright/gmath/mathi64"
	"gitlab.com/bytewright/gmath/mathui32"
)

type Mat4 struct {
	A00 int16
	A10 int16
	A20 int16
	A30 int16
	A01 int16
	A11 int16
	A21 int16
	A31 int16
	A02 int16
	A12 int16
	A22 int16
	A32 int16
	A03 int16
	A13 int16
	A23 int16
	A33 int16
}

func Ident4() Mat4 {
	return Mat4{1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1}
}

func (v Mat4) Sub(o Mat4) Mat4 {
	return Mat4{v.A00 - o.A00, v.A10 - o.A10, v.A20 - o.A20, v.A30 - o.A30, v.A01 - o.A01, v.A11 - o.A11, v.A21 - o.A21, v.A31 - o.A31, v.A02 - o.A02, v.A12 - o.A12, v.A22 - o.A22, v.A32 - o.A32, v.A03 - o.A03, v.A13 - o.A13, v.A23 - o.A23, v.A33 - o.A33}
}

func (v *Mat4) PSub(o Mat4) {
	v.A00 -= o.A00
	v.A10 -= o.A10
	v.A20 -= o.A20
	v.A30 -= o.A30
	v.A01 -= o.A01
	v.A11 -= o.A11
	v.A21 -= o.A21
	v.A31 -= o.A31
	v.A02 -= o.A02
	v.A12 -= o.A12
	v.A22 -= o.A22
	v.A32 -= o.A32
	v.A03 -= o.A03
	v.A13 -= o.A13
	v.A23 -= o.A23
	v.A33 -= o.A33
}

func (v Mat4) Add(o Mat4) Mat4 {
	return Mat4{v.A00 + o.A00, v.A10 + o.A10, v.A20 + o.A20, v.A30 + o.A30, v.A01 + o.A01, v.A11 + o.A11, v.A21 + o.A21, v.A31 + o.A31, v.A02 + o.A02, v.A12 + o.A12, v.A22 + o.A22, v.A32 + o.A32, v.A03 + o.A03, v.A13 + o.A13, v.A23 + o.A23, v.A33 + o.A33}
}

func (v *Mat4) PAdd(o Mat4) {
	v.A00 += o.A00
	v.A10 += o.A10
	v.A20 += o.A20
	v.A30 += o.A30
	v.A01 += o.A01
	v.A11 += o.A11
	v.A21 += o.A21
	v.A31 += o.A31
	v.A02 += o.A02
	v.A12 += o.A12
	v.A22 += o.A22
	v.A32 += o.A32
	v.A03 += o.A03
	v.A13 += o.A13
	v.A23 += o.A23
	v.A33 += o.A33
}

func (v Mat4) Div(d int16) Mat4 {
	return Mat4{v.A00 / d, v.A10 / d, v.A20 / d, v.A30 / d, v.A01 / d, v.A11 / d, v.A21 / d, v.A31 / d, v.A02 / d, v.A12 / d, v.A22 / d, v.A32 / d, v.A03 / d, v.A13 / d, v.A23 / d, v.A33 / d}
}

func (v *Mat4) PDiv(d int16) {
	v.A00 /= d
	v.A10 /= d
	v.A20 /= d
	v.A30 /= d
	v.A01 /= d
	v.A11 /= d
	v.A21 /= d
	v.A31 /= d
	v.A02 /= d
	v.A12 /= d
	v.A22 /= d
	v.A32 /= d
	v.A03 /= d
	v.A13 /= d
	v.A23 /= d
	v.A33 /= d
}

func (v Mat4) Mul(m int16) Mat4 {
	return Mat4{v.A00 * m, v.A10 * m, v.A20 * m, v.A30 * m, v.A01 * m, v.A11 * m, v.A21 * m, v.A31 * m, v.A02 * m, v.A12 * m, v.A22 * m, v.A32 * m, v.A03 * m, v.A13 * m, v.A23 * m, v.A33 * m}
}

func (v *Mat4) PMul(m int16) {
	v.A00 *= m
	v.A10 *= m
	v.A20 *= m
	v.A30 *= m
	v.A01 *= m
	v.A11 *= m
	v.A21 *= m
	v.A31 *= m
	v.A02 *= m
	v.A12 *= m
	v.A22 *= m
	v.A32 *= m
	v.A03 *= m
	v.A13 *= m
	v.A23 *= m
	v.A33 *= m
}

func (v Mat4) Negative() Mat4 {
	return Mat4{-v.A00, -v.A10, -v.A20, -v.A30, -v.A01, -v.A11, -v.A21, -v.A31, -v.A02, -v.A12, -v.A22, -v.A32, -v.A03, -v.A13, -v.A23, -v.A33}
}

func (v *Mat4) PNegative() {
	v.A00 = -v.A00
	v.A10 = -v.A10
	v.A20 = -v.A20
	v.A30 = -v.A30
	v.A01 = -v.A01
	v.A11 = -v.A11
	v.A21 = -v.A21
	v.A31 = -v.A31
	v.A02 = -v.A02
	v.A12 = -v.A12
	v.A22 = -v.A22
	v.A32 = -v.A32
	v.A03 = -v.A03
	v.A13 = -v.A13
	v.A23 = -v.A23
	v.A33 = -v.A33
}

func (v Mat4) Abs() Mat4 {
	if v.A00 < 0 {
		v.A00 = -v.A00
	}
	if v.A10 < 0 {
		v.A10 = -v.A10
	}
	if v.A20 < 0 {
		v.A20 = -v.A20
	}
	if v.A30 < 0 {
		v.A30 = -v.A30
	}
	if v.A01 < 0 {
		v.A01 = -v.A01
	}
	if v.A11 < 0 {
		v.A11 = -v.A11
	}
	if v.A21 < 0 {
		v.A21 = -v.A21
	}
	if v.A31 < 0 {
		v.A31 = -v.A31
	}
	if v.A02 < 0 {
		v.A02 = -v.A02
	}
	if v.A12 < 0 {
		v.A12 = -v.A12
	}
	if v.A22 < 0 {
		v.A22 = -v.A22
	}
	if v.A32 < 0 {
		v.A32 = -v.A32
	}
	if v.A03 < 0 {
		v.A03 = -v.A03
	}
	if v.A13 < 0 {
		v.A13 = -v.A13
	}
	if v.A23 < 0 {
		v.A23 = -v.A23
	}
	if v.A33 < 0 {
		v.A33 = -v.A33
	}

	return v
}

func (v *Mat4) PAbs() {
	if v.A00 < 0 {
		v.A00 = -v.A00
	}
	if v.A10 < 0 {
		v.A10 = -v.A10
	}
	if v.A20 < 0 {
		v.A20 = -v.A20
	}
	if v.A30 < 0 {
		v.A30 = -v.A30
	}
	if v.A01 < 0 {
		v.A01 = -v.A01
	}
	if v.A11 < 0 {
		v.A11 = -v.A11
	}
	if v.A21 < 0 {
		v.A21 = -v.A21
	}
	if v.A31 < 0 {
		v.A31 = -v.A31
	}
	if v.A02 < 0 {
		v.A02 = -v.A02
	}
	if v.A12 < 0 {
		v.A12 = -v.A12
	}
	if v.A22 < 0 {
		v.A22 = -v.A22
	}
	if v.A32 < 0 {
		v.A32 = -v.A32
	}
	if v.A03 < 0 {
		v.A03 = -v.A03
	}
	if v.A13 < 0 {
		v.A13 = -v.A13
	}
	if v.A23 < 0 {
		v.A23 = -v.A23
	}
	if v.A33 < 0 {
		v.A33 = -v.A33
	}

}

func (v Mat4) Mul1(o Vec4) Vec4 {
	return Vec4{
		v.A00*o.X + v.A01*o.Y + v.A02*o.Z + v.A03*o.W,
		v.A10*o.X + v.A11*o.Y + v.A12*o.Z + v.A13*o.W,
		v.A20*o.X + v.A21*o.Y + v.A22*o.Z + v.A23*o.W,
		v.A30*o.X + v.A31*o.Y + v.A32*o.Z + v.A33*o.W,
	}
}

func (v Mat4) Mul2(o Mat4x2) Mat4x2 {
	return Mat4x2{
		v.A00*o.A00 + v.A01*o.A10 + v.A02*o.A20 + v.A03*o.A30,
		v.A10*o.A00 + v.A11*o.A10 + v.A12*o.A20 + v.A13*o.A30,
		v.A20*o.A00 + v.A21*o.A10 + v.A22*o.A20 + v.A23*o.A30,
		v.A30*o.A00 + v.A31*o.A10 + v.A32*o.A20 + v.A33*o.A30,
		v.A00*o.A01 + v.A01*o.A11 + v.A02*o.A21 + v.A03*o.A31,
		v.A10*o.A01 + v.A11*o.A11 + v.A12*o.A21 + v.A13*o.A31,
		v.A20*o.A01 + v.A21*o.A11 + v.A22*o.A21 + v.A23*o.A31,
		v.A30*o.A01 + v.A31*o.A11 + v.A32*o.A21 + v.A33*o.A31,
	}
}

func (v Mat4) Mul3(o Mat4x3) Mat4x3 {
	return Mat4x3{
		v.A00*o.A00 + v.A01*o.A10 + v.A02*o.A20 + v.A03*o.A30,
		v.A10*o.A00 + v.A11*o.A10 + v.A12*o.A20 + v.A13*o.A30,
		v.A20*o.A00 + v.A21*o.A10 + v.A22*o.A20 + v.A23*o.A30,
		v.A30*o.A00 + v.A31*o.A10 + v.A32*o.A20 + v.A33*o.A30,
		v.A00*o.A01 + v.A01*o.A11 + v.A02*o.A21 + v.A03*o.A31,
		v.A10*o.A01 + v.A11*o.A11 + v.A12*o.A21 + v.A13*o.A31,
		v.A20*o.A01 + v.A21*o.A11 + v.A22*o.A21 + v.A23*o.A31,
		v.A30*o.A01 + v.A31*o.A11 + v.A32*o.A21 + v.A33*o.A31,
		v.A00*o.A02 + v.A01*o.A12 + v.A02*o.A22 + v.A03*o.A32,
		v.A10*o.A02 + v.A11*o.A12 + v.A12*o.A22 + v.A13*o.A32,
		v.A20*o.A02 + v.A21*o.A12 + v.A22*o.A22 + v.A23*o.A32,
		v.A30*o.A02 + v.A31*o.A12 + v.A32*o.A22 + v.A33*o.A32,
	}
}

func (v Mat4) Mul4(o Mat4) Mat4 {
	return Mat4{
		v.A00*o.A00 + v.A01*o.A10 + v.A02*o.A20 + v.A03*o.A30,
		v.A10*o.A00 + v.A11*o.A10 + v.A12*o.A20 + v.A13*o.A30,
		v.A20*o.A00 + v.A21*o.A10 + v.A22*o.A20 + v.A23*o.A30,
		v.A30*o.A00 + v.A31*o.A10 + v.A32*o.A20 + v.A33*o.A30,
		v.A00*o.A01 + v.A01*o.A11 + v.A02*o.A21 + v.A03*o.A31,
		v.A10*o.A01 + v.A11*o.A11 + v.A12*o.A21 + v.A13*o.A31,
		v.A20*o.A01 + v.A21*o.A11 + v.A22*o.A21 + v.A23*o.A31,
		v.A30*o.A01 + v.A31*o.A11 + v.A32*o.A21 + v.A33*o.A31,
		v.A00*o.A02 + v.A01*o.A12 + v.A02*o.A22 + v.A03*o.A32,
		v.A10*o.A02 + v.A11*o.A12 + v.A12*o.A22 + v.A13*o.A32,
		v.A20*o.A02 + v.A21*o.A12 + v.A22*o.A22 + v.A23*o.A32,
		v.A30*o.A02 + v.A31*o.A12 + v.A32*o.A22 + v.A33*o.A32,
		v.A00*o.A03 + v.A01*o.A13 + v.A02*o.A23 + v.A03*o.A33,
		v.A10*o.A03 + v.A11*o.A13 + v.A12*o.A23 + v.A13*o.A33,
		v.A20*o.A03 + v.A21*o.A13 + v.A22*o.A23 + v.A23*o.A33,
		v.A30*o.A03 + v.A31*o.A13 + v.A32*o.A23 + v.A33*o.A33,
	}
}

func (v Mat4) Uint32() mathui32.Mat4 {
	return mathui32.Mat4{
		uint32(v.A00),
		uint32(v.A10),
		uint32(v.A20),
		uint32(v.A30),
		uint32(v.A01),
		uint32(v.A11),
		uint32(v.A21),
		uint32(v.A31),
		uint32(v.A02),
		uint32(v.A12),
		uint32(v.A22),
		uint32(v.A32),
		uint32(v.A03),
		uint32(v.A13),
		uint32(v.A23),
		uint32(v.A33),
	}
}

func Mat4FromUint32(v mathui32.Mat4) Mat4 {
	return Mat4{
		int16(v.A00),
		int16(v.A10),
		int16(v.A20),
		int16(v.A30),
		int16(v.A01),
		int16(v.A11),
		int16(v.A21),
		int16(v.A31),
		int16(v.A02),
		int16(v.A12),
		int16(v.A22),
		int16(v.A32),
		int16(v.A03),
		int16(v.A13),
		int16(v.A23),
		int16(v.A33),
	}
}

func (v Mat4) Int32() mathi32.Mat4 {
	return mathi32.Mat4{
		int32(v.A00),
		int32(v.A10),
		int32(v.A20),
		int32(v.A30),
		int32(v.A01),
		int32(v.A11),
		int32(v.A21),
		int32(v.A31),
		int32(v.A02),
		int32(v.A12),
		int32(v.A22),
		int32(v.A32),
		int32(v.A03),
		int32(v.A13),
		int32(v.A23),
		int32(v.A33),
	}
}

func Mat4FromInt32(v mathi32.Mat4) Mat4 {
	return Mat4{
		int16(v.A00),
		int16(v.A10),
		int16(v.A20),
		int16(v.A30),
		int16(v.A01),
		int16(v.A11),
		int16(v.A21),
		int16(v.A31),
		int16(v.A02),
		int16(v.A12),
		int16(v.A22),
		int16(v.A32),
		int16(v.A03),
		int16(v.A13),
		int16(v.A23),
		int16(v.A33),
	}
}

func (v Mat4) Int() mathi.Mat4 {
	return mathi.Mat4{
		int(v.A00),
		int(v.A10),
		int(v.A20),
		int(v.A30),
		int(v.A01),
		int(v.A11),
		int(v.A21),
		int(v.A31),
		int(v.A02),
		int(v.A12),
		int(v.A22),
		int(v.A32),
		int(v.A03),
		int(v.A13),
		int(v.A23),
		int(v.A33),
	}
}

func Mat4FromInt(v mathi.Mat4) Mat4 {
	return Mat4{
		int16(v.A00),
		int16(v.A10),
		int16(v.A20),
		int16(v.A30),
		int16(v.A01),
		int16(v.A11),
		int16(v.A21),
		int16(v.A31),
		int16(v.A02),
		int16(v.A12),
		int16(v.A22),
		int16(v.A32),
		int16(v.A03),
		int16(v.A13),
		int16(v.A23),
		int16(v.A33),
	}
}

func (v Mat4) Int64() mathi64.Mat4 {
	return mathi64.Mat4{
		int64(v.A00),
		int64(v.A10),
		int64(v.A20),
		int64(v.A30),
		int64(v.A01),
		int64(v.A11),
		int64(v.A21),
		int64(v.A31),
		int64(v.A02),
		int64(v.A12),
		int64(v.A22),
		int64(v.A32),
		int64(v.A03),
		int64(v.A13),
		int64(v.A23),
		int64(v.A33),
	}
}

func Mat4FromInt64(v mathi64.Mat4) Mat4 {
	return Mat4{
		int16(v.A00),
		int16(v.A10),
		int16(v.A20),
		int16(v.A30),
		int16(v.A01),
		int16(v.A11),
		int16(v.A21),
		int16(v.A31),
		int16(v.A02),
		int16(v.A12),
		int16(v.A22),
		int16(v.A32),
		int16(v.A03),
		int16(v.A13),
		int16(v.A23),
		int16(v.A33),
	}
}

func (v Mat4) Float32() math32.Mat4 {
	return math32.Mat4{
		float32(v.A00),
		float32(v.A10),
		float32(v.A20),
		float32(v.A30),
		float32(v.A01),
		float32(v.A11),
		float32(v.A21),
		float32(v.A31),
		float32(v.A02),
		float32(v.A12),
		float32(v.A22),
		float32(v.A32),
		float32(v.A03),
		float32(v.A13),
		float32(v.A23),
		float32(v.A33),
	}
}

func Mat4FromFloat32(v math32.Mat4) Mat4 {
	return Mat4{
		int16(v.A00),
		int16(v.A10),
		int16(v.A20),
		int16(v.A30),
		int16(v.A01),
		int16(v.A11),
		int16(v.A21),
		int16(v.A31),
		int16(v.A02),
		int16(v.A12),
		int16(v.A22),
		int16(v.A32),
		int16(v.A03),
		int16(v.A13),
		int16(v.A23),
		int16(v.A33),
	}
}

func (v Mat4) Float64() math64.Mat4 {
	return math64.Mat4{
		float64(v.A00),
		float64(v.A10),
		float64(v.A20),
		float64(v.A30),
		float64(v.A01),
		float64(v.A11),
		float64(v.A21),
		float64(v.A31),
		float64(v.A02),
		float64(v.A12),
		float64(v.A22),
		float64(v.A32),
		float64(v.A03),
		float64(v.A13),
		float64(v.A23),
		float64(v.A33),
	}
}

func Mat4FromFloat64(v math64.Mat4) Mat4 {
	return Mat4{
		int16(v.A00),
		int16(v.A10),
		int16(v.A20),
		int16(v.A30),
		int16(v.A01),
		int16(v.A11),
		int16(v.A21),
		int16(v.A31),
		int16(v.A02),
		int16(v.A12),
		int16(v.A22),
		int16(v.A32),
		int16(v.A03),
		int16(v.A13),
		int16(v.A23),
		int16(v.A33),
	}
}
