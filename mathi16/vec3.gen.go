package mathi16

import (
	"gitlab.com/bytewright/gmath/math32"
	"gitlab.com/bytewright/gmath/math64"
	"gitlab.com/bytewright/gmath/mathi"
	"gitlab.com/bytewright/gmath/mathi32"
	"gitlab.com/bytewright/gmath/mathi64"
	"gitlab.com/bytewright/gmath/mathui32"
)

type Vec3 struct {
	X int16
	Y int16
	Z int16
}

func (v Vec3) Vec2() Vec2 {
	return Vec2{v.X, v.Y}
}

func (v Vec3) Vec4(w int16) Vec4 {
	return Vec4{v.X, v.Y, v.Z, w}
}

func (v Vec3) Sub(o Vec3) Vec3 {
	return Vec3{v.X - o.X, v.Y - o.Y, v.Z - o.Z}
}

func (v *Vec3) PSub(o Vec3) {
	v.X -= o.X
	v.Y -= o.Y
	v.Z -= o.Z
}

func (v Vec3) Add(o Vec3) Vec3 {
	return Vec3{v.X + o.X, v.Y + o.Y, v.Z + o.Z}
}

func (v *Vec3) PAdd(o Vec3) {
	v.X += o.X
	v.Y += o.Y
	v.Z += o.Z
}

func (v Vec3) Div(d int16) Vec3 {
	return Vec3{v.X / d, v.Y / d, v.Z / d}
}

func (v *Vec3) PDiv(d int16) {
	v.X /= d
	v.Y /= d
	v.Z /= d
}

func (v Vec3) Mul(m int16) Vec3 {
	return Vec3{v.X * m, v.Y * m, v.Z * m}
}

func (v *Vec3) PMul(m int16) {
	v.X *= m
	v.Y *= m
	v.Z *= m
}

func (v Vec3) Negative() Vec3 {
	return Vec3{-v.X, -v.Y, -v.Z}
}

func (v *Vec3) PNegative() {
	v.X = -v.X
	v.Y = -v.Y
	v.Z = -v.Z
}

func (v Vec3) Abs() Vec3 {
	if v.X < 0 {
		v.X = -v.X
	}
	if v.Y < 0 {
		v.Y = -v.Y
	}
	if v.Z < 0 {
		v.Z = -v.Z
	}

	return v
}

func (v *Vec3) PAbs() {
	if v.X < 0 {
		v.X = -v.X
	}
	if v.Y < 0 {
		v.Y = -v.Y
	}
	if v.Z < 0 {
		v.Z = -v.Z
	}

}

// Min returns the vector with the smaller value for each component
func (v Vec3) Min(o Vec3) Vec3 {
	if o.X < v.X {
		v.X = o.X
	}
	if o.Y < v.Y {
		v.Y = o.Y
	}
	if o.Z < v.Z {
		v.Z = o.Z
	}

	return v
}

// Max returns the vector with the bigger value for each component
func (v Vec3) Max(o Vec3) Vec3 {
	if o.X > v.X {
		v.X = o.X
	}
	if o.Y > v.Y {
		v.Y = o.Y
	}
	if o.Z > v.Z {
		v.Z = o.Z
	}

	return v
}

func (v Vec3) Dot(o Vec3) int16 {
	return v.X*o.X + v.Y*o.Y + v.Z*o.Z
}

func (v Vec3) LenSqr() int16 {
	return v.X*v.X + v.Y*v.Y + v.Z*v.Z
}

func (v Vec3) Len() int16 {
	return implSqrt(v.X*v.X + v.Y*v.Y + v.Z*v.Z)
}

// LenTaxiCab returns the taxicab length of the vector, sum of all components
func (v Vec3) LenTaxiCab() int16 {
	return Abs(v.X) + Abs(v.Y) + Abs(v.Z)
}

// LenChebyshev returns the chebyshev distance which is the max of all components
func (v Vec3) LenChebyshev() int16 {
	max := int16(0)

	if Abs(v.X) > max {
		max = Abs(v.X)
	}
	if Abs(v.Y) > max {
		max = Abs(v.Y)
	}
	if Abs(v.Z) > max {
		max = Abs(v.Z)
	}

	return max
}

func (v Vec3) DistanceTo(o Vec3) int16 {
	return o.Sub(v).Len()
}

func (v Vec3) TaxiCabDistanceTo(o Vec3) int16 {
	return o.Sub(v).LenTaxiCab()
}

func (v Vec3) ChebyshevDistanceTo(o Vec3) int16 {
	return o.Sub(v).LenChebyshev()
}

func (v *Vec3) PLen() int16 {
	return implSqrt(v.X*v.X + v.Y*v.Y + v.Z*v.Z)
}

func (v Vec3) Mul3(o Vec3) Vec3 {
	return Vec3{v.X * o.X, v.Y * o.Y, v.Z * o.Z}
}

func (v *Vec3) PMul3(o Vec3) {
	v.X *= o.X
	v.Y *= o.Y
	v.Z *= o.Z
}

func (v Vec3) Div3(o Vec3) Vec3 {
	return Vec3{v.X / o.X, v.Y / o.Y, v.Z / o.Z}
}

func (v *Vec3) PDiv3(o Vec3) {
	v.X /= o.X
	v.Y /= o.Y
	v.Z /= o.Z
}

func (v Vec3) Cross(o Vec3) Vec3 {
	return Vec3{v.Y*o.Z - v.Z*o.Y, v.Z*o.X - v.X*o.Z, v.X*o.Y - v.Y*o.X}
}

// ProjectOnPlane projects the vector onto the plane given by the normal n.
func (v Vec3) ProjectOnPlane(n Vec3) Vec3 {
	return v.Sub(n.Mul(n.Dot(v)))
}

func (v Vec3) Uint32() mathui32.Vec3 {
	return mathui32.Vec3{
		uint32(v.X),
		uint32(v.Y),
		uint32(v.Z),
	}
}

func Vec3FromUint32(v mathui32.Vec3) Vec3 {
	return Vec3{
		int16(v.X),
		int16(v.Y),
		int16(v.Z),
	}
}

func (v Vec3) Int32() mathi32.Vec3 {
	return mathi32.Vec3{
		int32(v.X),
		int32(v.Y),
		int32(v.Z),
	}
}

func Vec3FromInt32(v mathi32.Vec3) Vec3 {
	return Vec3{
		int16(v.X),
		int16(v.Y),
		int16(v.Z),
	}
}

func (v Vec3) Int() mathi.Vec3 {
	return mathi.Vec3{
		int(v.X),
		int(v.Y),
		int(v.Z),
	}
}

func Vec3FromInt(v mathi.Vec3) Vec3 {
	return Vec3{
		int16(v.X),
		int16(v.Y),
		int16(v.Z),
	}
}

func (v Vec3) Int64() mathi64.Vec3 {
	return mathi64.Vec3{
		int64(v.X),
		int64(v.Y),
		int64(v.Z),
	}
}

func Vec3FromInt64(v mathi64.Vec3) Vec3 {
	return Vec3{
		int16(v.X),
		int16(v.Y),
		int16(v.Z),
	}
}

func (v Vec3) Float32() math32.Vec3 {
	return math32.Vec3{
		float32(v.X),
		float32(v.Y),
		float32(v.Z),
	}
}

func Vec3FromFloat32(v math32.Vec3) Vec3 {
	return Vec3{
		int16(v.X),
		int16(v.Y),
		int16(v.Z),
	}
}

func (v Vec3) Float64() math64.Vec3 {
	return math64.Vec3{
		float64(v.X),
		float64(v.Y),
		float64(v.Z),
	}
}

func Vec3FromFloat64(v math64.Vec3) Vec3 {
	return Vec3{
		int16(v.X),
		int16(v.Y),
		int16(v.Z),
	}
}
