package mathi16

// Max returns the bigger of the two numbers
// we don't care about positive, negative zero or infinity
func Max(a, b int16) int16 {
	if a > b {
		return a
	}
	return b
}

// Min returns the smaller of the two numbers
// we don't care about positive, negative zero or infinity
func Min(a, b int16) int16 {
	if a < b {
		return a
	}
	return b
}

func Abs(a int16) int16 {
	if a < 0 {
		return -a
	}
	return a
}

// Sign returns either -1, 0, 1 depending on the sign
func Sign(a int16) int16 {
	if a < 0 {
		return -1
	}
	if a > 0 {
		return 1
	}
	return 0
}

func ModFloor(a, n int16) int16 {
	return ((a % n) + n) % n
}
