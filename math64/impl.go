package math64

import "math"

const epsilon = 0.00000000001

func implSqrt(i float64) float64 {
	return math.Sqrt(i)
}

var (
	sin = math.Sin
	cos = math.Cos
)

func floor(v float64) float64 {
	return math.Floor(v)
}

func ceil(v float64) float64 {
	return math.Ceil(v)
}

func round(v float64) float64 {
	return math.Round(v)
}
