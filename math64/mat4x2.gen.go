package math64

type Mat4x2 struct {
	A00 float64
	A10 float64
	A20 float64
	A30 float64
	A01 float64
	A11 float64
	A21 float64
	A31 float64
}

func (v Mat4x2) Sub(o Mat4x2) Mat4x2 {
	return Mat4x2{v.A00 - o.A00, v.A10 - o.A10, v.A20 - o.A20, v.A30 - o.A30, v.A01 - o.A01, v.A11 - o.A11, v.A21 - o.A21, v.A31 - o.A31}
}

func (v *Mat4x2) PSub(o Mat4x2) {
	v.A00 -= o.A00
	v.A10 -= o.A10
	v.A20 -= o.A20
	v.A30 -= o.A30
	v.A01 -= o.A01
	v.A11 -= o.A11
	v.A21 -= o.A21
	v.A31 -= o.A31
}

func (v Mat4x2) Add(o Mat4x2) Mat4x2 {
	return Mat4x2{v.A00 + o.A00, v.A10 + o.A10, v.A20 + o.A20, v.A30 + o.A30, v.A01 + o.A01, v.A11 + o.A11, v.A21 + o.A21, v.A31 + o.A31}
}

func (v *Mat4x2) PAdd(o Mat4x2) {
	v.A00 += o.A00
	v.A10 += o.A10
	v.A20 += o.A20
	v.A30 += o.A30
	v.A01 += o.A01
	v.A11 += o.A11
	v.A21 += o.A21
	v.A31 += o.A31
}

func (v Mat4x2) Div(d float64) Mat4x2 {
	return Mat4x2{v.A00 / d, v.A10 / d, v.A20 / d, v.A30 / d, v.A01 / d, v.A11 / d, v.A21 / d, v.A31 / d}
}

func (v *Mat4x2) PDiv(d float64) {
	v.A00 /= d
	v.A10 /= d
	v.A20 /= d
	v.A30 /= d
	v.A01 /= d
	v.A11 /= d
	v.A21 /= d
	v.A31 /= d
}

func (v Mat4x2) Mul(m float64) Mat4x2 {
	return Mat4x2{v.A00 * m, v.A10 * m, v.A20 * m, v.A30 * m, v.A01 * m, v.A11 * m, v.A21 * m, v.A31 * m}
}

func (v *Mat4x2) PMul(m float64) {
	v.A00 *= m
	v.A10 *= m
	v.A20 *= m
	v.A30 *= m
	v.A01 *= m
	v.A11 *= m
	v.A21 *= m
	v.A31 *= m
}

func (v Mat4x2) Negative() Mat4x2 {
	return Mat4x2{-v.A00, -v.A10, -v.A20, -v.A30, -v.A01, -v.A11, -v.A21, -v.A31}
}

func (v *Mat4x2) PNegative() {
	v.A00 = -v.A00
	v.A10 = -v.A10
	v.A20 = -v.A20
	v.A30 = -v.A30
	v.A01 = -v.A01
	v.A11 = -v.A11
	v.A21 = -v.A21
	v.A31 = -v.A31
}

func (v Mat4x2) Abs() Mat4x2 {
	if v.A00 < 0 {
		v.A00 = -v.A00
	}
	if v.A10 < 0 {
		v.A10 = -v.A10
	}
	if v.A20 < 0 {
		v.A20 = -v.A20
	}
	if v.A30 < 0 {
		v.A30 = -v.A30
	}
	if v.A01 < 0 {
		v.A01 = -v.A01
	}
	if v.A11 < 0 {
		v.A11 = -v.A11
	}
	if v.A21 < 0 {
		v.A21 = -v.A21
	}
	if v.A31 < 0 {
		v.A31 = -v.A31
	}

	return v
}

func (v *Mat4x2) PAbs() {
	if v.A00 < 0 {
		v.A00 = -v.A00
	}
	if v.A10 < 0 {
		v.A10 = -v.A10
	}
	if v.A20 < 0 {
		v.A20 = -v.A20
	}
	if v.A30 < 0 {
		v.A30 = -v.A30
	}
	if v.A01 < 0 {
		v.A01 = -v.A01
	}
	if v.A11 < 0 {
		v.A11 = -v.A11
	}
	if v.A21 < 0 {
		v.A21 = -v.A21
	}
	if v.A31 < 0 {
		v.A31 = -v.A31
	}

}

func (v Mat4x2) Mul1(o Vec2) Vec4 {
	return Vec4{
		v.A00*o.X + v.A01*o.Y,
		v.A10*o.X + v.A11*o.Y,
		v.A20*o.X + v.A21*o.Y,
		v.A30*o.X + v.A31*o.Y,
	}
}

func (v Mat4x2) Mul2(o Mat2) Mat4x2 {
	return Mat4x2{
		v.A00*o.A00 + v.A01*o.A10,
		v.A10*o.A00 + v.A11*o.A10,
		v.A20*o.A00 + v.A21*o.A10,
		v.A30*o.A00 + v.A31*o.A10,
		v.A00*o.A01 + v.A01*o.A11,
		v.A10*o.A01 + v.A11*o.A11,
		v.A20*o.A01 + v.A21*o.A11,
		v.A30*o.A01 + v.A31*o.A11,
	}
}

func (v Mat4x2) Mul3(o Mat2x3) Mat4x3 {
	return Mat4x3{
		v.A00*o.A00 + v.A01*o.A10,
		v.A10*o.A00 + v.A11*o.A10,
		v.A20*o.A00 + v.A21*o.A10,
		v.A30*o.A00 + v.A31*o.A10,
		v.A00*o.A01 + v.A01*o.A11,
		v.A10*o.A01 + v.A11*o.A11,
		v.A20*o.A01 + v.A21*o.A11,
		v.A30*o.A01 + v.A31*o.A11,
		v.A00*o.A02 + v.A01*o.A12,
		v.A10*o.A02 + v.A11*o.A12,
		v.A20*o.A02 + v.A21*o.A12,
		v.A30*o.A02 + v.A31*o.A12,
	}
}

func (v Mat4x2) Mul4(o Mat2x4) Mat4 {
	return Mat4{
		v.A00*o.A00 + v.A01*o.A10,
		v.A10*o.A00 + v.A11*o.A10,
		v.A20*o.A00 + v.A21*o.A10,
		v.A30*o.A00 + v.A31*o.A10,
		v.A00*o.A01 + v.A01*o.A11,
		v.A10*o.A01 + v.A11*o.A11,
		v.A20*o.A01 + v.A21*o.A11,
		v.A30*o.A01 + v.A31*o.A11,
		v.A00*o.A02 + v.A01*o.A12,
		v.A10*o.A02 + v.A11*o.A12,
		v.A20*o.A02 + v.A21*o.A12,
		v.A30*o.A02 + v.A31*o.A12,
		v.A00*o.A03 + v.A01*o.A13,
		v.A10*o.A03 + v.A11*o.A13,
		v.A20*o.A03 + v.A21*o.A13,
		v.A30*o.A03 + v.A31*o.A13,
	}
}

func (v Mat4x2) Floor() Mat4x2 {
	return Mat4x2{floor(v.A00), floor(v.A10), floor(v.A20), floor(v.A30), floor(v.A01), floor(v.A11), floor(v.A21), floor(v.A31)}
}
func (v *Mat4x2) PFloor() {
	v.A00 = floor(v.A00)
	v.A10 = floor(v.A10)
	v.A20 = floor(v.A20)
	v.A30 = floor(v.A30)
	v.A01 = floor(v.A01)
	v.A11 = floor(v.A11)
	v.A21 = floor(v.A21)
	v.A31 = floor(v.A31)
}

func (v Mat4x2) Ceil() Mat4x2 {
	return Mat4x2{ceil(v.A00), ceil(v.A10), ceil(v.A20), ceil(v.A30), ceil(v.A01), ceil(v.A11), ceil(v.A21), ceil(v.A31)}
}
func (v *Mat4x2) PCeil() {
	v.A00 = ceil(v.A00)
	v.A10 = ceil(v.A10)
	v.A20 = ceil(v.A20)
	v.A30 = ceil(v.A30)
	v.A01 = ceil(v.A01)
	v.A11 = ceil(v.A11)
	v.A21 = ceil(v.A21)
	v.A31 = ceil(v.A31)
}

func (v Mat4x2) Round() Mat4x2 {
	return Mat4x2{round(v.A00), round(v.A10), round(v.A20), round(v.A30), round(v.A01), round(v.A11), round(v.A21), round(v.A31)}
}
func (v *Mat4x2) PRound() {
	v.A00 = round(v.A00)
	v.A10 = round(v.A10)
	v.A20 = round(v.A20)
	v.A30 = round(v.A30)
	v.A01 = round(v.A01)
	v.A11 = round(v.A11)
	v.A21 = round(v.A21)
	v.A31 = round(v.A31)
}

func (v Mat4x2) Lerp(w Mat4x2, factor float64) Mat4x2 {
	return v.Mul(1 - factor).Add(w.Mul(factor))
}
