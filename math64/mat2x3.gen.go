package math64

type Mat2x3 struct {
	A00 float64
	A10 float64
	A01 float64
	A11 float64
	A02 float64
	A12 float64
}

func (v Mat2x3) Sub(o Mat2x3) Mat2x3 {
	return Mat2x3{v.A00 - o.A00, v.A10 - o.A10, v.A01 - o.A01, v.A11 - o.A11, v.A02 - o.A02, v.A12 - o.A12}
}

func (v *Mat2x3) PSub(o Mat2x3) {
	v.A00 -= o.A00
	v.A10 -= o.A10
	v.A01 -= o.A01
	v.A11 -= o.A11
	v.A02 -= o.A02
	v.A12 -= o.A12
}

func (v Mat2x3) Add(o Mat2x3) Mat2x3 {
	return Mat2x3{v.A00 + o.A00, v.A10 + o.A10, v.A01 + o.A01, v.A11 + o.A11, v.A02 + o.A02, v.A12 + o.A12}
}

func (v *Mat2x3) PAdd(o Mat2x3) {
	v.A00 += o.A00
	v.A10 += o.A10
	v.A01 += o.A01
	v.A11 += o.A11
	v.A02 += o.A02
	v.A12 += o.A12
}

func (v Mat2x3) Div(d float64) Mat2x3 {
	return Mat2x3{v.A00 / d, v.A10 / d, v.A01 / d, v.A11 / d, v.A02 / d, v.A12 / d}
}

func (v *Mat2x3) PDiv(d float64) {
	v.A00 /= d
	v.A10 /= d
	v.A01 /= d
	v.A11 /= d
	v.A02 /= d
	v.A12 /= d
}

func (v Mat2x3) Mul(m float64) Mat2x3 {
	return Mat2x3{v.A00 * m, v.A10 * m, v.A01 * m, v.A11 * m, v.A02 * m, v.A12 * m}
}

func (v *Mat2x3) PMul(m float64) {
	v.A00 *= m
	v.A10 *= m
	v.A01 *= m
	v.A11 *= m
	v.A02 *= m
	v.A12 *= m
}

func (v Mat2x3) Negative() Mat2x3 {
	return Mat2x3{-v.A00, -v.A10, -v.A01, -v.A11, -v.A02, -v.A12}
}

func (v *Mat2x3) PNegative() {
	v.A00 = -v.A00
	v.A10 = -v.A10
	v.A01 = -v.A01
	v.A11 = -v.A11
	v.A02 = -v.A02
	v.A12 = -v.A12
}

func (v Mat2x3) Abs() Mat2x3 {
	if v.A00 < 0 {
		v.A00 = -v.A00
	}
	if v.A10 < 0 {
		v.A10 = -v.A10
	}
	if v.A01 < 0 {
		v.A01 = -v.A01
	}
	if v.A11 < 0 {
		v.A11 = -v.A11
	}
	if v.A02 < 0 {
		v.A02 = -v.A02
	}
	if v.A12 < 0 {
		v.A12 = -v.A12
	}

	return v
}

func (v *Mat2x3) PAbs() {
	if v.A00 < 0 {
		v.A00 = -v.A00
	}
	if v.A10 < 0 {
		v.A10 = -v.A10
	}
	if v.A01 < 0 {
		v.A01 = -v.A01
	}
	if v.A11 < 0 {
		v.A11 = -v.A11
	}
	if v.A02 < 0 {
		v.A02 = -v.A02
	}
	if v.A12 < 0 {
		v.A12 = -v.A12
	}

}

func (v Mat2x3) Mul1(o Vec3) Vec2 {
	return Vec2{
		v.A00*o.X + v.A01*o.Y + v.A02*o.Z,
		v.A10*o.X + v.A11*o.Y + v.A12*o.Z,
	}
}

func (v Mat2x3) Mul2(o Mat3x2) Mat2 {
	return Mat2{
		v.A00*o.A00 + v.A01*o.A10 + v.A02*o.A20,
		v.A10*o.A00 + v.A11*o.A10 + v.A12*o.A20,
		v.A00*o.A01 + v.A01*o.A11 + v.A02*o.A21,
		v.A10*o.A01 + v.A11*o.A11 + v.A12*o.A21,
	}
}

func (v Mat2x3) Mul3(o Mat3) Mat2x3 {
	return Mat2x3{
		v.A00*o.A00 + v.A01*o.A10 + v.A02*o.A20,
		v.A10*o.A00 + v.A11*o.A10 + v.A12*o.A20,
		v.A00*o.A01 + v.A01*o.A11 + v.A02*o.A21,
		v.A10*o.A01 + v.A11*o.A11 + v.A12*o.A21,
		v.A00*o.A02 + v.A01*o.A12 + v.A02*o.A22,
		v.A10*o.A02 + v.A11*o.A12 + v.A12*o.A22,
	}
}

func (v Mat2x3) Mul4(o Mat3x4) Mat2x4 {
	return Mat2x4{
		v.A00*o.A00 + v.A01*o.A10 + v.A02*o.A20,
		v.A10*o.A00 + v.A11*o.A10 + v.A12*o.A20,
		v.A00*o.A01 + v.A01*o.A11 + v.A02*o.A21,
		v.A10*o.A01 + v.A11*o.A11 + v.A12*o.A21,
		v.A00*o.A02 + v.A01*o.A12 + v.A02*o.A22,
		v.A10*o.A02 + v.A11*o.A12 + v.A12*o.A22,
		v.A00*o.A03 + v.A01*o.A13 + v.A02*o.A23,
		v.A10*o.A03 + v.A11*o.A13 + v.A12*o.A23,
	}
}

func (v Mat2x3) Floor() Mat2x3 {
	return Mat2x3{floor(v.A00), floor(v.A10), floor(v.A01), floor(v.A11), floor(v.A02), floor(v.A12)}
}
func (v *Mat2x3) PFloor() {
	v.A00 = floor(v.A00)
	v.A10 = floor(v.A10)
	v.A01 = floor(v.A01)
	v.A11 = floor(v.A11)
	v.A02 = floor(v.A02)
	v.A12 = floor(v.A12)
}

func (v Mat2x3) Ceil() Mat2x3 {
	return Mat2x3{ceil(v.A00), ceil(v.A10), ceil(v.A01), ceil(v.A11), ceil(v.A02), ceil(v.A12)}
}
func (v *Mat2x3) PCeil() {
	v.A00 = ceil(v.A00)
	v.A10 = ceil(v.A10)
	v.A01 = ceil(v.A01)
	v.A11 = ceil(v.A11)
	v.A02 = ceil(v.A02)
	v.A12 = ceil(v.A12)
}

func (v Mat2x3) Round() Mat2x3 {
	return Mat2x3{round(v.A00), round(v.A10), round(v.A01), round(v.A11), round(v.A02), round(v.A12)}
}
func (v *Mat2x3) PRound() {
	v.A00 = round(v.A00)
	v.A10 = round(v.A10)
	v.A01 = round(v.A01)
	v.A11 = round(v.A11)
	v.A02 = round(v.A02)
	v.A12 = round(v.A12)
}

func (v Mat2x3) Lerp(w Mat2x3, factor float64) Mat2x3 {
	return v.Mul(1 - factor).Add(w.Mul(factor))
}
