package math64

import "math"

type Segment2 struct {
	From Vec2
	To   Vec2
}

// NewSegment creates a new segment
func NewSegment2(from, to Vec2) Segment2 {
	return Segment2{From: from, To: to}
}

func (r Segment2) Center() Vec2 {
	return r.From.Add(r.To).Div(2)
}

func (r Segment2) Vec() Vec2 {
	return r.To.Sub(r.From)
}

func (r Segment2) Distance(p Vec2) float64 {
	ab := r.To.Sub(r.From)
	ap := p.Sub(r.From)

	t := ap.Dot(ab) / ab.Dot(ab)

	var closest Vec2
	if t < 0.0 {
		closest = r.From
	} else if t > 1.0 {
		closest = r.To
	} else {
		closest = r.From.Add(ab.Mul(t))
	}

	return p.DistanceTo(closest)
}

// ClosestVec returns the position closest to p
// that lies somewhere on the segment.
func (r Segment2) ClosestVec(p Vec2) Vec2 {
	ab := r.To.Sub(r.From)
	ap := p.Sub(r.From)

	t := ap.Dot(ab) / ab.Dot(ab)

	if t < 0.0 {
		return r.From
	}
	if t > 1.0 {
		return r.To
	}

	return r.From.Add(ab.Mul(t))
}

func (r Segment2) Reverse() Segment2 {
	r.To, r.From = r.From, r.To

	return r
}

func (r Segment2) IntersectsBox(b Box2) (Vec2, bool) {
	pos, i := r.IntersectsPath(b.Path().Close())
	if i == -1 {
		return Vec2{}, false
	}
	return pos, true
}

// IntersectsPath returns position and index of intersection or -1 if there is no intersection
// it's treated as a an open path
func (seg Segment2) IntersectsPath(p Path2) (Vec2, int) {
	for i := 0; i < len(p)-1; i++ {
		segS := Segment2{p[i], p[i+1]}
		ivec, does := segS.Intersects(seg)
		if does {
			return ivec, i
		}
	}
	return Vec2{}, -1
}

func (seg Segment2) CountPathIntersections(p Path2) int {
	c := 0

	for i := 0; i < len(p)-1; i++ {
		segS := Segment2{p[i], p[i+1]}
		_, does := segS.Intersects(seg)
		if does {
			c++
		}
	}

	return c
}

// Intersects returns the intersection point of two line segments
// if there is no intersection, second param will be false
// from http://paulbourke.net/geometry/pointlineplane/
func (a Segment2) Intersects(b Segment2) (Vec2, bool) {
	x1 := a.From.X
	y1 := a.From.Y
	x2 := a.To.X
	y2 := a.To.Y
	x3 := b.From.X
	y3 := b.From.Y
	x4 := b.To.X
	y4 := b.To.Y

	y4my3 := y4 - y3
	x2mx1 := x2 - x1
	x4mx3 := x4 - x3
	y2my1 := y2 - y1

	denom := (y4my3)*(x2mx1) - (x4mx3)*(y2my1)

	// lines are parallel (including coincident, which means on top of each other)
	if denom*denom < epsilon {
		return Vec2{}, false
	}

	y1my3 := y1 - y3
	x1mx3 := x1 - x3

	numera := (x4mx3)*(y1my3) - (y4my3)*(x1mx3)
	numerb := (x2mx1)*(y1my3) - (y2my1)*(x1mx3)
	mua := numera / denom
	mub := numerb / denom
	// lines intersect outside segment
	if mua < 0 || mua > 1 || mub < 0 || mub > 1 {
		return Vec2{}, false
	}
	return Vec2{
		x1 + mua*(x2-x1),
		y1 + mua*(y2-y1),
	}, true
}

func (a Segment2) IntersectsCircle(c Vec2, radius float64) (int, Vec2, Vec2) {
	// float A, B, C, det, t;

	d := a.Vec()

	A := d.LenSqr()

	bVec := a.From.Sub(c).Mul2(d)
	B := 2 * (bVec.X + bVec.Y)
	// B = 2 * (dx * (point1.X - cx) + dy * (point1.Y - cy));

	C := a.From.Sub(c).LenSqr() - radius*radius
	// C = (point1.X - cx) * (point1.X - cx) + (point1.Y - cy) * (point1.Y - cy) - radius * radius;

	det := B*B - 4*A*C
	if A <= 0.0000001 || det < 0 {
		return 0, Vec2{}, Vec2{}

	} else if det == 0 {
		t := -B / (2 * A)
		if t >= 0 && t <= 1 {
			return 1, a.From.Add(d.Mul(t)), Vec2{}
		}

	} else {
		detSqrt := float64(math.Sqrt(float64(det)))
		t1 := (-B + detSqrt) / (2 * A)
		t2 := (-B - detSqrt) / (2 * A)

		t1v := t1 >= 0 && t1 <= 1
		t2v := t2 >= 0 && t2 <= 1

		if t1v && t2v {
			return 2, a.From.Add(d.Mul(t1)), a.From.Add(d.Mul(t2))
		}
		if t1v {
			return 1, a.From.Add(d.Mul(t1)), Vec2{}
		}
		if t2v {
			return 1, a.From.Add(d.Mul(t2)), Vec2{}
		}
	}
	return 0, Vec2{}, Vec2{}
}
