package math64

type Box3 struct {
	From Vec3
	To   Vec3
}

// NewBox creates a new Orthothing in a normalized form,
// diagonals component always point up and right.
func NewBox3(from, to Vec3) Box3 {
	r := Box3{From: from, To: to}
	(&r).normalize()
	return r
}

func NewBox3FromSize(origin, size Vec3) Box3 {
	r := Box3{From: origin, To: origin.Add(size)}
	(&r).normalize()
	return r
}

func (r Box3) Center() Vec3 {
	return r.From.Add(r.To).Div(2)
}

func (r Box3) Size() Vec3 {
	return r.To.Sub(r.From).Abs()
}

func (r Box3) Translate(v Vec3) Box3 {
	return Box3{
		From: r.From.Add(v),
		To:   r.To.Add(v),
	}
}

// Contains checks if vec is inside, >= from and < to
func (r Box3) Contains(v Vec3) bool {
	r.normalize()
	return v.X >= r.From.X && v.X < r.To.X &&
		v.Y >= r.From.Y && v.Y < r.To.Y &&
		v.Z >= r.From.Z && v.Z < r.To.Z
}

// ContainsInclusive checks if vec is inside, >= from and <= to
func (r Box3) ContainsInclusive(v Vec3) bool {
	r.normalize()
	return v.X >= r.From.X && v.X <= r.To.X &&
		v.Y >= r.From.Y && v.Y <= r.To.Y &&
		v.Z >= r.From.Z && v.Z <= r.To.Z
}

func (a Box3) Union(b Box3) Box3 {
	return Box3{
		From: a.From.Min(b.From),
		To:   a.To.Max(b.To),
	}
}

func (a Box3) Extend(v Vec3) Box3 {
	return Box3{
		From: a.From.Min(v),
		To:   a.To.Max(v),
	}
}

func (a Box3) Grow(v float64) Box3 {
	a.normalize()

	a.From.X -= v
	a.To.X += v
	a.From.Y -= v
	a.To.Y += v
	a.From.Z -= v
	a.To.Z += v
	return a
}

// Intersects checks if the two boxes overlap, they must both be normalized
func (a Box3) Intersects(b Box3) bool {
	return !(a.To.X <= b.From.X ||
		a.From.X >= b.To.X ||
		a.To.Y <= b.From.Y ||
		a.From.Y >= b.To.Y ||
		a.To.Z <= b.From.Z ||
		a.From.Z >= b.To.Z)
}

// Normalized returns the Box where the diagonal points up in each axis
func (r Box3) Normalized() Box3 {
	r.normalize()
	return r
}

// DistanceToVec gets the distance from the box' edges to the point.
// If the point is inside it returns 0
func (r Box3) DistanceToVec(point Vec3) float64 {
	over := point.Sub(r.To)
	under := r.From.Sub(point)
	return over.Max(under).Max(Vec3{}).Len()
}

// normalize changes the diagonal of the Box to point up in each axis
func (r *Box3) normalize() {
	if r.From.X > r.To.X {
		r.From.X, r.To.X = r.To.X, r.From.X
	}
	if r.From.Y > r.To.Y {
		r.From.Y, r.To.Y = r.To.Y, r.From.Y
	}
	if r.From.Z > r.To.Z {
		r.From.Z, r.To.Z = r.To.Z, r.From.Z
	}
}
