package math64

type Mat3x2 struct {
	A00 float64
	A10 float64
	A20 float64
	A01 float64
	A11 float64
	A21 float64
}

func (v Mat3x2) Sub(o Mat3x2) Mat3x2 {
	return Mat3x2{v.A00 - o.A00, v.A10 - o.A10, v.A20 - o.A20, v.A01 - o.A01, v.A11 - o.A11, v.A21 - o.A21}
}

func (v *Mat3x2) PSub(o Mat3x2) {
	v.A00 -= o.A00
	v.A10 -= o.A10
	v.A20 -= o.A20
	v.A01 -= o.A01
	v.A11 -= o.A11
	v.A21 -= o.A21
}

func (v Mat3x2) Add(o Mat3x2) Mat3x2 {
	return Mat3x2{v.A00 + o.A00, v.A10 + o.A10, v.A20 + o.A20, v.A01 + o.A01, v.A11 + o.A11, v.A21 + o.A21}
}

func (v *Mat3x2) PAdd(o Mat3x2) {
	v.A00 += o.A00
	v.A10 += o.A10
	v.A20 += o.A20
	v.A01 += o.A01
	v.A11 += o.A11
	v.A21 += o.A21
}

func (v Mat3x2) Div(d float64) Mat3x2 {
	return Mat3x2{v.A00 / d, v.A10 / d, v.A20 / d, v.A01 / d, v.A11 / d, v.A21 / d}
}

func (v *Mat3x2) PDiv(d float64) {
	v.A00 /= d
	v.A10 /= d
	v.A20 /= d
	v.A01 /= d
	v.A11 /= d
	v.A21 /= d
}

func (v Mat3x2) Mul(m float64) Mat3x2 {
	return Mat3x2{v.A00 * m, v.A10 * m, v.A20 * m, v.A01 * m, v.A11 * m, v.A21 * m}
}

func (v *Mat3x2) PMul(m float64) {
	v.A00 *= m
	v.A10 *= m
	v.A20 *= m
	v.A01 *= m
	v.A11 *= m
	v.A21 *= m
}

func (v Mat3x2) Negative() Mat3x2 {
	return Mat3x2{-v.A00, -v.A10, -v.A20, -v.A01, -v.A11, -v.A21}
}

func (v *Mat3x2) PNegative() {
	v.A00 = -v.A00
	v.A10 = -v.A10
	v.A20 = -v.A20
	v.A01 = -v.A01
	v.A11 = -v.A11
	v.A21 = -v.A21
}

func (v Mat3x2) Abs() Mat3x2 {
	if v.A00 < 0 {
		v.A00 = -v.A00
	}
	if v.A10 < 0 {
		v.A10 = -v.A10
	}
	if v.A20 < 0 {
		v.A20 = -v.A20
	}
	if v.A01 < 0 {
		v.A01 = -v.A01
	}
	if v.A11 < 0 {
		v.A11 = -v.A11
	}
	if v.A21 < 0 {
		v.A21 = -v.A21
	}

	return v
}

func (v *Mat3x2) PAbs() {
	if v.A00 < 0 {
		v.A00 = -v.A00
	}
	if v.A10 < 0 {
		v.A10 = -v.A10
	}
	if v.A20 < 0 {
		v.A20 = -v.A20
	}
	if v.A01 < 0 {
		v.A01 = -v.A01
	}
	if v.A11 < 0 {
		v.A11 = -v.A11
	}
	if v.A21 < 0 {
		v.A21 = -v.A21
	}

}

func (v Mat3x2) Mul1(o Vec2) Vec3 {
	return Vec3{
		v.A00*o.X + v.A01*o.Y,
		v.A10*o.X + v.A11*o.Y,
		v.A20*o.X + v.A21*o.Y,
	}
}

func (v Mat3x2) Mul2(o Mat2) Mat3x2 {
	return Mat3x2{
		v.A00*o.A00 + v.A01*o.A10,
		v.A10*o.A00 + v.A11*o.A10,
		v.A20*o.A00 + v.A21*o.A10,
		v.A00*o.A01 + v.A01*o.A11,
		v.A10*o.A01 + v.A11*o.A11,
		v.A20*o.A01 + v.A21*o.A11,
	}
}

func (v Mat3x2) Mul3(o Mat2x3) Mat3 {
	return Mat3{
		v.A00*o.A00 + v.A01*o.A10,
		v.A10*o.A00 + v.A11*o.A10,
		v.A20*o.A00 + v.A21*o.A10,
		v.A00*o.A01 + v.A01*o.A11,
		v.A10*o.A01 + v.A11*o.A11,
		v.A20*o.A01 + v.A21*o.A11,
		v.A00*o.A02 + v.A01*o.A12,
		v.A10*o.A02 + v.A11*o.A12,
		v.A20*o.A02 + v.A21*o.A12,
	}
}

func (v Mat3x2) Mul4(o Mat2x4) Mat3x4 {
	return Mat3x4{
		v.A00*o.A00 + v.A01*o.A10,
		v.A10*o.A00 + v.A11*o.A10,
		v.A20*o.A00 + v.A21*o.A10,
		v.A00*o.A01 + v.A01*o.A11,
		v.A10*o.A01 + v.A11*o.A11,
		v.A20*o.A01 + v.A21*o.A11,
		v.A00*o.A02 + v.A01*o.A12,
		v.A10*o.A02 + v.A11*o.A12,
		v.A20*o.A02 + v.A21*o.A12,
		v.A00*o.A03 + v.A01*o.A13,
		v.A10*o.A03 + v.A11*o.A13,
		v.A20*o.A03 + v.A21*o.A13,
	}
}

func (v Mat3x2) Floor() Mat3x2 {
	return Mat3x2{floor(v.A00), floor(v.A10), floor(v.A20), floor(v.A01), floor(v.A11), floor(v.A21)}
}
func (v *Mat3x2) PFloor() {
	v.A00 = floor(v.A00)
	v.A10 = floor(v.A10)
	v.A20 = floor(v.A20)
	v.A01 = floor(v.A01)
	v.A11 = floor(v.A11)
	v.A21 = floor(v.A21)
}

func (v Mat3x2) Ceil() Mat3x2 {
	return Mat3x2{ceil(v.A00), ceil(v.A10), ceil(v.A20), ceil(v.A01), ceil(v.A11), ceil(v.A21)}
}
func (v *Mat3x2) PCeil() {
	v.A00 = ceil(v.A00)
	v.A10 = ceil(v.A10)
	v.A20 = ceil(v.A20)
	v.A01 = ceil(v.A01)
	v.A11 = ceil(v.A11)
	v.A21 = ceil(v.A21)
}

func (v Mat3x2) Round() Mat3x2 {
	return Mat3x2{round(v.A00), round(v.A10), round(v.A20), round(v.A01), round(v.A11), round(v.A21)}
}
func (v *Mat3x2) PRound() {
	v.A00 = round(v.A00)
	v.A10 = round(v.A10)
	v.A20 = round(v.A20)
	v.A01 = round(v.A01)
	v.A11 = round(v.A11)
	v.A21 = round(v.A21)
}

func (v Mat3x2) Lerp(w Mat3x2, factor float64) Mat3x2 {
	return v.Mul(1 - factor).Add(w.Mul(factor))
}
