package math64

type Vec3 struct {
	X float64
	Y float64
	Z float64
}

func (v Vec3) Vec2() Vec2 {
	return Vec2{v.X, v.Y}
}

func (v Vec3) Vec4(w float64) Vec4 {
	return Vec4{v.X, v.Y, v.Z, w}
}

func (v Vec3) Sub(o Vec3) Vec3 {
	return Vec3{v.X - o.X, v.Y - o.Y, v.Z - o.Z}
}

func (v *Vec3) PSub(o Vec3) {
	v.X -= o.X
	v.Y -= o.Y
	v.Z -= o.Z
}

func (v Vec3) Add(o Vec3) Vec3 {
	return Vec3{v.X + o.X, v.Y + o.Y, v.Z + o.Z}
}

func (v *Vec3) PAdd(o Vec3) {
	v.X += o.X
	v.Y += o.Y
	v.Z += o.Z
}

func (v Vec3) Div(d float64) Vec3 {
	return Vec3{v.X / d, v.Y / d, v.Z / d}
}

func (v *Vec3) PDiv(d float64) {
	v.X /= d
	v.Y /= d
	v.Z /= d
}

func (v Vec3) Mul(m float64) Vec3 {
	return Vec3{v.X * m, v.Y * m, v.Z * m}
}

func (v *Vec3) PMul(m float64) {
	v.X *= m
	v.Y *= m
	v.Z *= m
}

func (v Vec3) Negative() Vec3 {
	return Vec3{-v.X, -v.Y, -v.Z}
}

func (v *Vec3) PNegative() {
	v.X = -v.X
	v.Y = -v.Y
	v.Z = -v.Z
}

func (v Vec3) Abs() Vec3 {
	if v.X < 0 {
		v.X = -v.X
	}
	if v.Y < 0 {
		v.Y = -v.Y
	}
	if v.Z < 0 {
		v.Z = -v.Z
	}

	return v
}

func (v *Vec3) PAbs() {
	if v.X < 0 {
		v.X = -v.X
	}
	if v.Y < 0 {
		v.Y = -v.Y
	}
	if v.Z < 0 {
		v.Z = -v.Z
	}

}

func (v Vec3) Floor() Vec3 {
	return Vec3{floor(v.X), floor(v.Y), floor(v.Z)}
}
func (v *Vec3) PFloor() {
	v.X = floor(v.X)
	v.Y = floor(v.Y)
	v.Z = floor(v.Z)
}

func (v Vec3) Ceil() Vec3 {
	return Vec3{ceil(v.X), ceil(v.Y), ceil(v.Z)}
}
func (v *Vec3) PCeil() {
	v.X = ceil(v.X)
	v.Y = ceil(v.Y)
	v.Z = ceil(v.Z)
}

func (v Vec3) Round() Vec3 {
	return Vec3{round(v.X), round(v.Y), round(v.Z)}
}
func (v *Vec3) PRound() {
	v.X = round(v.X)
	v.Y = round(v.Y)
	v.Z = round(v.Z)
}

func (v Vec3) Lerp(w Vec3, factor float64) Vec3 {
	return v.Mul(1 - factor).Add(w.Mul(factor))
}

// Min returns the vector with the smaller value for each component
func (v Vec3) Min(o Vec3) Vec3 {
	if o.X < v.X {
		v.X = o.X
	}
	if o.Y < v.Y {
		v.Y = o.Y
	}
	if o.Z < v.Z {
		v.Z = o.Z
	}

	return v
}

// Max returns the vector with the bigger value for each component
func (v Vec3) Max(o Vec3) Vec3 {
	if o.X > v.X {
		v.X = o.X
	}
	if o.Y > v.Y {
		v.Y = o.Y
	}
	if o.Z > v.Z {
		v.Z = o.Z
	}

	return v
}

func (v Vec3) Dot(o Vec3) float64 {
	return v.X*o.X + v.Y*o.Y + v.Z*o.Z
}

func (v Vec3) LenSqr() float64 {
	return v.X*v.X + v.Y*v.Y + v.Z*v.Z
}

func (v Vec3) Len() float64 {
	return implSqrt(v.X*v.X + v.Y*v.Y + v.Z*v.Z)
}

// LenTaxiCab returns the taxicab length of the vector, sum of all components
func (v Vec3) LenTaxiCab() float64 {
	return Abs(v.X) + Abs(v.Y) + Abs(v.Z)
}

// LenChebyshev returns the chebyshev distance which is the max of all components
func (v Vec3) LenChebyshev() float64 {
	max := float64(0)

	if Abs(v.X) > max {
		max = Abs(v.X)
	}
	if Abs(v.Y) > max {
		max = Abs(v.Y)
	}
	if Abs(v.Z) > max {
		max = Abs(v.Z)
	}

	return max
}

func (v Vec3) DistanceTo(o Vec3) float64 {
	return o.Sub(v).Len()
}

func (v Vec3) TaxiCabDistanceTo(o Vec3) float64 {
	return o.Sub(v).LenTaxiCab()
}

func (v Vec3) ChebyshevDistanceTo(o Vec3) float64 {
	return o.Sub(v).LenChebyshev()
}

func (v *Vec3) PLen() float64 {
	return implSqrt(v.X*v.X + v.Y*v.Y + v.Z*v.Z)
}

func (v Vec3) Normalize() Vec3 {
	l := 1.0 / v.Len()
	return Vec3{v.X * l, v.Y * l, v.Z * l}
}
func (v *Vec3) PNormalize() {
	l := 1.0 / v.PLen()
	v.X *= l
	v.Y *= l
	v.Z *= l
}

func (v Vec3) Mul3(o Vec3) Vec3 {
	return Vec3{v.X * o.X, v.Y * o.Y, v.Z * o.Z}
}

func (v *Vec3) PMul3(o Vec3) {
	v.X *= o.X
	v.Y *= o.Y
	v.Z *= o.Z
}

func (v Vec3) Div3(o Vec3) Vec3 {
	return Vec3{v.X / o.X, v.Y / o.Y, v.Z / o.Z}
}

func (v *Vec3) PDiv3(o Vec3) {
	v.X /= o.X
	v.Y /= o.Y
	v.Z /= o.Z
}

func (v Vec3) Cross(o Vec3) Vec3 {
	return Vec3{v.Y*o.Z - v.Z*o.Y, v.Z*o.X - v.X*o.Z, v.X*o.Y - v.Y*o.X}
}

// ProjectOnPlane projects the vector onto the plane given by the normal n.
func (v Vec3) ProjectOnPlane(n Vec3) Vec3 {
	return v.Sub(n.Mul(n.Dot(v)))
}
