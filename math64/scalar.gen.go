package math64

import "math"

// Max returns the bigger of the two numbers
// we don't care about positive, negative zero or infinity
func Max(a, b float64) float64 {
	if a != a { //  a is NaN
		return a
	}
	if b != b { //  b is NaN
		return b
	}
	if a > b {
		return a
	}
	return b
}

// Min returns the smaller of the two numbers
// we don't care about positive, negative zero or infinity
func Min(a, b float64) float64 {
	if a != a { //  a is NaN
		return a
	}
	if b != b { //  b is NaN
		return b
	}
	if a < b {
		return a
	}
	return b
}

func Abs(a float64) float64 {
	if a < 0 {
		return -a
	}
	return a
}

// Sign returns either -1, 0, 1 depending on the sign
func Sign(a float64) float64 {
	if a < 0 {
		return -1
	}
	if a > 0 {
		return 1
	}
	return 0
}

// SignBit returns true of the float is negative or negative zero
func SignBit(x float64) bool {
	return math.Float64bits(x)&(1<<63) != 0
}
