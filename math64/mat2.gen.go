package math64

type Mat2 struct {
	A00 float64
	A10 float64
	A01 float64
	A11 float64
}

func Ident2() Mat2 {
	return Mat2{1, 0, 0, 1}
}

func (v Mat2) Sub(o Mat2) Mat2 {
	return Mat2{v.A00 - o.A00, v.A10 - o.A10, v.A01 - o.A01, v.A11 - o.A11}
}

func (v *Mat2) PSub(o Mat2) {
	v.A00 -= o.A00
	v.A10 -= o.A10
	v.A01 -= o.A01
	v.A11 -= o.A11
}

func (v Mat2) Add(o Mat2) Mat2 {
	return Mat2{v.A00 + o.A00, v.A10 + o.A10, v.A01 + o.A01, v.A11 + o.A11}
}

func (v *Mat2) PAdd(o Mat2) {
	v.A00 += o.A00
	v.A10 += o.A10
	v.A01 += o.A01
	v.A11 += o.A11
}

func (v Mat2) Div(d float64) Mat2 {
	return Mat2{v.A00 / d, v.A10 / d, v.A01 / d, v.A11 / d}
}

func (v *Mat2) PDiv(d float64) {
	v.A00 /= d
	v.A10 /= d
	v.A01 /= d
	v.A11 /= d
}

func (v Mat2) Mul(m float64) Mat2 {
	return Mat2{v.A00 * m, v.A10 * m, v.A01 * m, v.A11 * m}
}

func (v *Mat2) PMul(m float64) {
	v.A00 *= m
	v.A10 *= m
	v.A01 *= m
	v.A11 *= m
}

func (v Mat2) Negative() Mat2 {
	return Mat2{-v.A00, -v.A10, -v.A01, -v.A11}
}

func (v *Mat2) PNegative() {
	v.A00 = -v.A00
	v.A10 = -v.A10
	v.A01 = -v.A01
	v.A11 = -v.A11
}

func (v Mat2) Abs() Mat2 {
	if v.A00 < 0 {
		v.A00 = -v.A00
	}
	if v.A10 < 0 {
		v.A10 = -v.A10
	}
	if v.A01 < 0 {
		v.A01 = -v.A01
	}
	if v.A11 < 0 {
		v.A11 = -v.A11
	}

	return v
}

func (v *Mat2) PAbs() {
	if v.A00 < 0 {
		v.A00 = -v.A00
	}
	if v.A10 < 0 {
		v.A10 = -v.A10
	}
	if v.A01 < 0 {
		v.A01 = -v.A01
	}
	if v.A11 < 0 {
		v.A11 = -v.A11
	}

}

func (v Mat2) Mul1(o Vec2) Vec2 {
	return Vec2{
		v.A00*o.X + v.A01*o.Y,
		v.A10*o.X + v.A11*o.Y,
	}
}

func (v Mat2) Mul2(o Mat2) Mat2 {
	return Mat2{
		v.A00*o.A00 + v.A01*o.A10,
		v.A10*o.A00 + v.A11*o.A10,
		v.A00*o.A01 + v.A01*o.A11,
		v.A10*o.A01 + v.A11*o.A11,
	}
}

func (v Mat2) Mul3(o Mat2x3) Mat2x3 {
	return Mat2x3{
		v.A00*o.A00 + v.A01*o.A10,
		v.A10*o.A00 + v.A11*o.A10,
		v.A00*o.A01 + v.A01*o.A11,
		v.A10*o.A01 + v.A11*o.A11,
		v.A00*o.A02 + v.A01*o.A12,
		v.A10*o.A02 + v.A11*o.A12,
	}
}

func (v Mat2) Mul4(o Mat2x4) Mat2x4 {
	return Mat2x4{
		v.A00*o.A00 + v.A01*o.A10,
		v.A10*o.A00 + v.A11*o.A10,
		v.A00*o.A01 + v.A01*o.A11,
		v.A10*o.A01 + v.A11*o.A11,
		v.A00*o.A02 + v.A01*o.A12,
		v.A10*o.A02 + v.A11*o.A12,
		v.A00*o.A03 + v.A01*o.A13,
		v.A10*o.A03 + v.A11*o.A13,
	}
}

func (v Mat2) Floor() Mat2 {
	return Mat2{floor(v.A00), floor(v.A10), floor(v.A01), floor(v.A11)}
}
func (v *Mat2) PFloor() {
	v.A00 = floor(v.A00)
	v.A10 = floor(v.A10)
	v.A01 = floor(v.A01)
	v.A11 = floor(v.A11)
}

func (v Mat2) Ceil() Mat2 {
	return Mat2{ceil(v.A00), ceil(v.A10), ceil(v.A01), ceil(v.A11)}
}
func (v *Mat2) PCeil() {
	v.A00 = ceil(v.A00)
	v.A10 = ceil(v.A10)
	v.A01 = ceil(v.A01)
	v.A11 = ceil(v.A11)
}

func (v Mat2) Round() Mat2 {
	return Mat2{round(v.A00), round(v.A10), round(v.A01), round(v.A11)}
}
func (v *Mat2) PRound() {
	v.A00 = round(v.A00)
	v.A10 = round(v.A10)
	v.A01 = round(v.A01)
	v.A11 = round(v.A11)
}

func (v Mat2) Lerp(w Mat2, factor float64) Mat2 {
	return v.Mul(1 - factor).Add(w.Mul(factor))
}
