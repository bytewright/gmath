package math64

// Triangle3 has 3 vertices
type Triangle3 struct {
	A Vec3
	B Vec3
	C Vec3
}

func (r Triangle3) SegmentIntersection(seg Segment3) (Vec3, bool) {
	edge1 := r.B.Sub(r.A)
	edge2 := r.C.Sub(r.A)

	rayOrigin := seg.From
	rayVector := seg.Vec()

	h := rayVector.Cross(edge2)
	a := edge1.Dot(h)

	if a > -0.000001 && a < 0.000001 {
		return Vec3{}, false // This ray is parallel to this triangle.
	}

	f := 1.0 / a
	s := rayOrigin.Sub(r.A)

	u := f * s.Dot(h)
	if u < 0.0 || u > 1.0 {
		return Vec3{}, false
	}
	q := s.Cross(edge1)
	v := f * rayVector.Dot(q)
	if v < 0.0 || u+v > 1.0 {
		return Vec3{}, false
	}

	// At this stage we can compute t to find out where the intersection point is on the line.
	t := f * edge2.Dot(q)
	if t > 0.00000 && t <= 1 {
		return rayOrigin.Add(rayVector.Mul(t)), true
	}

	return Vec3{}, false
}

func (r Triangle3) Barycentric(p Vec3) Vec3 {
	v0 := r.B.Sub(r.A)
	v1 := r.C.Sub(r.A)
	v2 := p.Sub(r.A)

	// Compute dot products
	d00 := v0.Dot(v0)
	d01 := v0.Dot(v1)
	d11 := v1.Dot(v1)
	d20 := v2.Dot(v0)
	d21 := v2.Dot(v1)

	// Compute denominator
	denom := d00*d11 - d01*d01

	// Compute barycentric coordinates
	beta := (d11*d20 - d01*d21) / denom
	gamma := (d00*d21 - d01*d20) / denom
	alpha := 1.0 - beta - gamma

	return Vec3{alpha, beta, gamma}
}
