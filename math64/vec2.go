package math64

import (
	"math"
)

func (v Vec2) Angle() float64 {
	return math.Atan2(float64(v.X), float64(v.Y))
}

// Rotate rotates a vec around it's origin by given angle
func (v Vec2) Rotate(a float32) Vec2 {
	cs := math.Cos(float64(a))
	sn := math.Sin(float64(a))
	return Vec2{v.X*cs - v.Y*sn, v.X*sn + v.Y*cs}
}
