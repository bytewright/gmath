package mathi32

type Segment3 struct {
	From Vec3
	To   Vec3
}

// NewSegment creates a new segment
func NewSegment3(from, to Vec3) Segment3 {
	return Segment3{From: from, To: to}
}

func (r Segment3) Center() Vec3 {
	return r.From.Add(r.To).Div(2)
}

func (r Segment3) Vec() Vec3 {
	return r.To.Sub(r.From)
}

func (r Segment3) Distance(p Vec3) int32 {
	ab := r.To.Sub(r.From)
	ap := p.Sub(r.From)

	t := ap.Dot(ab) / ab.Dot(ab)

	var closest Vec3
	if t < 0.0 {
		closest = r.From
	} else if t > 1.0 {
		closest = r.To
	} else {
		closest = r.From.Add(ab.Mul(t))
	}

	return p.DistanceTo(closest)
}

// ClosestVec returns the position closest to p
// that lies somewhere on the segment.
func (r Segment3) ClosestVec(p Vec3) Vec3 {
	ab := r.To.Sub(r.From)
	ap := p.Sub(r.From)

	t := ap.Dot(ab) / ab.Dot(ab)

	if t < 0.0 {
		return r.From
	}
	if t > 1.0 {
		return r.To
	}

	return r.From.Add(ab.Mul(t))
}

func (r Segment3) Reverse() Segment3 {
	r.To, r.From = r.From, r.To

	return r
}
