package mathi32

import "math"

const epsilon = 0.000001

func implSqrt(i int32) int32 {
	return int32(math.Sqrt(float64(i)))
}
