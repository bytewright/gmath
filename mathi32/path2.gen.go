package mathi32

type Path2 []Vec2

func (s Path2) Transform(m Mat3) Path2 {
	np := make(Path2, len(s))

	for i, p := range s {
		np[i] = m.Mul1(p.Vec3(1)).Vec2()
	}

	return np
}

// Get returns the element at index, if out of bounds it wraps around.
func (s Path2) Get(i int) Vec2 {
	n := len(s)
	return s[((i%n)+n)%n]
}

// Split the given path up into two paths, cut at the given indices.
// The segment defined by the indices is added to both paths.
func (p Path2) Split(i, j int, max int32) (Path2, Path2) {
	if i > j {
		i, j = j, i
	}

	a := p[i]
	b := p[j]
	diff := b.Sub(a)

	np := int(diff.Len() / max)

	between := []Vec2{}

	for i := 0; i < np; i++ {
		between = append(between, a.Add(diff.Mul(int32(i+1)/int32(np+1))))
	}

	p1 := Path2{}
	for k := 0; k <= i; k++ {
		p1 = append(p1, p[k])
	}
	p1 = append(p1, between...)
	for k := j; k < len(p); k++ {
		p1 = append(p1, p[k])
	}

	p2 := Path2{}
	for i := len(between) - 1; i >= 0; i-- {
		p2 = append(p2, between[i])
	}
	for k := i; k <= j; k++ {
		p2 = append(p2, p[k])
	}

	return p1, p2
}

func (s Path2) Segments() []Segment2 {
	// TODO buggy in non 2 dimensions
	segs := make([]Segment2, len(s)-1)

	for i := 1; i < len(s); i++ {
		segs[i-1] = Segment2{s[i-1], s[i]}
	}

	return segs
}

func (s Path2) ClosedSegments() []Segment2 {
	// TODO buggy in non 2 dimensions
	segs := make([]Segment2, len(s))

	for i := 0; i < len(s); i++ {
		segs[i] = Segment2{s[i], s[(i+1)%len(s)]}
	}

	return segs
}

func (s Path2) Close() Path2 {
	if len(s) == 0 {
		return s
	}
	return append(s[:], s[0])
}
