package mathi32

import (
	"gitlab.com/bytewright/gmath/math32"
	"gitlab.com/bytewright/gmath/math64"
	"gitlab.com/bytewright/gmath/mathi"
	"gitlab.com/bytewright/gmath/mathi64"
)

type Mat2x4 struct {
	A00 int32
	A10 int32
	A01 int32
	A11 int32
	A02 int32
	A12 int32
	A03 int32
	A13 int32
}

func (v Mat2x4) Sub(o Mat2x4) Mat2x4 {
	return Mat2x4{v.A00 - o.A00, v.A10 - o.A10, v.A01 - o.A01, v.A11 - o.A11, v.A02 - o.A02, v.A12 - o.A12, v.A03 - o.A03, v.A13 - o.A13}
}

func (v *Mat2x4) PSub(o Mat2x4) {
	v.A00 -= o.A00
	v.A10 -= o.A10
	v.A01 -= o.A01
	v.A11 -= o.A11
	v.A02 -= o.A02
	v.A12 -= o.A12
	v.A03 -= o.A03
	v.A13 -= o.A13
}

func (v Mat2x4) Add(o Mat2x4) Mat2x4 {
	return Mat2x4{v.A00 + o.A00, v.A10 + o.A10, v.A01 + o.A01, v.A11 + o.A11, v.A02 + o.A02, v.A12 + o.A12, v.A03 + o.A03, v.A13 + o.A13}
}

func (v *Mat2x4) PAdd(o Mat2x4) {
	v.A00 += o.A00
	v.A10 += o.A10
	v.A01 += o.A01
	v.A11 += o.A11
	v.A02 += o.A02
	v.A12 += o.A12
	v.A03 += o.A03
	v.A13 += o.A13
}

func (v Mat2x4) Div(d int32) Mat2x4 {
	return Mat2x4{v.A00 / d, v.A10 / d, v.A01 / d, v.A11 / d, v.A02 / d, v.A12 / d, v.A03 / d, v.A13 / d}
}

func (v *Mat2x4) PDiv(d int32) {
	v.A00 /= d
	v.A10 /= d
	v.A01 /= d
	v.A11 /= d
	v.A02 /= d
	v.A12 /= d
	v.A03 /= d
	v.A13 /= d
}

func (v Mat2x4) Mul(m int32) Mat2x4 {
	return Mat2x4{v.A00 * m, v.A10 * m, v.A01 * m, v.A11 * m, v.A02 * m, v.A12 * m, v.A03 * m, v.A13 * m}
}

func (v *Mat2x4) PMul(m int32) {
	v.A00 *= m
	v.A10 *= m
	v.A01 *= m
	v.A11 *= m
	v.A02 *= m
	v.A12 *= m
	v.A03 *= m
	v.A13 *= m
}

func (v Mat2x4) Negative() Mat2x4 {
	return Mat2x4{-v.A00, -v.A10, -v.A01, -v.A11, -v.A02, -v.A12, -v.A03, -v.A13}
}

func (v *Mat2x4) PNegative() {
	v.A00 = -v.A00
	v.A10 = -v.A10
	v.A01 = -v.A01
	v.A11 = -v.A11
	v.A02 = -v.A02
	v.A12 = -v.A12
	v.A03 = -v.A03
	v.A13 = -v.A13
}

func (v Mat2x4) Abs() Mat2x4 {
	if v.A00 < 0 {
		v.A00 = -v.A00
	}
	if v.A10 < 0 {
		v.A10 = -v.A10
	}
	if v.A01 < 0 {
		v.A01 = -v.A01
	}
	if v.A11 < 0 {
		v.A11 = -v.A11
	}
	if v.A02 < 0 {
		v.A02 = -v.A02
	}
	if v.A12 < 0 {
		v.A12 = -v.A12
	}
	if v.A03 < 0 {
		v.A03 = -v.A03
	}
	if v.A13 < 0 {
		v.A13 = -v.A13
	}

	return v
}

func (v *Mat2x4) PAbs() {
	if v.A00 < 0 {
		v.A00 = -v.A00
	}
	if v.A10 < 0 {
		v.A10 = -v.A10
	}
	if v.A01 < 0 {
		v.A01 = -v.A01
	}
	if v.A11 < 0 {
		v.A11 = -v.A11
	}
	if v.A02 < 0 {
		v.A02 = -v.A02
	}
	if v.A12 < 0 {
		v.A12 = -v.A12
	}
	if v.A03 < 0 {
		v.A03 = -v.A03
	}
	if v.A13 < 0 {
		v.A13 = -v.A13
	}

}

func (v Mat2x4) Mul1(o Vec4) Vec2 {
	return Vec2{
		v.A00*o.X + v.A01*o.Y + v.A02*o.Z + v.A03*o.W,
		v.A10*o.X + v.A11*o.Y + v.A12*o.Z + v.A13*o.W,
	}
}

func (v Mat2x4) Mul2(o Mat4x2) Mat2 {
	return Mat2{
		v.A00*o.A00 + v.A01*o.A10 + v.A02*o.A20 + v.A03*o.A30,
		v.A10*o.A00 + v.A11*o.A10 + v.A12*o.A20 + v.A13*o.A30,
		v.A00*o.A01 + v.A01*o.A11 + v.A02*o.A21 + v.A03*o.A31,
		v.A10*o.A01 + v.A11*o.A11 + v.A12*o.A21 + v.A13*o.A31,
	}
}

func (v Mat2x4) Mul3(o Mat4x3) Mat2x3 {
	return Mat2x3{
		v.A00*o.A00 + v.A01*o.A10 + v.A02*o.A20 + v.A03*o.A30,
		v.A10*o.A00 + v.A11*o.A10 + v.A12*o.A20 + v.A13*o.A30,
		v.A00*o.A01 + v.A01*o.A11 + v.A02*o.A21 + v.A03*o.A31,
		v.A10*o.A01 + v.A11*o.A11 + v.A12*o.A21 + v.A13*o.A31,
		v.A00*o.A02 + v.A01*o.A12 + v.A02*o.A22 + v.A03*o.A32,
		v.A10*o.A02 + v.A11*o.A12 + v.A12*o.A22 + v.A13*o.A32,
	}
}

func (v Mat2x4) Mul4(o Mat4) Mat2x4 {
	return Mat2x4{
		v.A00*o.A00 + v.A01*o.A10 + v.A02*o.A20 + v.A03*o.A30,
		v.A10*o.A00 + v.A11*o.A10 + v.A12*o.A20 + v.A13*o.A30,
		v.A00*o.A01 + v.A01*o.A11 + v.A02*o.A21 + v.A03*o.A31,
		v.A10*o.A01 + v.A11*o.A11 + v.A12*o.A21 + v.A13*o.A31,
		v.A00*o.A02 + v.A01*o.A12 + v.A02*o.A22 + v.A03*o.A32,
		v.A10*o.A02 + v.A11*o.A12 + v.A12*o.A22 + v.A13*o.A32,
		v.A00*o.A03 + v.A01*o.A13 + v.A02*o.A23 + v.A03*o.A33,
		v.A10*o.A03 + v.A11*o.A13 + v.A12*o.A23 + v.A13*o.A33,
	}
}

func (v Mat2x4) Int() mathi.Mat2x4 {
	return mathi.Mat2x4{
		int(v.A00),
		int(v.A10),
		int(v.A01),
		int(v.A11),
		int(v.A02),
		int(v.A12),
		int(v.A03),
		int(v.A13),
	}
}

func Mat2x4FromInt(v mathi.Mat2x4) Mat2x4 {
	return Mat2x4{
		int32(v.A00),
		int32(v.A10),
		int32(v.A01),
		int32(v.A11),
		int32(v.A02),
		int32(v.A12),
		int32(v.A03),
		int32(v.A13),
	}
}

func (v Mat2x4) Int64() mathi64.Mat2x4 {
	return mathi64.Mat2x4{
		int64(v.A00),
		int64(v.A10),
		int64(v.A01),
		int64(v.A11),
		int64(v.A02),
		int64(v.A12),
		int64(v.A03),
		int64(v.A13),
	}
}

func Mat2x4FromInt64(v mathi64.Mat2x4) Mat2x4 {
	return Mat2x4{
		int32(v.A00),
		int32(v.A10),
		int32(v.A01),
		int32(v.A11),
		int32(v.A02),
		int32(v.A12),
		int32(v.A03),
		int32(v.A13),
	}
}

func (v Mat2x4) Float32() math32.Mat2x4 {
	return math32.Mat2x4{
		float32(v.A00),
		float32(v.A10),
		float32(v.A01),
		float32(v.A11),
		float32(v.A02),
		float32(v.A12),
		float32(v.A03),
		float32(v.A13),
	}
}

func Mat2x4FromFloat32(v math32.Mat2x4) Mat2x4 {
	return Mat2x4{
		int32(v.A00),
		int32(v.A10),
		int32(v.A01),
		int32(v.A11),
		int32(v.A02),
		int32(v.A12),
		int32(v.A03),
		int32(v.A13),
	}
}

func (v Mat2x4) Float64() math64.Mat2x4 {
	return math64.Mat2x4{
		float64(v.A00),
		float64(v.A10),
		float64(v.A01),
		float64(v.A11),
		float64(v.A02),
		float64(v.A12),
		float64(v.A03),
		float64(v.A13),
	}
}

func Mat2x4FromFloat64(v math64.Mat2x4) Mat2x4 {
	return Mat2x4{
		int32(v.A00),
		int32(v.A10),
		int32(v.A01),
		int32(v.A11),
		int32(v.A02),
		int32(v.A12),
		int32(v.A03),
		int32(v.A13),
	}
}
