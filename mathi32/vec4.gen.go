package mathi32

import (
	"gitlab.com/bytewright/gmath/math32"
	"gitlab.com/bytewright/gmath/math64"
	"gitlab.com/bytewright/gmath/mathi"
	"gitlab.com/bytewright/gmath/mathi64"
)

type Vec4 struct {
	X int32
	Y int32
	Z int32
	W int32
}

func (v Vec4) Vec2() Vec2 {
	return Vec2{v.X, v.Y}
}

func (v Vec4) Vec3() Vec3 {
	return Vec3{v.X, v.Y, v.Z}
}

func (v Vec4) Sub(o Vec4) Vec4 {
	return Vec4{v.X - o.X, v.Y - o.Y, v.Z - o.Z, v.W - o.W}
}

func (v *Vec4) PSub(o Vec4) {
	v.X -= o.X
	v.Y -= o.Y
	v.Z -= o.Z
	v.W -= o.W
}

func (v Vec4) Add(o Vec4) Vec4 {
	return Vec4{v.X + o.X, v.Y + o.Y, v.Z + o.Z, v.W + o.W}
}

func (v *Vec4) PAdd(o Vec4) {
	v.X += o.X
	v.Y += o.Y
	v.Z += o.Z
	v.W += o.W
}

func (v Vec4) Div(d int32) Vec4 {
	return Vec4{v.X / d, v.Y / d, v.Z / d, v.W / d}
}

func (v *Vec4) PDiv(d int32) {
	v.X /= d
	v.Y /= d
	v.Z /= d
	v.W /= d
}

func (v Vec4) Mul(m int32) Vec4 {
	return Vec4{v.X * m, v.Y * m, v.Z * m, v.W * m}
}

func (v *Vec4) PMul(m int32) {
	v.X *= m
	v.Y *= m
	v.Z *= m
	v.W *= m
}

func (v Vec4) Negative() Vec4 {
	return Vec4{-v.X, -v.Y, -v.Z, -v.W}
}

func (v *Vec4) PNegative() {
	v.X = -v.X
	v.Y = -v.Y
	v.Z = -v.Z
	v.W = -v.W
}

func (v Vec4) Abs() Vec4 {
	if v.X < 0 {
		v.X = -v.X
	}
	if v.Y < 0 {
		v.Y = -v.Y
	}
	if v.Z < 0 {
		v.Z = -v.Z
	}
	if v.W < 0 {
		v.W = -v.W
	}

	return v
}

func (v *Vec4) PAbs() {
	if v.X < 0 {
		v.X = -v.X
	}
	if v.Y < 0 {
		v.Y = -v.Y
	}
	if v.Z < 0 {
		v.Z = -v.Z
	}
	if v.W < 0 {
		v.W = -v.W
	}

}

// Min returns the vector with the smaller value for each component
func (v Vec4) Min(o Vec4) Vec4 {
	if o.X < v.X {
		v.X = o.X
	}
	if o.Y < v.Y {
		v.Y = o.Y
	}
	if o.Z < v.Z {
		v.Z = o.Z
	}
	if o.W < v.W {
		v.W = o.W
	}

	return v
}

// Max returns the vector with the bigger value for each component
func (v Vec4) Max(o Vec4) Vec4 {
	if o.X > v.X {
		v.X = o.X
	}
	if o.Y > v.Y {
		v.Y = o.Y
	}
	if o.Z > v.Z {
		v.Z = o.Z
	}
	if o.W > v.W {
		v.W = o.W
	}

	return v
}

func (v Vec4) Dot(o Vec4) int32 {
	return v.X*o.X + v.Y*o.Y + v.Z*o.Z + v.W*o.W
}

func (v Vec4) LenSqr() int32 {
	return v.X*v.X + v.Y*v.Y + v.Z*v.Z + v.W*v.W
}

func (v Vec4) Len() int32 {
	return implSqrt(v.X*v.X + v.Y*v.Y + v.Z*v.Z + v.W*v.W)
}

// LenTaxiCab returns the taxicab length of the vector, sum of all components
func (v Vec4) LenTaxiCab() int32 {
	return Abs(v.X) + Abs(v.Y) + Abs(v.Z) + Abs(v.W)
}

// LenChebyshev returns the chebyshev distance which is the max of all components
func (v Vec4) LenChebyshev() int32 {
	max := int32(0)

	if Abs(v.X) > max {
		max = Abs(v.X)
	}
	if Abs(v.Y) > max {
		max = Abs(v.Y)
	}
	if Abs(v.Z) > max {
		max = Abs(v.Z)
	}
	if Abs(v.W) > max {
		max = Abs(v.W)
	}

	return max
}

func (v Vec4) DistanceTo(o Vec4) int32 {
	return o.Sub(v).Len()
}

func (v Vec4) TaxiCabDistanceTo(o Vec4) int32 {
	return o.Sub(v).LenTaxiCab()
}

func (v Vec4) ChebyshevDistanceTo(o Vec4) int32 {
	return o.Sub(v).LenChebyshev()
}

func (v *Vec4) PLen() int32 {
	return implSqrt(v.X*v.X + v.Y*v.Y + v.Z*v.Z + v.W*v.W)
}

func (v Vec4) Mul4(o Vec4) Vec4 {
	return Vec4{v.X * o.X, v.Y * o.Y, v.Z * o.Z, v.W * o.W}
}

func (v *Vec4) PMul4(o Vec4) {
	v.X *= o.X
	v.Y *= o.Y
	v.Z *= o.Z
	v.W *= o.W
}

func (v Vec4) Div4(o Vec4) Vec4 {
	return Vec4{v.X / o.X, v.Y / o.Y, v.Z / o.Z, v.W / o.W}
}

func (v *Vec4) PDiv4(o Vec4) {
	v.X /= o.X
	v.Y /= o.Y
	v.Z /= o.Z
	v.W /= o.W
}

// ProjectOnPlane projects the vector onto the plane given by the normal n.
func (v Vec4) ProjectOnPlane(n Vec4) Vec4 {
	return v.Sub(n.Mul(n.Dot(v)))
}

func (v Vec4) Int() mathi.Vec4 {
	return mathi.Vec4{
		int(v.X),
		int(v.Y),
		int(v.Z),
		int(v.W),
	}
}

func Vec4FromInt(v mathi.Vec4) Vec4 {
	return Vec4{
		int32(v.X),
		int32(v.Y),
		int32(v.Z),
		int32(v.W),
	}
}

func (v Vec4) Int64() mathi64.Vec4 {
	return mathi64.Vec4{
		int64(v.X),
		int64(v.Y),
		int64(v.Z),
		int64(v.W),
	}
}

func Vec4FromInt64(v mathi64.Vec4) Vec4 {
	return Vec4{
		int32(v.X),
		int32(v.Y),
		int32(v.Z),
		int32(v.W),
	}
}

func (v Vec4) Float32() math32.Vec4 {
	return math32.Vec4{
		float32(v.X),
		float32(v.Y),
		float32(v.Z),
		float32(v.W),
	}
}

func Vec4FromFloat32(v math32.Vec4) Vec4 {
	return Vec4{
		int32(v.X),
		int32(v.Y),
		int32(v.Z),
		int32(v.W),
	}
}

func (v Vec4) Float64() math64.Vec4 {
	return math64.Vec4{
		float64(v.X),
		float64(v.Y),
		float64(v.Z),
		float64(v.W),
	}
}

func Vec4FromFloat64(v math64.Vec4) Vec4 {
	return Vec4{
		int32(v.X),
		int32(v.Y),
		int32(v.Z),
		int32(v.W),
	}
}
