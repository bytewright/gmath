package mathui32

import "math"

const epsilon = 0.000001

func implSqrt(i uint32) uint32 {
	return uint32(math.Sqrt(float64(i)))
}
