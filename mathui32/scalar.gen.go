package mathui32

// Max returns the bigger of the two numbers
// we don't care about positive, negative zero or infinity
func Max(a, b uint32) uint32 {
	if a > b {
		return a
	}
	return b
}

// Min returns the smaller of the two numbers
// we don't care about positive, negative zero or infinity
func Min(a, b uint32) uint32 {
	if a < b {
		return a
	}
	return b
}

func Abs(a uint32) uint32 {
	if a < 0 {
		return -a
	}
	return a
}
