package mathui32

type Segment2 struct {
	From Vec2
	To   Vec2
}

// NewSegment creates a new segment
func NewSegment2(from, to Vec2) Segment2 {
	return Segment2{From: from, To: to}
}

func (r Segment2) Center() Vec2 {
	return r.From.Add(r.To).Div(2)
}

func (r Segment2) Vec() Vec2 {
	return r.To.Sub(r.From)
}

func (r Segment2) Distance(p Vec2) uint32 {
	ab := r.To.Sub(r.From)
	ap := p.Sub(r.From)

	t := ap.Dot(ab) / ab.Dot(ab)

	var closest Vec2
	if t < 0.0 {
		closest = r.From
	} else if t > 1.0 {
		closest = r.To
	} else {
		closest = r.From.Add(ab.Mul(t))
	}

	return p.DistanceTo(closest)
}

// ClosestVec returns the position closest to p
// that lies somewhere on the segment.
func (r Segment2) ClosestVec(p Vec2) Vec2 {
	ab := r.To.Sub(r.From)
	ap := p.Sub(r.From)

	t := ap.Dot(ab) / ab.Dot(ab)

	if t < 0.0 {
		return r.From
	}
	if t > 1.0 {
		return r.To
	}

	return r.From.Add(ab.Mul(t))
}

func (r Segment2) Reverse() Segment2 {
	r.To, r.From = r.From, r.To

	return r
}
