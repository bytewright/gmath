package math32

func Ortho(left, right, bottom, top, near, far float32) Mat4 {
	rml := (right - left)
	rpl := (right + left)
	tmb := (top - bottom)
	tpb := (top + bottom)
	fmn := (far - near)
	fpn := (far + near)

	return Mat4{
		2 / rml, 0, 0, 0,
		0, 2 / tmb, 0, 0,
		0, 0, -2 / fmn, 0,
		-rpl / rml, -tpb / tmb, -fpn / fmn, 1,
	}
}

func Perspective(left, right, bottom, top, near, far float32) Mat4 {
	rml := (right - left)
	rpl := (right + left)
	tmb := (top - bottom)
	tpb := (top + bottom)
	fmn := (far - near)
	fpn := (far + near)
	n := near
	f := far

	return Mat4{
		2 * n / rml, 0, 0, 0,
		0, 2 * n / tmb, 0, 0,
		rpl / rml, tpb / tmb, -fpn / fmn, -1,
		0, 0, -2 * f * n / fmn, 0,
	}
}

func LookAt(from, to, up Vec3) Mat4 {
	f := to.Sub(from).Normalize()
	s := f.Cross(up.Normalize()).Normalize()
	u := s.Cross(f)

	m := Mat4{
		s.X, u.X, -f.X, 0,
		s.Y, u.Y, -f.Y, 0,
		s.Z, u.Z, -f.Z, 0,
		0, 0, 0, 1,
	}

	return m.Mul4(
		Translate3(from.Mul(-1)),
	)
}

func Translate2(v Vec2) Mat3 {
	return Mat3{1, 0, 0, 0, 1, 0, v.X, v.Y, 1}
}

func Scale2(v Vec2) Mat3 {
	return Mat3{v.X, 0, 0, 0, v.Y, 0, 0, 0, 1}
}

func Translate3(v Vec3) Mat4 {
	return Mat4{1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, v.X, v.Y, v.Z, 1}
}

func Scale3(v Vec3) Mat4 {
	return Mat4{v.X, 0, 0, 0, 0, v.Y, 0, 0, 0, 0, v.Z, 0, 0, 0, 0, 1}
}

func Rotate3(u Vec3, a float32) Mat4 {
	cosA := cos(a)
	cosAD := 1 - cosA

	sinA := sin(a)

	ux := u.X
	uy := u.Y
	uz := u.Z

	return Mat4{
		cosA + ux*ux*cosAD,
		uy*ux*cosAD + uz*sinA,
		uz*ux*cosAD - uy*sinA,
		0,

		ux*uy*cosAD - uz*sinA,
		cosA + uy*uy*cosAD,
		uz*uy*cosAD + ux*sinA,
		0,

		ux*uz*cosAD + uy*sinA,
		uy*uz*cosAD - ux*sinA,
		cosA + uz*uz*cosAD,
		0,

		0, 0, 0, 1,
	}
}

func Rotate2(a float32) Mat3 {
	cosA := cos(a)

	sinA := sin(a)

	return Mat3{
		cosA, -sinA, 0,
		sinA, cosA, 0,
		0, 0, 1,
	}
}
