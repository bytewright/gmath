package math32_test

import (
	"math/rand"
	"testing"

	"gitlab.com/bytewright/gmath/canvas"
	"gitlab.com/bytewright/gmath/math32"
)

type Circle struct {
	Center math32.Vec2
	Radius float32
}

func TestSegmentCircleIntersection(t *testing.T) {
	r := rand.New(rand.NewSource(100))

	cv := canvas.New(1000, 1000)

	circles := []*Circle{}
	for c := 0; c < 10; c++ {
		circles = append(circles, &Circle{
			Center: math32.Vec2{r.Float32(), r.Float32()}.Mul(1000),
			Radius: (r.Float32() + 1) * 100,
		})
	}

	segments := []math32.Segment2{{
		math32.Vec2{10, 10}, math32.Vec2{800, 600},
	}, {
		math32.Vec2{10, 10}, math32.Vec2{400, 600},
	}, {
		math32.Vec2{830, 600}, math32.Vec2{600, 800},
	}}

	for _, c := range circles {
		cv.Stroke32Circle2(c.Center, c.Radius, math32.Vec4{1, 0.2, 0.2, 1}, 1)
	}

	for _, s := range segments {
		cv.Stroke32Segment2(s, math32.Vec4{0.5, 1, 0.2, 1}, 1)
		for _, c := range circles {
			num, a, b := s.IntersectsCircle(c.Center, c.Radius)
			if num > 0 {
				cv.Stroke32Circle2(a, 3, math32.Vec4{0.4, 0.2, 1, 1}, 4)
			}
			if num > 1 {
				cv.Stroke32Circle2(b, 3, math32.Vec4{0.4, 0.2, 1, 1}, 4)
			}
		}
	}

	err := cv.SaveAsPNG("segmentCircleIntersection.png")
	if err != nil {
		t.Fatal(err)
	}
}
