package math32

type Path3 []Vec3

func (s Path3) Transform(m Mat4) Path3 {
	np := make(Path3, len(s))

	for i, p := range s {
		np[i] = m.Mul1(p.Vec4(1)).Vec3()
	}

	return np
}

// Get returns the element at index, if out of bounds it wraps around.
func (s Path3) Get(i int) Vec3 {
	n := len(s)
	return s[((i%n)+n)%n]
}

func (s Path3) GetNormal(i int, up Vec3) Vec3 {
	f := s.Get(i - 1)
	t := s.Get(i)
	n := s.Get(i + 1)
	diff1 := f.Sub(t).Normalize()
	diff2 := t.Sub(n).Normalize()

	n1 := up.Cross(diff1)
	n2 := up.Cross(diff2)

	return n1.Add(n2).Normalize()
}

// Split the given path up into two paths, cut at the given indices.
// The segment defined by the indices is added to both paths.
func (p Path3) Split(i, j int, max float32) (Path3, Path3) {
	if i > j {
		i, j = j, i
	}

	a := p[i]
	b := p[j]
	diff := b.Sub(a)

	np := int(diff.Len() / max)

	between := []Vec3{}

	for i := 0; i < np; i++ {
		between = append(between, a.Add(diff.Mul(float32(i+1)/float32(np+1))))
	}

	p1 := Path3{}
	for k := 0; k <= i; k++ {
		p1 = append(p1, p[k])
	}
	p1 = append(p1, between...)
	for k := j; k < len(p); k++ {
		p1 = append(p1, p[k])
	}

	p2 := Path3{}
	for i := len(between) - 1; i >= 0; i-- {
		p2 = append(p2, between[i])
	}
	for k := i; k <= j; k++ {
		p2 = append(p2, p[k])
	}

	return p1, p2
}

func (s Path3) Segments() []Segment3 {
	// TODO buggy in non 2 dimensions
	segs := make([]Segment3, len(s)-1)

	for i := 1; i < len(s); i++ {
		segs[i-1] = Segment3{s[i-1], s[i]}
	}

	return segs
}

func (s Path3) ClosedSegments() []Segment3 {
	// TODO buggy in non 2 dimensions
	segs := make([]Segment3, len(s))

	for i := 0; i < len(s); i++ {
		segs[i] = Segment3{s[i], s[(i+1)%len(s)]}
	}

	return segs
}

func (s Path3) Close() Path3 {
	if len(s) == 0 {
		return s
	}
	return append(s[:], s[0])
}
