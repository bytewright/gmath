package math32_test

import (
	"testing"

	"github.com/akabio/expect"
	"gitlab.com/bytewright/gmath/math32"
)

func TestRayPlaneIntersection(t *testing.T) {
	ray := math32.NewRay3(math32.Vec3{0, 0, 10}, math32.Vec3{0, -1, -1})
	plane := math32.NewRay3(math32.Vec3{0, 0, 5}, math32.Vec3{0, 0, 1})

	ip, _ := ray.PlaneIntersection(plane)

	expect.Value(t, "intersection", ip).ToBe(math32.Vec3{0, -5, 5})
}
