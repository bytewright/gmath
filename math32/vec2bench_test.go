package math32

import (
	"testing"

	"github.com/akabio/expect"
)

// 35 ns
func BenchmarkVec2Ops(b *testing.B) {
	a := Vec2{20, 10}
	c := Vec2{50, -10}

	r := Vec2{}

	for i := 0; i < b.N; i++ {
		x := a.Mul2(c)
		x = x.Add(c)
		x = x.Div2(a)
		x = x.Normalize()
		r = r.Add(x)
	}
}

func BenchmarkVec2POps(b *testing.B) {
	a := Vec2{20, 10}
	c := Vec2{50, -10}
	r := Vec2{}

	for i := 0; i < b.N; i++ {
		x := a
		x.PMul2(c)
		x.PAdd(c)
		x.PDiv2(a)
		x.PNormalize()
		r.PAdd(x)
	}
}

func TestOpsByValue(t *testing.T) {
	a := Vec2{20, 10}
	b := Vec2{50, -10}

	x := a.Mul2(b)
	x = x.Add(b)
	x = x.Div2(a)
	x = x.Normalize()

	expect.Value(t, "x", x).ToBe(Vec2{0.9787472, -0.20507084})
}

func TestOpsByReference(t *testing.T) {
	a := Vec2{20, 10}
	b := Vec2{50, -10}

	x := a
	x.PMul2(b)
	x.PAdd(b)
	x.PDiv2(a)
	x.PNormalize()

	expect.Value(t, "x", x).ToBe(Vec2{0.9787472, -0.20507084})
}
