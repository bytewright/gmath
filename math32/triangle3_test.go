package math32

import (
	"testing"

	"github.com/akabio/expect"
)

func TestBarycentric(t *testing.T) {
	tri := Triangle3{
		A: Vec3{-2, -1, 0},
		B: Vec3{3, -1, 0},
		C: Vec3{1, 4, 0},
	}
	bary := tri.Barycentric(Vec3{2, 1, 0})

	expect.Value(t, "x", bary.X).ToBeAbout(1.0/25, 0.0001)
	expect.Value(t, "y", bary.Y).ToBeAbout(14.0/25, 0.0001)
	expect.Value(t, "z", bary.Z).ToBeAbout(2.0/5, 0.0001)
}
