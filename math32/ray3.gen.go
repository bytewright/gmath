package math32

// Ray has origin and direction, direction is expected to be normalized
type Ray3 struct {
	Origin Vec3
	Dir    Vec3
}

// NewRay creates a new ray, normalizes direction
func NewRay3(origin, dir Vec3) Ray3 {
	return Ray3{Origin: origin, Dir: dir.Normalize()}
}

func (r Ray3) DistanceToVec(v Vec3) float32 {
	return r.Dir.Cross(v.Sub(r.Origin)).Len()
}

func (r Ray3) InFront(v Vec3) bool {
	z := r.Dir.Dot(v.Sub(r.Origin))
	return z >= 0
}

func (r Ray3) PlaneIntersection(plane Ray3) (Vec3, bool) {
	diff := r.Origin.Sub(plane.Origin)
	prod1 := diff.Dot(plane.Dir)
	prod2 := r.Dir.Dot(plane.Dir)
	if prod2 == 0 {
		return Vec3{}, false
	}

	prod3 := prod1 / prod2
	return r.Origin.Sub(r.Dir.Mul(prod3)), true
}
