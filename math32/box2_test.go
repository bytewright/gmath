package math32_test

import (
	"fmt"
	"testing"

	"github.com/akabio/expect"
	"gitlab.com/bytewright/gmath/math32"
)

func TestBoxDistanceToPoint(t *testing.T) {
	buc := math32.NewBox2FromSize(math32.Vec2{-1, -1}, math32.Vec2{3, 3})

	vecs := map[math32.Vec2]float64{
		{0, 0}:   0,
		{-1, -1}: 0,
		{2, 2}:   0,
		{0, 3}:   1,
		{0, -2}:  1,
		{3, 0}:   1,
		{-2, 0}:  1,
		{-2, -2}: 1.41421,
	}

	for v, expected := range vecs {
		expect.Value(t, fmt.Sprintf("dist to %v", v), buc.DistanceToVec(v)).ToBeAbout(expected, 0.0001)
	}
}

func TestIntersect(t *testing.T) {
	expect.Value(t, "boxes", math32.NewBox2FromSize(math32.Vec2{}, math32.Vec2{X: 1, Y: 1}).Intersects(
		math32.NewBox2FromSize(math32.Vec2{X: 1}, math32.Vec2{X: 1, Y: 1}),
	)).ToBe(false)

	expect.Value(t, "boxes", math32.NewBox2FromSize(math32.Vec2{}, math32.Vec2{X: 1, Y: 1}).Intersects(
		math32.NewBox2FromSize(math32.Vec2{X: 0.99}, math32.Vec2{X: 1, Y: 1}),
	)).ToBe(true)

	expect.Value(t, "boxes", math32.NewBox2FromSize(math32.Vec2{X: 1}, math32.Vec2{X: 1, Y: 1}).Intersects(
		math32.NewBox2FromSize(math32.Vec2{}, math32.Vec2{X: 1, Y: 1}),
	)).ToBe(false)
}
