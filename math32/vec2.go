package math32

import "math"

func (v Vec2) Angle() float32 {
	return float32(math.Atan2(float64(v.X), float64(v.Y)))
}

// Rotate rotates a vec around it's origin by given angle
func (v Vec2) Rotate(a float32) Vec2 {
	cs := float32(math.Cos(float64(a)))
	sn := float32(math.Sin(float64(a)))
	return Vec2{v.X*cs - v.Y*sn, v.X*sn + v.Y*cs}
}
