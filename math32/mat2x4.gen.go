package math32

import (
	"gitlab.com/bytewright/gmath/math64"
)

type Mat2x4 struct {
	A00 float32
	A10 float32
	A01 float32
	A11 float32
	A02 float32
	A12 float32
	A03 float32
	A13 float32
}

func (v Mat2x4) Sub(o Mat2x4) Mat2x4 {
	return Mat2x4{v.A00 - o.A00, v.A10 - o.A10, v.A01 - o.A01, v.A11 - o.A11, v.A02 - o.A02, v.A12 - o.A12, v.A03 - o.A03, v.A13 - o.A13}
}

func (v *Mat2x4) PSub(o Mat2x4) {
	v.A00 -= o.A00
	v.A10 -= o.A10
	v.A01 -= o.A01
	v.A11 -= o.A11
	v.A02 -= o.A02
	v.A12 -= o.A12
	v.A03 -= o.A03
	v.A13 -= o.A13
}

func (v Mat2x4) Add(o Mat2x4) Mat2x4 {
	return Mat2x4{v.A00 + o.A00, v.A10 + o.A10, v.A01 + o.A01, v.A11 + o.A11, v.A02 + o.A02, v.A12 + o.A12, v.A03 + o.A03, v.A13 + o.A13}
}

func (v *Mat2x4) PAdd(o Mat2x4) {
	v.A00 += o.A00
	v.A10 += o.A10
	v.A01 += o.A01
	v.A11 += o.A11
	v.A02 += o.A02
	v.A12 += o.A12
	v.A03 += o.A03
	v.A13 += o.A13
}

func (v Mat2x4) Div(d float32) Mat2x4 {
	return Mat2x4{v.A00 / d, v.A10 / d, v.A01 / d, v.A11 / d, v.A02 / d, v.A12 / d, v.A03 / d, v.A13 / d}
}

func (v *Mat2x4) PDiv(d float32) {
	v.A00 /= d
	v.A10 /= d
	v.A01 /= d
	v.A11 /= d
	v.A02 /= d
	v.A12 /= d
	v.A03 /= d
	v.A13 /= d
}

func (v Mat2x4) Mul(m float32) Mat2x4 {
	return Mat2x4{v.A00 * m, v.A10 * m, v.A01 * m, v.A11 * m, v.A02 * m, v.A12 * m, v.A03 * m, v.A13 * m}
}

func (v *Mat2x4) PMul(m float32) {
	v.A00 *= m
	v.A10 *= m
	v.A01 *= m
	v.A11 *= m
	v.A02 *= m
	v.A12 *= m
	v.A03 *= m
	v.A13 *= m
}

func (v Mat2x4) Negative() Mat2x4 {
	return Mat2x4{-v.A00, -v.A10, -v.A01, -v.A11, -v.A02, -v.A12, -v.A03, -v.A13}
}

func (v *Mat2x4) PNegative() {
	v.A00 = -v.A00
	v.A10 = -v.A10
	v.A01 = -v.A01
	v.A11 = -v.A11
	v.A02 = -v.A02
	v.A12 = -v.A12
	v.A03 = -v.A03
	v.A13 = -v.A13
}

func (v Mat2x4) Abs() Mat2x4 {
	if v.A00 < 0 {
		v.A00 = -v.A00
	}
	if v.A10 < 0 {
		v.A10 = -v.A10
	}
	if v.A01 < 0 {
		v.A01 = -v.A01
	}
	if v.A11 < 0 {
		v.A11 = -v.A11
	}
	if v.A02 < 0 {
		v.A02 = -v.A02
	}
	if v.A12 < 0 {
		v.A12 = -v.A12
	}
	if v.A03 < 0 {
		v.A03 = -v.A03
	}
	if v.A13 < 0 {
		v.A13 = -v.A13
	}

	return v
}

func (v *Mat2x4) PAbs() {
	if v.A00 < 0 {
		v.A00 = -v.A00
	}
	if v.A10 < 0 {
		v.A10 = -v.A10
	}
	if v.A01 < 0 {
		v.A01 = -v.A01
	}
	if v.A11 < 0 {
		v.A11 = -v.A11
	}
	if v.A02 < 0 {
		v.A02 = -v.A02
	}
	if v.A12 < 0 {
		v.A12 = -v.A12
	}
	if v.A03 < 0 {
		v.A03 = -v.A03
	}
	if v.A13 < 0 {
		v.A13 = -v.A13
	}

}

func (v Mat2x4) Mul1(o Vec4) Vec2 {
	return Vec2{
		v.A00*o.X + v.A01*o.Y + v.A02*o.Z + v.A03*o.W,
		v.A10*o.X + v.A11*o.Y + v.A12*o.Z + v.A13*o.W,
	}
}

func (v Mat2x4) Mul2(o Mat4x2) Mat2 {
	return Mat2{
		v.A00*o.A00 + v.A01*o.A10 + v.A02*o.A20 + v.A03*o.A30,
		v.A10*o.A00 + v.A11*o.A10 + v.A12*o.A20 + v.A13*o.A30,
		v.A00*o.A01 + v.A01*o.A11 + v.A02*o.A21 + v.A03*o.A31,
		v.A10*o.A01 + v.A11*o.A11 + v.A12*o.A21 + v.A13*o.A31,
	}
}

func (v Mat2x4) Mul3(o Mat4x3) Mat2x3 {
	return Mat2x3{
		v.A00*o.A00 + v.A01*o.A10 + v.A02*o.A20 + v.A03*o.A30,
		v.A10*o.A00 + v.A11*o.A10 + v.A12*o.A20 + v.A13*o.A30,
		v.A00*o.A01 + v.A01*o.A11 + v.A02*o.A21 + v.A03*o.A31,
		v.A10*o.A01 + v.A11*o.A11 + v.A12*o.A21 + v.A13*o.A31,
		v.A00*o.A02 + v.A01*o.A12 + v.A02*o.A22 + v.A03*o.A32,
		v.A10*o.A02 + v.A11*o.A12 + v.A12*o.A22 + v.A13*o.A32,
	}
}

func (v Mat2x4) Mul4(o Mat4) Mat2x4 {
	return Mat2x4{
		v.A00*o.A00 + v.A01*o.A10 + v.A02*o.A20 + v.A03*o.A30,
		v.A10*o.A00 + v.A11*o.A10 + v.A12*o.A20 + v.A13*o.A30,
		v.A00*o.A01 + v.A01*o.A11 + v.A02*o.A21 + v.A03*o.A31,
		v.A10*o.A01 + v.A11*o.A11 + v.A12*o.A21 + v.A13*o.A31,
		v.A00*o.A02 + v.A01*o.A12 + v.A02*o.A22 + v.A03*o.A32,
		v.A10*o.A02 + v.A11*o.A12 + v.A12*o.A22 + v.A13*o.A32,
		v.A00*o.A03 + v.A01*o.A13 + v.A02*o.A23 + v.A03*o.A33,
		v.A10*o.A03 + v.A11*o.A13 + v.A12*o.A23 + v.A13*o.A33,
	}
}

func (v Mat2x4) Floor() Mat2x4 {
	return Mat2x4{floor(v.A00), floor(v.A10), floor(v.A01), floor(v.A11), floor(v.A02), floor(v.A12), floor(v.A03), floor(v.A13)}
}
func (v *Mat2x4) PFloor() {
	v.A00 = floor(v.A00)
	v.A10 = floor(v.A10)
	v.A01 = floor(v.A01)
	v.A11 = floor(v.A11)
	v.A02 = floor(v.A02)
	v.A12 = floor(v.A12)
	v.A03 = floor(v.A03)
	v.A13 = floor(v.A13)
}

func (v Mat2x4) Ceil() Mat2x4 {
	return Mat2x4{ceil(v.A00), ceil(v.A10), ceil(v.A01), ceil(v.A11), ceil(v.A02), ceil(v.A12), ceil(v.A03), ceil(v.A13)}
}
func (v *Mat2x4) PCeil() {
	v.A00 = ceil(v.A00)
	v.A10 = ceil(v.A10)
	v.A01 = ceil(v.A01)
	v.A11 = ceil(v.A11)
	v.A02 = ceil(v.A02)
	v.A12 = ceil(v.A12)
	v.A03 = ceil(v.A03)
	v.A13 = ceil(v.A13)
}

func (v Mat2x4) Round() Mat2x4 {
	return Mat2x4{round(v.A00), round(v.A10), round(v.A01), round(v.A11), round(v.A02), round(v.A12), round(v.A03), round(v.A13)}
}
func (v *Mat2x4) PRound() {
	v.A00 = round(v.A00)
	v.A10 = round(v.A10)
	v.A01 = round(v.A01)
	v.A11 = round(v.A11)
	v.A02 = round(v.A02)
	v.A12 = round(v.A12)
	v.A03 = round(v.A03)
	v.A13 = round(v.A13)
}

func (v Mat2x4) Lerp(w Mat2x4, factor float32) Mat2x4 {
	return v.Mul(1 - factor).Add(w.Mul(factor))
}

func (v Mat2x4) Float64() math64.Mat2x4 {
	return math64.Mat2x4{
		float64(v.A00),
		float64(v.A10),
		float64(v.A01),
		float64(v.A11),
		float64(v.A02),
		float64(v.A12),
		float64(v.A03),
		float64(v.A13),
	}
}

func Mat2x4FromFloat64(v math64.Mat2x4) Mat2x4 {
	return Mat2x4{
		float32(v.A00),
		float32(v.A10),
		float32(v.A01),
		float32(v.A11),
		float32(v.A02),
		float32(v.A12),
		float32(v.A03),
		float32(v.A13),
	}
}
