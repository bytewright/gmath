package math32

import (
	"gitlab.com/bytewright/gmath/math64"
)

type Mat4x3 struct {
	A00 float32
	A10 float32
	A20 float32
	A30 float32
	A01 float32
	A11 float32
	A21 float32
	A31 float32
	A02 float32
	A12 float32
	A22 float32
	A32 float32
}

func (v Mat4x3) Sub(o Mat4x3) Mat4x3 {
	return Mat4x3{v.A00 - o.A00, v.A10 - o.A10, v.A20 - o.A20, v.A30 - o.A30, v.A01 - o.A01, v.A11 - o.A11, v.A21 - o.A21, v.A31 - o.A31, v.A02 - o.A02, v.A12 - o.A12, v.A22 - o.A22, v.A32 - o.A32}
}

func (v *Mat4x3) PSub(o Mat4x3) {
	v.A00 -= o.A00
	v.A10 -= o.A10
	v.A20 -= o.A20
	v.A30 -= o.A30
	v.A01 -= o.A01
	v.A11 -= o.A11
	v.A21 -= o.A21
	v.A31 -= o.A31
	v.A02 -= o.A02
	v.A12 -= o.A12
	v.A22 -= o.A22
	v.A32 -= o.A32
}

func (v Mat4x3) Add(o Mat4x3) Mat4x3 {
	return Mat4x3{v.A00 + o.A00, v.A10 + o.A10, v.A20 + o.A20, v.A30 + o.A30, v.A01 + o.A01, v.A11 + o.A11, v.A21 + o.A21, v.A31 + o.A31, v.A02 + o.A02, v.A12 + o.A12, v.A22 + o.A22, v.A32 + o.A32}
}

func (v *Mat4x3) PAdd(o Mat4x3) {
	v.A00 += o.A00
	v.A10 += o.A10
	v.A20 += o.A20
	v.A30 += o.A30
	v.A01 += o.A01
	v.A11 += o.A11
	v.A21 += o.A21
	v.A31 += o.A31
	v.A02 += o.A02
	v.A12 += o.A12
	v.A22 += o.A22
	v.A32 += o.A32
}

func (v Mat4x3) Div(d float32) Mat4x3 {
	return Mat4x3{v.A00 / d, v.A10 / d, v.A20 / d, v.A30 / d, v.A01 / d, v.A11 / d, v.A21 / d, v.A31 / d, v.A02 / d, v.A12 / d, v.A22 / d, v.A32 / d}
}

func (v *Mat4x3) PDiv(d float32) {
	v.A00 /= d
	v.A10 /= d
	v.A20 /= d
	v.A30 /= d
	v.A01 /= d
	v.A11 /= d
	v.A21 /= d
	v.A31 /= d
	v.A02 /= d
	v.A12 /= d
	v.A22 /= d
	v.A32 /= d
}

func (v Mat4x3) Mul(m float32) Mat4x3 {
	return Mat4x3{v.A00 * m, v.A10 * m, v.A20 * m, v.A30 * m, v.A01 * m, v.A11 * m, v.A21 * m, v.A31 * m, v.A02 * m, v.A12 * m, v.A22 * m, v.A32 * m}
}

func (v *Mat4x3) PMul(m float32) {
	v.A00 *= m
	v.A10 *= m
	v.A20 *= m
	v.A30 *= m
	v.A01 *= m
	v.A11 *= m
	v.A21 *= m
	v.A31 *= m
	v.A02 *= m
	v.A12 *= m
	v.A22 *= m
	v.A32 *= m
}

func (v Mat4x3) Negative() Mat4x3 {
	return Mat4x3{-v.A00, -v.A10, -v.A20, -v.A30, -v.A01, -v.A11, -v.A21, -v.A31, -v.A02, -v.A12, -v.A22, -v.A32}
}

func (v *Mat4x3) PNegative() {
	v.A00 = -v.A00
	v.A10 = -v.A10
	v.A20 = -v.A20
	v.A30 = -v.A30
	v.A01 = -v.A01
	v.A11 = -v.A11
	v.A21 = -v.A21
	v.A31 = -v.A31
	v.A02 = -v.A02
	v.A12 = -v.A12
	v.A22 = -v.A22
	v.A32 = -v.A32
}

func (v Mat4x3) Abs() Mat4x3 {
	if v.A00 < 0 {
		v.A00 = -v.A00
	}
	if v.A10 < 0 {
		v.A10 = -v.A10
	}
	if v.A20 < 0 {
		v.A20 = -v.A20
	}
	if v.A30 < 0 {
		v.A30 = -v.A30
	}
	if v.A01 < 0 {
		v.A01 = -v.A01
	}
	if v.A11 < 0 {
		v.A11 = -v.A11
	}
	if v.A21 < 0 {
		v.A21 = -v.A21
	}
	if v.A31 < 0 {
		v.A31 = -v.A31
	}
	if v.A02 < 0 {
		v.A02 = -v.A02
	}
	if v.A12 < 0 {
		v.A12 = -v.A12
	}
	if v.A22 < 0 {
		v.A22 = -v.A22
	}
	if v.A32 < 0 {
		v.A32 = -v.A32
	}

	return v
}

func (v *Mat4x3) PAbs() {
	if v.A00 < 0 {
		v.A00 = -v.A00
	}
	if v.A10 < 0 {
		v.A10 = -v.A10
	}
	if v.A20 < 0 {
		v.A20 = -v.A20
	}
	if v.A30 < 0 {
		v.A30 = -v.A30
	}
	if v.A01 < 0 {
		v.A01 = -v.A01
	}
	if v.A11 < 0 {
		v.A11 = -v.A11
	}
	if v.A21 < 0 {
		v.A21 = -v.A21
	}
	if v.A31 < 0 {
		v.A31 = -v.A31
	}
	if v.A02 < 0 {
		v.A02 = -v.A02
	}
	if v.A12 < 0 {
		v.A12 = -v.A12
	}
	if v.A22 < 0 {
		v.A22 = -v.A22
	}
	if v.A32 < 0 {
		v.A32 = -v.A32
	}

}

func (v Mat4x3) Mul1(o Vec3) Vec4 {
	return Vec4{
		v.A00*o.X + v.A01*o.Y + v.A02*o.Z,
		v.A10*o.X + v.A11*o.Y + v.A12*o.Z,
		v.A20*o.X + v.A21*o.Y + v.A22*o.Z,
		v.A30*o.X + v.A31*o.Y + v.A32*o.Z,
	}
}

func (v Mat4x3) Mul2(o Mat3x2) Mat4x2 {
	return Mat4x2{
		v.A00*o.A00 + v.A01*o.A10 + v.A02*o.A20,
		v.A10*o.A00 + v.A11*o.A10 + v.A12*o.A20,
		v.A20*o.A00 + v.A21*o.A10 + v.A22*o.A20,
		v.A30*o.A00 + v.A31*o.A10 + v.A32*o.A20,
		v.A00*o.A01 + v.A01*o.A11 + v.A02*o.A21,
		v.A10*o.A01 + v.A11*o.A11 + v.A12*o.A21,
		v.A20*o.A01 + v.A21*o.A11 + v.A22*o.A21,
		v.A30*o.A01 + v.A31*o.A11 + v.A32*o.A21,
	}
}

func (v Mat4x3) Mul3(o Mat3) Mat4x3 {
	return Mat4x3{
		v.A00*o.A00 + v.A01*o.A10 + v.A02*o.A20,
		v.A10*o.A00 + v.A11*o.A10 + v.A12*o.A20,
		v.A20*o.A00 + v.A21*o.A10 + v.A22*o.A20,
		v.A30*o.A00 + v.A31*o.A10 + v.A32*o.A20,
		v.A00*o.A01 + v.A01*o.A11 + v.A02*o.A21,
		v.A10*o.A01 + v.A11*o.A11 + v.A12*o.A21,
		v.A20*o.A01 + v.A21*o.A11 + v.A22*o.A21,
		v.A30*o.A01 + v.A31*o.A11 + v.A32*o.A21,
		v.A00*o.A02 + v.A01*o.A12 + v.A02*o.A22,
		v.A10*o.A02 + v.A11*o.A12 + v.A12*o.A22,
		v.A20*o.A02 + v.A21*o.A12 + v.A22*o.A22,
		v.A30*o.A02 + v.A31*o.A12 + v.A32*o.A22,
	}
}

func (v Mat4x3) Mul4(o Mat3x4) Mat4 {
	return Mat4{
		v.A00*o.A00 + v.A01*o.A10 + v.A02*o.A20,
		v.A10*o.A00 + v.A11*o.A10 + v.A12*o.A20,
		v.A20*o.A00 + v.A21*o.A10 + v.A22*o.A20,
		v.A30*o.A00 + v.A31*o.A10 + v.A32*o.A20,
		v.A00*o.A01 + v.A01*o.A11 + v.A02*o.A21,
		v.A10*o.A01 + v.A11*o.A11 + v.A12*o.A21,
		v.A20*o.A01 + v.A21*o.A11 + v.A22*o.A21,
		v.A30*o.A01 + v.A31*o.A11 + v.A32*o.A21,
		v.A00*o.A02 + v.A01*o.A12 + v.A02*o.A22,
		v.A10*o.A02 + v.A11*o.A12 + v.A12*o.A22,
		v.A20*o.A02 + v.A21*o.A12 + v.A22*o.A22,
		v.A30*o.A02 + v.A31*o.A12 + v.A32*o.A22,
		v.A00*o.A03 + v.A01*o.A13 + v.A02*o.A23,
		v.A10*o.A03 + v.A11*o.A13 + v.A12*o.A23,
		v.A20*o.A03 + v.A21*o.A13 + v.A22*o.A23,
		v.A30*o.A03 + v.A31*o.A13 + v.A32*o.A23,
	}
}

func (v Mat4x3) Floor() Mat4x3 {
	return Mat4x3{floor(v.A00), floor(v.A10), floor(v.A20), floor(v.A30), floor(v.A01), floor(v.A11), floor(v.A21), floor(v.A31), floor(v.A02), floor(v.A12), floor(v.A22), floor(v.A32)}
}
func (v *Mat4x3) PFloor() {
	v.A00 = floor(v.A00)
	v.A10 = floor(v.A10)
	v.A20 = floor(v.A20)
	v.A30 = floor(v.A30)
	v.A01 = floor(v.A01)
	v.A11 = floor(v.A11)
	v.A21 = floor(v.A21)
	v.A31 = floor(v.A31)
	v.A02 = floor(v.A02)
	v.A12 = floor(v.A12)
	v.A22 = floor(v.A22)
	v.A32 = floor(v.A32)
}

func (v Mat4x3) Ceil() Mat4x3 {
	return Mat4x3{ceil(v.A00), ceil(v.A10), ceil(v.A20), ceil(v.A30), ceil(v.A01), ceil(v.A11), ceil(v.A21), ceil(v.A31), ceil(v.A02), ceil(v.A12), ceil(v.A22), ceil(v.A32)}
}
func (v *Mat4x3) PCeil() {
	v.A00 = ceil(v.A00)
	v.A10 = ceil(v.A10)
	v.A20 = ceil(v.A20)
	v.A30 = ceil(v.A30)
	v.A01 = ceil(v.A01)
	v.A11 = ceil(v.A11)
	v.A21 = ceil(v.A21)
	v.A31 = ceil(v.A31)
	v.A02 = ceil(v.A02)
	v.A12 = ceil(v.A12)
	v.A22 = ceil(v.A22)
	v.A32 = ceil(v.A32)
}

func (v Mat4x3) Round() Mat4x3 {
	return Mat4x3{round(v.A00), round(v.A10), round(v.A20), round(v.A30), round(v.A01), round(v.A11), round(v.A21), round(v.A31), round(v.A02), round(v.A12), round(v.A22), round(v.A32)}
}
func (v *Mat4x3) PRound() {
	v.A00 = round(v.A00)
	v.A10 = round(v.A10)
	v.A20 = round(v.A20)
	v.A30 = round(v.A30)
	v.A01 = round(v.A01)
	v.A11 = round(v.A11)
	v.A21 = round(v.A21)
	v.A31 = round(v.A31)
	v.A02 = round(v.A02)
	v.A12 = round(v.A12)
	v.A22 = round(v.A22)
	v.A32 = round(v.A32)
}

func (v Mat4x3) Lerp(w Mat4x3, factor float32) Mat4x3 {
	return v.Mul(1 - factor).Add(w.Mul(factor))
}

func (v Mat4x3) Float64() math64.Mat4x3 {
	return math64.Mat4x3{
		float64(v.A00),
		float64(v.A10),
		float64(v.A20),
		float64(v.A30),
		float64(v.A01),
		float64(v.A11),
		float64(v.A21),
		float64(v.A31),
		float64(v.A02),
		float64(v.A12),
		float64(v.A22),
		float64(v.A32),
	}
}

func Mat4x3FromFloat64(v math64.Mat4x3) Mat4x3 {
	return Mat4x3{
		float32(v.A00),
		float32(v.A10),
		float32(v.A20),
		float32(v.A30),
		float32(v.A01),
		float32(v.A11),
		float32(v.A21),
		float32(v.A31),
		float32(v.A02),
		float32(v.A12),
		float32(v.A22),
		float32(v.A32),
	}
}
