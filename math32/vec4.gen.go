package math32

import (
	"gitlab.com/bytewright/gmath/math64"
)

type Vec4 struct {
	X float32
	Y float32
	Z float32
	W float32
}

func (v Vec4) Vec2() Vec2 {
	return Vec2{v.X, v.Y}
}

func (v Vec4) Vec3() Vec3 {
	return Vec3{v.X, v.Y, v.Z}
}

func (v Vec4) Sub(o Vec4) Vec4 {
	return Vec4{v.X - o.X, v.Y - o.Y, v.Z - o.Z, v.W - o.W}
}

func (v *Vec4) PSub(o Vec4) {
	v.X -= o.X
	v.Y -= o.Y
	v.Z -= o.Z
	v.W -= o.W
}

func (v Vec4) Add(o Vec4) Vec4 {
	return Vec4{v.X + o.X, v.Y + o.Y, v.Z + o.Z, v.W + o.W}
}

func (v *Vec4) PAdd(o Vec4) {
	v.X += o.X
	v.Y += o.Y
	v.Z += o.Z
	v.W += o.W
}

func (v Vec4) Div(d float32) Vec4 {
	return Vec4{v.X / d, v.Y / d, v.Z / d, v.W / d}
}

func (v *Vec4) PDiv(d float32) {
	v.X /= d
	v.Y /= d
	v.Z /= d
	v.W /= d
}

func (v Vec4) Mul(m float32) Vec4 {
	return Vec4{v.X * m, v.Y * m, v.Z * m, v.W * m}
}

func (v *Vec4) PMul(m float32) {
	v.X *= m
	v.Y *= m
	v.Z *= m
	v.W *= m
}

func (v Vec4) Negative() Vec4 {
	return Vec4{-v.X, -v.Y, -v.Z, -v.W}
}

func (v *Vec4) PNegative() {
	v.X = -v.X
	v.Y = -v.Y
	v.Z = -v.Z
	v.W = -v.W
}

func (v Vec4) Abs() Vec4 {
	if v.X < 0 {
		v.X = -v.X
	}
	if v.Y < 0 {
		v.Y = -v.Y
	}
	if v.Z < 0 {
		v.Z = -v.Z
	}
	if v.W < 0 {
		v.W = -v.W
	}

	return v
}

func (v *Vec4) PAbs() {
	if v.X < 0 {
		v.X = -v.X
	}
	if v.Y < 0 {
		v.Y = -v.Y
	}
	if v.Z < 0 {
		v.Z = -v.Z
	}
	if v.W < 0 {
		v.W = -v.W
	}

}

func (v Vec4) Floor() Vec4 {
	return Vec4{floor(v.X), floor(v.Y), floor(v.Z), floor(v.W)}
}
func (v *Vec4) PFloor() {
	v.X = floor(v.X)
	v.Y = floor(v.Y)
	v.Z = floor(v.Z)
	v.W = floor(v.W)
}

func (v Vec4) Ceil() Vec4 {
	return Vec4{ceil(v.X), ceil(v.Y), ceil(v.Z), ceil(v.W)}
}
func (v *Vec4) PCeil() {
	v.X = ceil(v.X)
	v.Y = ceil(v.Y)
	v.Z = ceil(v.Z)
	v.W = ceil(v.W)
}

func (v Vec4) Round() Vec4 {
	return Vec4{round(v.X), round(v.Y), round(v.Z), round(v.W)}
}
func (v *Vec4) PRound() {
	v.X = round(v.X)
	v.Y = round(v.Y)
	v.Z = round(v.Z)
	v.W = round(v.W)
}

func (v Vec4) Lerp(w Vec4, factor float32) Vec4 {
	return v.Mul(1 - factor).Add(w.Mul(factor))
}

// Min returns the vector with the smaller value for each component
func (v Vec4) Min(o Vec4) Vec4 {
	if o.X < v.X {
		v.X = o.X
	}
	if o.Y < v.Y {
		v.Y = o.Y
	}
	if o.Z < v.Z {
		v.Z = o.Z
	}
	if o.W < v.W {
		v.W = o.W
	}

	return v
}

// Max returns the vector with the bigger value for each component
func (v Vec4) Max(o Vec4) Vec4 {
	if o.X > v.X {
		v.X = o.X
	}
	if o.Y > v.Y {
		v.Y = o.Y
	}
	if o.Z > v.Z {
		v.Z = o.Z
	}
	if o.W > v.W {
		v.W = o.W
	}

	return v
}

func (v Vec4) Dot(o Vec4) float32 {
	return v.X*o.X + v.Y*o.Y + v.Z*o.Z + v.W*o.W
}

func (v Vec4) LenSqr() float32 {
	return v.X*v.X + v.Y*v.Y + v.Z*v.Z + v.W*v.W
}

func (v Vec4) Len() float32 {
	return implSqrt(v.X*v.X + v.Y*v.Y + v.Z*v.Z + v.W*v.W)
}

// LenTaxiCab returns the taxicab length of the vector, sum of all components
func (v Vec4) LenTaxiCab() float32 {
	return Abs(v.X) + Abs(v.Y) + Abs(v.Z) + Abs(v.W)
}

// LenChebyshev returns the chebyshev distance which is the max of all components
func (v Vec4) LenChebyshev() float32 {
	max := float32(0)

	if Abs(v.X) > max {
		max = Abs(v.X)
	}
	if Abs(v.Y) > max {
		max = Abs(v.Y)
	}
	if Abs(v.Z) > max {
		max = Abs(v.Z)
	}
	if Abs(v.W) > max {
		max = Abs(v.W)
	}

	return max
}

func (v Vec4) DistanceTo(o Vec4) float32 {
	return o.Sub(v).Len()
}

func (v Vec4) TaxiCabDistanceTo(o Vec4) float32 {
	return o.Sub(v).LenTaxiCab()
}

func (v Vec4) ChebyshevDistanceTo(o Vec4) float32 {
	return o.Sub(v).LenChebyshev()
}

func (v *Vec4) PLen() float32 {
	return implSqrt(v.X*v.X + v.Y*v.Y + v.Z*v.Z + v.W*v.W)
}

func (v Vec4) Normalize() Vec4 {
	l := 1.0 / v.Len()
	return Vec4{v.X * l, v.Y * l, v.Z * l, v.W * l}
}
func (v *Vec4) PNormalize() {
	l := 1.0 / v.PLen()
	v.X *= l
	v.Y *= l
	v.Z *= l
	v.W *= l
}

func (v Vec4) Mul4(o Vec4) Vec4 {
	return Vec4{v.X * o.X, v.Y * o.Y, v.Z * o.Z, v.W * o.W}
}

func (v *Vec4) PMul4(o Vec4) {
	v.X *= o.X
	v.Y *= o.Y
	v.Z *= o.Z
	v.W *= o.W
}

func (v Vec4) Div4(o Vec4) Vec4 {
	return Vec4{v.X / o.X, v.Y / o.Y, v.Z / o.Z, v.W / o.W}
}

func (v *Vec4) PDiv4(o Vec4) {
	v.X /= o.X
	v.Y /= o.Y
	v.Z /= o.Z
	v.W /= o.W
}

// ProjectOnPlane projects the vector onto the plane given by the normal n.
func (v Vec4) ProjectOnPlane(n Vec4) Vec4 {
	return v.Sub(n.Mul(n.Dot(v)))
}

func (v Vec4) Float64() math64.Vec4 {
	return math64.Vec4{
		float64(v.X),
		float64(v.Y),
		float64(v.Z),
		float64(v.W),
	}
}

func Vec4FromFloat64(v math64.Vec4) Vec4 {
	return Vec4{
		float32(v.X),
		float32(v.Y),
		float32(v.Z),
		float32(v.W),
	}
}
