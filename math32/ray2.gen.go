package math32

// Ray has origin and direction, direction is expected to be normalized
type Ray2 struct {
	Origin Vec2
	Dir    Vec2
}

// NewRay creates a new ray, normalizes direction
func NewRay2(origin, dir Vec2) Ray2 {
	return Ray2{Origin: origin, Dir: dir.Normalize()}
}
