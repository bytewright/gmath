package math32

import (
	"gitlab.com/bytewright/gmath/math64"
)

type Vec2 struct {
	X float32
	Y float32
}

func (v Vec2) Vec3(z float32) Vec3 {
	return Vec3{v.X, v.Y, z}
}

func (v Vec2) Vec4(z float32, w float32) Vec4 {
	return Vec4{v.X, v.Y, z, w}
}

func (v Vec2) Sub(o Vec2) Vec2 {
	return Vec2{v.X - o.X, v.Y - o.Y}
}

func (v *Vec2) PSub(o Vec2) {
	v.X -= o.X
	v.Y -= o.Y
}

func (v Vec2) Add(o Vec2) Vec2 {
	return Vec2{v.X + o.X, v.Y + o.Y}
}

func (v *Vec2) PAdd(o Vec2) {
	v.X += o.X
	v.Y += o.Y
}

func (v Vec2) Div(d float32) Vec2 {
	return Vec2{v.X / d, v.Y / d}
}

func (v *Vec2) PDiv(d float32) {
	v.X /= d
	v.Y /= d
}

func (v Vec2) Mul(m float32) Vec2 {
	return Vec2{v.X * m, v.Y * m}
}

func (v *Vec2) PMul(m float32) {
	v.X *= m
	v.Y *= m
}

func (v Vec2) Negative() Vec2 {
	return Vec2{-v.X, -v.Y}
}

func (v *Vec2) PNegative() {
	v.X = -v.X
	v.Y = -v.Y
}

func (v Vec2) Abs() Vec2 {
	if v.X < 0 {
		v.X = -v.X
	}
	if v.Y < 0 {
		v.Y = -v.Y
	}

	return v
}

func (v *Vec2) PAbs() {
	if v.X < 0 {
		v.X = -v.X
	}
	if v.Y < 0 {
		v.Y = -v.Y
	}

}

func (v Vec2) Floor() Vec2 {
	return Vec2{floor(v.X), floor(v.Y)}
}
func (v *Vec2) PFloor() {
	v.X = floor(v.X)
	v.Y = floor(v.Y)
}

func (v Vec2) Ceil() Vec2 {
	return Vec2{ceil(v.X), ceil(v.Y)}
}
func (v *Vec2) PCeil() {
	v.X = ceil(v.X)
	v.Y = ceil(v.Y)
}

func (v Vec2) Round() Vec2 {
	return Vec2{round(v.X), round(v.Y)}
}
func (v *Vec2) PRound() {
	v.X = round(v.X)
	v.Y = round(v.Y)
}

func (v Vec2) Lerp(w Vec2, factor float32) Vec2 {
	return v.Mul(1 - factor).Add(w.Mul(factor))
}

// Min returns the vector with the smaller value for each component
func (v Vec2) Min(o Vec2) Vec2 {
	if o.X < v.X {
		v.X = o.X
	}
	if o.Y < v.Y {
		v.Y = o.Y
	}

	return v
}

// Max returns the vector with the bigger value for each component
func (v Vec2) Max(o Vec2) Vec2 {
	if o.X > v.X {
		v.X = o.X
	}
	if o.Y > v.Y {
		v.Y = o.Y
	}

	return v
}

func (v Vec2) Dot(o Vec2) float32 {
	return v.X*o.X + v.Y*o.Y
}

func (v Vec2) LenSqr() float32 {
	return v.X*v.X + v.Y*v.Y
}

func (v Vec2) Len() float32 {
	return implSqrt(v.X*v.X + v.Y*v.Y)
}

// LenTaxiCab returns the taxicab length of the vector, sum of all components
func (v Vec2) LenTaxiCab() float32 {
	return Abs(v.X) + Abs(v.Y)
}

// LenChebyshev returns the chebyshev distance which is the max of all components
func (v Vec2) LenChebyshev() float32 {
	max := float32(0)

	if Abs(v.X) > max {
		max = Abs(v.X)
	}
	if Abs(v.Y) > max {
		max = Abs(v.Y)
	}

	return max
}

func (v Vec2) DistanceTo(o Vec2) float32 {
	return o.Sub(v).Len()
}

func (v Vec2) TaxiCabDistanceTo(o Vec2) float32 {
	return o.Sub(v).LenTaxiCab()
}

func (v Vec2) ChebyshevDistanceTo(o Vec2) float32 {
	return o.Sub(v).LenChebyshev()
}

func (v *Vec2) PLen() float32 {
	return implSqrt(v.X*v.X + v.Y*v.Y)
}

func (v Vec2) Normalize() Vec2 {
	l := 1.0 / v.Len()
	return Vec2{v.X * l, v.Y * l}
}
func (v *Vec2) PNormalize() {
	l := 1.0 / v.PLen()
	v.X *= l
	v.Y *= l
}

func (v Vec2) Mul2(o Vec2) Vec2 {
	return Vec2{v.X * o.X, v.Y * o.Y}
}

func (v *Vec2) PMul2(o Vec2) {
	v.X *= o.X
	v.Y *= o.Y
}

func (v Vec2) Div2(o Vec2) Vec2 {
	return Vec2{v.X / o.X, v.Y / o.Y}
}

func (v *Vec2) PDiv2(o Vec2) {
	v.X /= o.X
	v.Y /= o.Y
}

// ProjectOnPlane projects the vector onto the plane given by the normal n.
func (v Vec2) ProjectOnPlane(n Vec2) Vec2 {
	return v.Sub(n.Mul(n.Dot(v)))
}

func (v Vec2) Float64() math64.Vec2 {
	return math64.Vec2{
		float64(v.X),
		float64(v.Y),
	}
}

func Vec2FromFloat64(v math64.Vec2) Vec2 {
	return Vec2{
		float32(v.X),
		float32(v.Y),
	}
}
