package math32

import "math"

// Triangle2 has 3 vertices
type Triangle2 struct {
	A Vec2
	B Vec2
	C Vec2
}

func (r Triangle2) IsVecInside(vec Vec2) (Vec2, bool) {
	v0 := r.B.Sub(r.A)
	v1 := r.C.Sub(r.A)
	v2 := vec.Sub(r.A)

	d00 := v0.Dot(v0)
	d01 := v0.Dot(v1)
	d11 := v1.Dot(v1)
	d20 := v2.Dot(v0)
	d21 := v2.Dot(v1)
	denom := d00*d11 - d01*d01

	u := (d11*d20 - d01*d21) / denom
	v := (d00*d21 - d01*d20) / denom

	if u >= 0 && v >= 0 && u+v <= 1 {
		return Vec2{X: u, Y: v}, true
	}
	return Vec2{X: u, Y: v}, false
}

func (t Triangle2) DistanceToVec(vec Vec2) float32 {
	vToLDistance := func(a, b, p Vec2) float32 {
		ab := b.Sub(a)
		ap := p.Sub(a)
		t := ap.Dot(ab) / ab.Dot(ab)

		if t < 0 || t > 1 {
			return float32(math.Inf(1)) // Outside the segment
		}

		projection := a.Add(ab.Mul(t))
		return p.Sub(projection).Len()
	}

	min := t.A.Sub(vec).Len()
	min = Min(min, t.B.Sub(vec).Len())
	min = Min(min, t.C.Sub(vec).Len())

	min = Min(min, vToLDistance(t.A, t.B, vec))
	min = Min(min, vToLDistance(t.B, t.C, vec))
	min = Min(min, vToLDistance(t.C, t.A, vec))

	return min
}

func (r Triangle2) Barycentric(p Vec2) Vec3 {
	v0 := r.B.Sub(r.A)
	v1 := r.C.Sub(r.A)
	v2 := p.Sub(r.A)

	// Compute dot products
	d00 := v0.Dot(v0)
	d01 := v0.Dot(v1)
	d11 := v1.Dot(v1)
	d20 := v2.Dot(v0)
	d21 := v2.Dot(v1)

	// Compute denominator
	denom := d00*d11 - d01*d01

	// Compute barycentric coordinates
	beta := (d11*d20 - d01*d21) / denom
	gamma := (d00*d21 - d01*d20) / denom
	alpha := 1.0 - beta - gamma

	return Vec3{alpha, beta, gamma}
}
