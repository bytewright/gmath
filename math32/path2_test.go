package math32_test

import (
	"fmt"
	"testing"

	"github.com/akabio/expect"
	"gitlab.com/bytewright/gmath/canvas"
	"gitlab.com/bytewright/gmath/math32"
	"gitlab.com/bytewright/gmath/math64"
)

var p1 = math32.Path2{
	{1.000000, 1.000000},
	{-1.000000, 1.000000},
	{-1.466602, 0.272551},
	{-2.289518, 0.374355},
	{-2.416773, 0.671283},
	{-3.392395, 0.382838},
	{-3.027597, -0.312823},
	{-3.095467, 0.060459},
	{-2.823989, 0.221649},
	{-2.628865, -0.143149},
	{-0.770941, -1.313896},
	{-0.728523, -0.635202},
	{0.230132, -0.966065},
	{3.623600, -0.558849},
	{1.875964, -0.032862},
	{0.518577, -0.304339},
	{1.240956, 0.7544028},
}

func TestPath2Normals(t *testing.T) {
	cv := canvas.New(1000, 500)
	cv.Translate(math64.Vec2{5, 1.5})
	cv.Scale(math64.Vec2{100, 100})

	cv.Stroke32Path2(p1, math32.Vec4{0.4, 0.4, 0.4, 1}, 0.5)

	for i := range p1 {
		f := p1.Get(i).Float64()
		t := f.Add(p1.GetNormal(i).Float64())

		cv.StrokePath2(math64.Path2{f, t}, math32.Vec4{1, 0.2, 0.2, 1}, 1)
	}

	err := cv.SaveAsPNG("testPathNorm2d.png")
	if err != nil {
		t.Fatal(err)
	}
}

func TestPath3Normals(t *testing.T) {
	path := math32.Path3{}
	for _, e := range p1 {
		path = append(path, e.Vec3(0))
	}

	cv := canvas.New(1000, 500)
	cv.Translate(math64.Vec2{5, 1.5})
	cv.Scale(math64.Vec2{100, 100})

	cv.Stroke32Path3(path, math32.Vec4{0.4, 0.4, 0.4, 1}, 1)

	for i := range path {
		f := path.Get(i).Float64().Vec2()
		t := f.Add(path.GetNormal(i, math32.Vec3{0, 0, 1}).Float64().Mul(30).Vec2())

		cv.StrokePath2(math64.Path2{f, t}, math32.Vec4{1, 0.2, 0.2, 1}, 1)
	}

	err := cv.SaveAsPNG("testPathNorm3d.png")
	if err != nil {
		t.Fatal(err)
	}
}

func TestPath3Split(t *testing.T) {
	path := math32.Path3{}
	for _, e := range p1 {
		path = append(path, e.Vec3(0))
	}

	pa, pb := path.Split(2, 11, 40)

	fmt.Println(len(path), len(pa), len(pb))

	cv := canvas.New(1000, 500)
	cv.Translate(math64.Vec2{5, 1.5})
	cv.Scale(math64.Vec2{100, 100})

	cv.Stroke32Path3(pa, math32.Vec4{0.4, 0.4, 0.4, 1}, 1)
	for i := range pa {
		f := pa.Get(i).Float64().Vec2()
		t := f.Add(pa.GetNormal(i, math32.Vec3{0, 0, 1}).Float64().Mul(0.1).Vec2())

		cv.StrokePath2(math64.Path2{f, t}, math32.Vec4{1, 0.2, 0.2, 1}, 1)
	}

	cv.Translate(math64.Vec2{-10, 0})

	cv.Stroke32Path3(pb, math32.Vec4{0.4, 0.4, 0.4, 1}, 1)
	for i := range pb {
		f := pb.Get(i).Float64().Vec2()
		t := f.Add(pb.GetNormal(i, math32.Vec3{0, 0, 1}).Float64().Mul(0.1).Vec2())

		cv.StrokePath2(math64.Path2{f, t}, math32.Vec4{1, 0.2, 0.2, 1}, 1)
	}

	err := cv.SaveAsPNG("testPathSplit3d.png")
	if err != nil {
		t.Fatal(err)
	}

	if err != nil {
		t.Fatal(err)
	}
}

func TestPath2Area(t *testing.T) {
	expect.Value(t, "area", math32.Path2{
		{1, 1},
		{1, 2},
		{2, 1},
	}.Area()).ToBe(float32(0.5))
}
