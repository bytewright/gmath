package math32

import "math"

const epsilon = 0.000001

func implSqrt(i float32) float32 {
	return float32(math.Sqrt(float64(i)))
}

func sin(i float32) float32 {
	return float32(math.Sin(float64(i)))
}

func cos(i float32) float32 {
	return float32(math.Cos(float64(i)))
}

func floor(v float32) float32 {
	return float32(math.Floor(float64(v)))
}

func ceil(v float32) float32 {
	return float32(math.Ceil(float64(v)))
}

func round(v float32) float32 {
	return float32(math.Round(float64(v)))
}
