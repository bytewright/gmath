module gitlab.com/bytewright/gmath

go 1.20

require (
	github.com/akabio/expect v0.10.0
	github.com/llgcode/draw2d v0.0.0-20210904075650-80aa0a2a901d
	gitlab.com/akabio/fmtid v0.2.1
	gitlab.com/akabio/gogen v0.5.6
	golang.org/x/tools v0.11.0
)

require (
	github.com/antlr/antlr4/runtime/Go/antlr/v4 v4.0.0-20230321174746-8dcc6526cfb1 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/ghodss/yaml v1.0.0 // indirect
	github.com/golang/freetype v0.0.0-20170609003504-e2365dfdc4a0 // indirect
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/sergi/go-diff v1.2.0 // indirect
	golang.org/x/exp v0.0.0-20220722155223-a9213eeb770e // indirect
	golang.org/x/image v0.0.0-20180708004352-c73c2afc3b81 // indirect
	golang.org/x/mod v0.12.0 // indirect
	golang.org/x/sys v0.10.0 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
)
