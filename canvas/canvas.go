package canvas

import (
	"image"
	"image/color"
	"math"

	"github.com/llgcode/draw2d/draw2dimg"
	"gitlab.com/bytewright/gmath/math32"
	"gitlab.com/bytewright/gmath/math64"
	"gitlab.com/bytewright/gmath/mathi"
)

type Canvas struct {
	transformations []math64.Mat3
	image           *image.RGBA
	c               *draw2dimg.GraphicContext
}

func New(w, h int) *Canvas {
	img := image.NewRGBA(image.Rectangle{Max: image.Point{X: w, Y: h}})
	c := draw2dimg.NewGraphicContext(img)

	c.BeginPath()
	c.MoveTo(0, 0)
	c.LineTo(float64(w), 0)
	c.LineTo(float64(w), float64(h))
	c.LineTo(0, float64(h))
	c.Close()

	c.SetFillColor(color.Black)
	c.Fill()

	return &Canvas{
		transformations: []math64.Mat3{math64.Ident3()},
		image:           img,
		c:               c,
	}
}

func (c *Canvas) Translate(t math64.Vec2) {
	nt := math64.Translate2(t).Mul3(c.peek())
	c.transformations = append(c.transformations, nt)
}

func (c *Canvas) Scale(t math64.Vec2) {
	nt := math64.Scale2(t).Mul3(c.peek())
	c.transformations = append(c.transformations, nt)
}

func (c *Canvas) peek() math64.Mat3 {
	return c.transformations[len(c.transformations)-1]
}

func (c *Canvas) transform(v math64.Vec2) math64.Vec2 {
	return c.peek().Mul1(v.Vec3(1)).Vec2()
}

func (c *Canvas) StrokePath2(p math64.Path2, col math32.Vec4, lineWidth float64) {
	co := mathi.Vec4FromFloat32(col.Mul(255))
	c.c.SetStrokeColor(color.RGBA{uint8(co.X), uint8(co.Y), uint8(co.Z), uint8(co.W)})
	c.c.SetLineWidth(float64(lineWidth))

	c.c.BeginPath()

	for i := range p {
		f := c.transform(p.Get(i))
		t := c.transform(p.Get(i + 1))
		c.c.MoveTo(f.X, f.Y)
		c.c.LineTo(t.X, t.Y)
	}

	c.c.Stroke()
}

// Stroke32Path3 draws path using x and y coordinates
func (c *Canvas) Stroke32Path3(p math32.Path3, col math32.Vec4, lineWidth float64) {
	p64 := math64.Path2{}
	for _, v := range p {
		p64 = append(p64, v.Float64().Vec2())
	}
	c.StrokePath2(p64, col, lineWidth)
}

// Stroke32Path2
func (c *Canvas) Stroke32Path2(p math32.Path2, col math32.Vec4, lineWidth float64) {
	p64 := math64.Path2{}
	for _, v := range p {
		p64 = append(p64, v.Float64())
	}
	c.StrokePath2(p64, col, lineWidth)
}

func (c *Canvas) Stroke32Segment2(seg math32.Segment2, col math32.Vec4, lineWidth float64) {
	p64 := math64.Path2{}
	p64 = append(p64, seg.From.Float64())
	p64 = append(p64, seg.To.Float64())
	c.StrokePath2(p64, col, lineWidth)
}

func (c *Canvas) Stroke32Circle2(center math32.Vec2, r float32, col math32.Vec4, lineWidth float64) {
	c.Stroke64Circle2(center.Float64(), float64(r), col, lineWidth)
}

func (c *Canvas) Stroke64Circle2(center math64.Vec2, r float64, col math32.Vec4, lineWidth float64) {
	co := mathi.Vec4FromFloat32(col.Mul(255))
	c.c.SetStrokeColor(color.RGBA{uint8(co.X), uint8(co.Y), uint8(co.Z), uint8(co.W)})
	c.c.SetLineWidth(float64(lineWidth))

	c.c.BeginPath()

	c.c.ArcTo(center.X, center.Y, r, r, 0, math.Pi*2.0)

	c.c.Stroke()
}

func (c *Canvas) SaveAsPNG(path string) error {
	return draw2dimg.SaveToPngFile(path, c.image)
}
