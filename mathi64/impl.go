package mathi64

import "math"

const epsilon = 0.000001

func implSqrt(i int64) int64 {
	return int64(math.Sqrt(float64(i)))
}
