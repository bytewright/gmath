package mathi64

import (
	"gitlab.com/bytewright/gmath/math32"
	"gitlab.com/bytewright/gmath/math64"
)

type Mat3x4 struct {
	A00 int64
	A10 int64
	A20 int64
	A01 int64
	A11 int64
	A21 int64
	A02 int64
	A12 int64
	A22 int64
	A03 int64
	A13 int64
	A23 int64
}

func (v Mat3x4) Sub(o Mat3x4) Mat3x4 {
	return Mat3x4{v.A00 - o.A00, v.A10 - o.A10, v.A20 - o.A20, v.A01 - o.A01, v.A11 - o.A11, v.A21 - o.A21, v.A02 - o.A02, v.A12 - o.A12, v.A22 - o.A22, v.A03 - o.A03, v.A13 - o.A13, v.A23 - o.A23}
}

func (v *Mat3x4) PSub(o Mat3x4) {
	v.A00 -= o.A00
	v.A10 -= o.A10
	v.A20 -= o.A20
	v.A01 -= o.A01
	v.A11 -= o.A11
	v.A21 -= o.A21
	v.A02 -= o.A02
	v.A12 -= o.A12
	v.A22 -= o.A22
	v.A03 -= o.A03
	v.A13 -= o.A13
	v.A23 -= o.A23
}

func (v Mat3x4) Add(o Mat3x4) Mat3x4 {
	return Mat3x4{v.A00 + o.A00, v.A10 + o.A10, v.A20 + o.A20, v.A01 + o.A01, v.A11 + o.A11, v.A21 + o.A21, v.A02 + o.A02, v.A12 + o.A12, v.A22 + o.A22, v.A03 + o.A03, v.A13 + o.A13, v.A23 + o.A23}
}

func (v *Mat3x4) PAdd(o Mat3x4) {
	v.A00 += o.A00
	v.A10 += o.A10
	v.A20 += o.A20
	v.A01 += o.A01
	v.A11 += o.A11
	v.A21 += o.A21
	v.A02 += o.A02
	v.A12 += o.A12
	v.A22 += o.A22
	v.A03 += o.A03
	v.A13 += o.A13
	v.A23 += o.A23
}

func (v Mat3x4) Div(d int64) Mat3x4 {
	return Mat3x4{v.A00 / d, v.A10 / d, v.A20 / d, v.A01 / d, v.A11 / d, v.A21 / d, v.A02 / d, v.A12 / d, v.A22 / d, v.A03 / d, v.A13 / d, v.A23 / d}
}

func (v *Mat3x4) PDiv(d int64) {
	v.A00 /= d
	v.A10 /= d
	v.A20 /= d
	v.A01 /= d
	v.A11 /= d
	v.A21 /= d
	v.A02 /= d
	v.A12 /= d
	v.A22 /= d
	v.A03 /= d
	v.A13 /= d
	v.A23 /= d
}

func (v Mat3x4) Mul(m int64) Mat3x4 {
	return Mat3x4{v.A00 * m, v.A10 * m, v.A20 * m, v.A01 * m, v.A11 * m, v.A21 * m, v.A02 * m, v.A12 * m, v.A22 * m, v.A03 * m, v.A13 * m, v.A23 * m}
}

func (v *Mat3x4) PMul(m int64) {
	v.A00 *= m
	v.A10 *= m
	v.A20 *= m
	v.A01 *= m
	v.A11 *= m
	v.A21 *= m
	v.A02 *= m
	v.A12 *= m
	v.A22 *= m
	v.A03 *= m
	v.A13 *= m
	v.A23 *= m
}

func (v Mat3x4) Negative() Mat3x4 {
	return Mat3x4{-v.A00, -v.A10, -v.A20, -v.A01, -v.A11, -v.A21, -v.A02, -v.A12, -v.A22, -v.A03, -v.A13, -v.A23}
}

func (v *Mat3x4) PNegative() {
	v.A00 = -v.A00
	v.A10 = -v.A10
	v.A20 = -v.A20
	v.A01 = -v.A01
	v.A11 = -v.A11
	v.A21 = -v.A21
	v.A02 = -v.A02
	v.A12 = -v.A12
	v.A22 = -v.A22
	v.A03 = -v.A03
	v.A13 = -v.A13
	v.A23 = -v.A23
}

func (v Mat3x4) Abs() Mat3x4 {
	if v.A00 < 0 {
		v.A00 = -v.A00
	}
	if v.A10 < 0 {
		v.A10 = -v.A10
	}
	if v.A20 < 0 {
		v.A20 = -v.A20
	}
	if v.A01 < 0 {
		v.A01 = -v.A01
	}
	if v.A11 < 0 {
		v.A11 = -v.A11
	}
	if v.A21 < 0 {
		v.A21 = -v.A21
	}
	if v.A02 < 0 {
		v.A02 = -v.A02
	}
	if v.A12 < 0 {
		v.A12 = -v.A12
	}
	if v.A22 < 0 {
		v.A22 = -v.A22
	}
	if v.A03 < 0 {
		v.A03 = -v.A03
	}
	if v.A13 < 0 {
		v.A13 = -v.A13
	}
	if v.A23 < 0 {
		v.A23 = -v.A23
	}

	return v
}

func (v *Mat3x4) PAbs() {
	if v.A00 < 0 {
		v.A00 = -v.A00
	}
	if v.A10 < 0 {
		v.A10 = -v.A10
	}
	if v.A20 < 0 {
		v.A20 = -v.A20
	}
	if v.A01 < 0 {
		v.A01 = -v.A01
	}
	if v.A11 < 0 {
		v.A11 = -v.A11
	}
	if v.A21 < 0 {
		v.A21 = -v.A21
	}
	if v.A02 < 0 {
		v.A02 = -v.A02
	}
	if v.A12 < 0 {
		v.A12 = -v.A12
	}
	if v.A22 < 0 {
		v.A22 = -v.A22
	}
	if v.A03 < 0 {
		v.A03 = -v.A03
	}
	if v.A13 < 0 {
		v.A13 = -v.A13
	}
	if v.A23 < 0 {
		v.A23 = -v.A23
	}

}

func (v Mat3x4) Mul1(o Vec4) Vec3 {
	return Vec3{
		v.A00*o.X + v.A01*o.Y + v.A02*o.Z + v.A03*o.W,
		v.A10*o.X + v.A11*o.Y + v.A12*o.Z + v.A13*o.W,
		v.A20*o.X + v.A21*o.Y + v.A22*o.Z + v.A23*o.W,
	}
}

func (v Mat3x4) Mul2(o Mat4x2) Mat3x2 {
	return Mat3x2{
		v.A00*o.A00 + v.A01*o.A10 + v.A02*o.A20 + v.A03*o.A30,
		v.A10*o.A00 + v.A11*o.A10 + v.A12*o.A20 + v.A13*o.A30,
		v.A20*o.A00 + v.A21*o.A10 + v.A22*o.A20 + v.A23*o.A30,
		v.A00*o.A01 + v.A01*o.A11 + v.A02*o.A21 + v.A03*o.A31,
		v.A10*o.A01 + v.A11*o.A11 + v.A12*o.A21 + v.A13*o.A31,
		v.A20*o.A01 + v.A21*o.A11 + v.A22*o.A21 + v.A23*o.A31,
	}
}

func (v Mat3x4) Mul3(o Mat4x3) Mat3 {
	return Mat3{
		v.A00*o.A00 + v.A01*o.A10 + v.A02*o.A20 + v.A03*o.A30,
		v.A10*o.A00 + v.A11*o.A10 + v.A12*o.A20 + v.A13*o.A30,
		v.A20*o.A00 + v.A21*o.A10 + v.A22*o.A20 + v.A23*o.A30,
		v.A00*o.A01 + v.A01*o.A11 + v.A02*o.A21 + v.A03*o.A31,
		v.A10*o.A01 + v.A11*o.A11 + v.A12*o.A21 + v.A13*o.A31,
		v.A20*o.A01 + v.A21*o.A11 + v.A22*o.A21 + v.A23*o.A31,
		v.A00*o.A02 + v.A01*o.A12 + v.A02*o.A22 + v.A03*o.A32,
		v.A10*o.A02 + v.A11*o.A12 + v.A12*o.A22 + v.A13*o.A32,
		v.A20*o.A02 + v.A21*o.A12 + v.A22*o.A22 + v.A23*o.A32,
	}
}

func (v Mat3x4) Mul4(o Mat4) Mat3x4 {
	return Mat3x4{
		v.A00*o.A00 + v.A01*o.A10 + v.A02*o.A20 + v.A03*o.A30,
		v.A10*o.A00 + v.A11*o.A10 + v.A12*o.A20 + v.A13*o.A30,
		v.A20*o.A00 + v.A21*o.A10 + v.A22*o.A20 + v.A23*o.A30,
		v.A00*o.A01 + v.A01*o.A11 + v.A02*o.A21 + v.A03*o.A31,
		v.A10*o.A01 + v.A11*o.A11 + v.A12*o.A21 + v.A13*o.A31,
		v.A20*o.A01 + v.A21*o.A11 + v.A22*o.A21 + v.A23*o.A31,
		v.A00*o.A02 + v.A01*o.A12 + v.A02*o.A22 + v.A03*o.A32,
		v.A10*o.A02 + v.A11*o.A12 + v.A12*o.A22 + v.A13*o.A32,
		v.A20*o.A02 + v.A21*o.A12 + v.A22*o.A22 + v.A23*o.A32,
		v.A00*o.A03 + v.A01*o.A13 + v.A02*o.A23 + v.A03*o.A33,
		v.A10*o.A03 + v.A11*o.A13 + v.A12*o.A23 + v.A13*o.A33,
		v.A20*o.A03 + v.A21*o.A13 + v.A22*o.A23 + v.A23*o.A33,
	}
}

func (v Mat3x4) Float32() math32.Mat3x4 {
	return math32.Mat3x4{
		float32(v.A00),
		float32(v.A10),
		float32(v.A20),
		float32(v.A01),
		float32(v.A11),
		float32(v.A21),
		float32(v.A02),
		float32(v.A12),
		float32(v.A22),
		float32(v.A03),
		float32(v.A13),
		float32(v.A23),
	}
}

func Mat3x4FromFloat32(v math32.Mat3x4) Mat3x4 {
	return Mat3x4{
		int64(v.A00),
		int64(v.A10),
		int64(v.A20),
		int64(v.A01),
		int64(v.A11),
		int64(v.A21),
		int64(v.A02),
		int64(v.A12),
		int64(v.A22),
		int64(v.A03),
		int64(v.A13),
		int64(v.A23),
	}
}

func (v Mat3x4) Float64() math64.Mat3x4 {
	return math64.Mat3x4{
		float64(v.A00),
		float64(v.A10),
		float64(v.A20),
		float64(v.A01),
		float64(v.A11),
		float64(v.A21),
		float64(v.A02),
		float64(v.A12),
		float64(v.A22),
		float64(v.A03),
		float64(v.A13),
		float64(v.A23),
	}
}

func Mat3x4FromFloat64(v math64.Mat3x4) Mat3x4 {
	return Mat3x4{
		int64(v.A00),
		int64(v.A10),
		int64(v.A20),
		int64(v.A01),
		int64(v.A11),
		int64(v.A21),
		int64(v.A02),
		int64(v.A12),
		int64(v.A22),
		int64(v.A03),
		int64(v.A13),
		int64(v.A23),
	}
}
