package mathui16

import (
	"gitlab.com/bytewright/gmath/math32"
	"gitlab.com/bytewright/gmath/math64"
	"gitlab.com/bytewright/gmath/mathi"
	"gitlab.com/bytewright/gmath/mathi16"
	"gitlab.com/bytewright/gmath/mathi32"
	"gitlab.com/bytewright/gmath/mathi64"
	"gitlab.com/bytewright/gmath/mathui32"
)

type Mat2 struct {
	A00 uint16
	A10 uint16
	A01 uint16
	A11 uint16
}

func Ident2() Mat2 {
	return Mat2{1, 0, 0, 1}
}

func (v Mat2) Sub(o Mat2) Mat2 {
	return Mat2{v.A00 - o.A00, v.A10 - o.A10, v.A01 - o.A01, v.A11 - o.A11}
}

func (v *Mat2) PSub(o Mat2) {
	v.A00 -= o.A00
	v.A10 -= o.A10
	v.A01 -= o.A01
	v.A11 -= o.A11
}

func (v Mat2) Add(o Mat2) Mat2 {
	return Mat2{v.A00 + o.A00, v.A10 + o.A10, v.A01 + o.A01, v.A11 + o.A11}
}

func (v *Mat2) PAdd(o Mat2) {
	v.A00 += o.A00
	v.A10 += o.A10
	v.A01 += o.A01
	v.A11 += o.A11
}

func (v Mat2) Div(d uint16) Mat2 {
	return Mat2{v.A00 / d, v.A10 / d, v.A01 / d, v.A11 / d}
}

func (v *Mat2) PDiv(d uint16) {
	v.A00 /= d
	v.A10 /= d
	v.A01 /= d
	v.A11 /= d
}

func (v Mat2) Mul(m uint16) Mat2 {
	return Mat2{v.A00 * m, v.A10 * m, v.A01 * m, v.A11 * m}
}

func (v *Mat2) PMul(m uint16) {
	v.A00 *= m
	v.A10 *= m
	v.A01 *= m
	v.A11 *= m
}

func (v Mat2) Negative() Mat2 {
	return Mat2{-v.A00, -v.A10, -v.A01, -v.A11}
}

func (v *Mat2) PNegative() {
	v.A00 = -v.A00
	v.A10 = -v.A10
	v.A01 = -v.A01
	v.A11 = -v.A11
}

func (v Mat2) Abs() Mat2 {
	if v.A00 < 0 {
		v.A00 = -v.A00
	}
	if v.A10 < 0 {
		v.A10 = -v.A10
	}
	if v.A01 < 0 {
		v.A01 = -v.A01
	}
	if v.A11 < 0 {
		v.A11 = -v.A11
	}

	return v
}

func (v *Mat2) PAbs() {
	if v.A00 < 0 {
		v.A00 = -v.A00
	}
	if v.A10 < 0 {
		v.A10 = -v.A10
	}
	if v.A01 < 0 {
		v.A01 = -v.A01
	}
	if v.A11 < 0 {
		v.A11 = -v.A11
	}

}

func (v Mat2) Mul1(o Vec2) Vec2 {
	return Vec2{
		v.A00*o.X + v.A01*o.Y,
		v.A10*o.X + v.A11*o.Y,
	}
}

func (v Mat2) Mul2(o Mat2) Mat2 {
	return Mat2{
		v.A00*o.A00 + v.A01*o.A10,
		v.A10*o.A00 + v.A11*o.A10,
		v.A00*o.A01 + v.A01*o.A11,
		v.A10*o.A01 + v.A11*o.A11,
	}
}

func (v Mat2) Mul3(o Mat2x3) Mat2x3 {
	return Mat2x3{
		v.A00*o.A00 + v.A01*o.A10,
		v.A10*o.A00 + v.A11*o.A10,
		v.A00*o.A01 + v.A01*o.A11,
		v.A10*o.A01 + v.A11*o.A11,
		v.A00*o.A02 + v.A01*o.A12,
		v.A10*o.A02 + v.A11*o.A12,
	}
}

func (v Mat2) Mul4(o Mat2x4) Mat2x4 {
	return Mat2x4{
		v.A00*o.A00 + v.A01*o.A10,
		v.A10*o.A00 + v.A11*o.A10,
		v.A00*o.A01 + v.A01*o.A11,
		v.A10*o.A01 + v.A11*o.A11,
		v.A00*o.A02 + v.A01*o.A12,
		v.A10*o.A02 + v.A11*o.A12,
		v.A00*o.A03 + v.A01*o.A13,
		v.A10*o.A03 + v.A11*o.A13,
	}
}

func (v Mat2) Int16() mathi16.Mat2 {
	return mathi16.Mat2{
		int16(v.A00),
		int16(v.A10),
		int16(v.A01),
		int16(v.A11),
	}
}

func Mat2FromInt16(v mathi16.Mat2) Mat2 {
	return Mat2{
		uint16(v.A00),
		uint16(v.A10),
		uint16(v.A01),
		uint16(v.A11),
	}
}

func (v Mat2) Uint32() mathui32.Mat2 {
	return mathui32.Mat2{
		uint32(v.A00),
		uint32(v.A10),
		uint32(v.A01),
		uint32(v.A11),
	}
}

func Mat2FromUint32(v mathui32.Mat2) Mat2 {
	return Mat2{
		uint16(v.A00),
		uint16(v.A10),
		uint16(v.A01),
		uint16(v.A11),
	}
}

func (v Mat2) Int32() mathi32.Mat2 {
	return mathi32.Mat2{
		int32(v.A00),
		int32(v.A10),
		int32(v.A01),
		int32(v.A11),
	}
}

func Mat2FromInt32(v mathi32.Mat2) Mat2 {
	return Mat2{
		uint16(v.A00),
		uint16(v.A10),
		uint16(v.A01),
		uint16(v.A11),
	}
}

func (v Mat2) Int() mathi.Mat2 {
	return mathi.Mat2{
		int(v.A00),
		int(v.A10),
		int(v.A01),
		int(v.A11),
	}
}

func Mat2FromInt(v mathi.Mat2) Mat2 {
	return Mat2{
		uint16(v.A00),
		uint16(v.A10),
		uint16(v.A01),
		uint16(v.A11),
	}
}

func (v Mat2) Int64() mathi64.Mat2 {
	return mathi64.Mat2{
		int64(v.A00),
		int64(v.A10),
		int64(v.A01),
		int64(v.A11),
	}
}

func Mat2FromInt64(v mathi64.Mat2) Mat2 {
	return Mat2{
		uint16(v.A00),
		uint16(v.A10),
		uint16(v.A01),
		uint16(v.A11),
	}
}

func (v Mat2) Float32() math32.Mat2 {
	return math32.Mat2{
		float32(v.A00),
		float32(v.A10),
		float32(v.A01),
		float32(v.A11),
	}
}

func Mat2FromFloat32(v math32.Mat2) Mat2 {
	return Mat2{
		uint16(v.A00),
		uint16(v.A10),
		uint16(v.A01),
		uint16(v.A11),
	}
}

func (v Mat2) Float64() math64.Mat2 {
	return math64.Mat2{
		float64(v.A00),
		float64(v.A10),
		float64(v.A01),
		float64(v.A11),
	}
}

func Mat2FromFloat64(v math64.Mat2) Mat2 {
	return Mat2{
		uint16(v.A00),
		uint16(v.A10),
		uint16(v.A01),
		uint16(v.A11),
	}
}
