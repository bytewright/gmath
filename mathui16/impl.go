package mathui16

import "math"

const epsilon = 0.000001

func implSqrt(i uint16) uint16 {
	return uint16(math.Sqrt(float64(i)))
}
