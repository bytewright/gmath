package mathui16

import (
	"gitlab.com/bytewright/gmath/math32"
	"gitlab.com/bytewright/gmath/math64"
	"gitlab.com/bytewright/gmath/mathi"
	"gitlab.com/bytewright/gmath/mathi16"
	"gitlab.com/bytewright/gmath/mathi32"
	"gitlab.com/bytewright/gmath/mathi64"
	"gitlab.com/bytewright/gmath/mathui32"
)

type Mat3 struct {
	A00 uint16
	A10 uint16
	A20 uint16
	A01 uint16
	A11 uint16
	A21 uint16
	A02 uint16
	A12 uint16
	A22 uint16
}

func Ident3() Mat3 {
	return Mat3{1, 0, 0, 0, 1, 0, 0, 0, 1}
}

func (v Mat3) Sub(o Mat3) Mat3 {
	return Mat3{v.A00 - o.A00, v.A10 - o.A10, v.A20 - o.A20, v.A01 - o.A01, v.A11 - o.A11, v.A21 - o.A21, v.A02 - o.A02, v.A12 - o.A12, v.A22 - o.A22}
}

func (v *Mat3) PSub(o Mat3) {
	v.A00 -= o.A00
	v.A10 -= o.A10
	v.A20 -= o.A20
	v.A01 -= o.A01
	v.A11 -= o.A11
	v.A21 -= o.A21
	v.A02 -= o.A02
	v.A12 -= o.A12
	v.A22 -= o.A22
}

func (v Mat3) Add(o Mat3) Mat3 {
	return Mat3{v.A00 + o.A00, v.A10 + o.A10, v.A20 + o.A20, v.A01 + o.A01, v.A11 + o.A11, v.A21 + o.A21, v.A02 + o.A02, v.A12 + o.A12, v.A22 + o.A22}
}

func (v *Mat3) PAdd(o Mat3) {
	v.A00 += o.A00
	v.A10 += o.A10
	v.A20 += o.A20
	v.A01 += o.A01
	v.A11 += o.A11
	v.A21 += o.A21
	v.A02 += o.A02
	v.A12 += o.A12
	v.A22 += o.A22
}

func (v Mat3) Div(d uint16) Mat3 {
	return Mat3{v.A00 / d, v.A10 / d, v.A20 / d, v.A01 / d, v.A11 / d, v.A21 / d, v.A02 / d, v.A12 / d, v.A22 / d}
}

func (v *Mat3) PDiv(d uint16) {
	v.A00 /= d
	v.A10 /= d
	v.A20 /= d
	v.A01 /= d
	v.A11 /= d
	v.A21 /= d
	v.A02 /= d
	v.A12 /= d
	v.A22 /= d
}

func (v Mat3) Mul(m uint16) Mat3 {
	return Mat3{v.A00 * m, v.A10 * m, v.A20 * m, v.A01 * m, v.A11 * m, v.A21 * m, v.A02 * m, v.A12 * m, v.A22 * m}
}

func (v *Mat3) PMul(m uint16) {
	v.A00 *= m
	v.A10 *= m
	v.A20 *= m
	v.A01 *= m
	v.A11 *= m
	v.A21 *= m
	v.A02 *= m
	v.A12 *= m
	v.A22 *= m
}

func (v Mat3) Negative() Mat3 {
	return Mat3{-v.A00, -v.A10, -v.A20, -v.A01, -v.A11, -v.A21, -v.A02, -v.A12, -v.A22}
}

func (v *Mat3) PNegative() {
	v.A00 = -v.A00
	v.A10 = -v.A10
	v.A20 = -v.A20
	v.A01 = -v.A01
	v.A11 = -v.A11
	v.A21 = -v.A21
	v.A02 = -v.A02
	v.A12 = -v.A12
	v.A22 = -v.A22
}

func (v Mat3) Abs() Mat3 {
	if v.A00 < 0 {
		v.A00 = -v.A00
	}
	if v.A10 < 0 {
		v.A10 = -v.A10
	}
	if v.A20 < 0 {
		v.A20 = -v.A20
	}
	if v.A01 < 0 {
		v.A01 = -v.A01
	}
	if v.A11 < 0 {
		v.A11 = -v.A11
	}
	if v.A21 < 0 {
		v.A21 = -v.A21
	}
	if v.A02 < 0 {
		v.A02 = -v.A02
	}
	if v.A12 < 0 {
		v.A12 = -v.A12
	}
	if v.A22 < 0 {
		v.A22 = -v.A22
	}

	return v
}

func (v *Mat3) PAbs() {
	if v.A00 < 0 {
		v.A00 = -v.A00
	}
	if v.A10 < 0 {
		v.A10 = -v.A10
	}
	if v.A20 < 0 {
		v.A20 = -v.A20
	}
	if v.A01 < 0 {
		v.A01 = -v.A01
	}
	if v.A11 < 0 {
		v.A11 = -v.A11
	}
	if v.A21 < 0 {
		v.A21 = -v.A21
	}
	if v.A02 < 0 {
		v.A02 = -v.A02
	}
	if v.A12 < 0 {
		v.A12 = -v.A12
	}
	if v.A22 < 0 {
		v.A22 = -v.A22
	}

}

func (v Mat3) Mul1(o Vec3) Vec3 {
	return Vec3{
		v.A00*o.X + v.A01*o.Y + v.A02*o.Z,
		v.A10*o.X + v.A11*o.Y + v.A12*o.Z,
		v.A20*o.X + v.A21*o.Y + v.A22*o.Z,
	}
}

func (v Mat3) Mul2(o Mat3x2) Mat3x2 {
	return Mat3x2{
		v.A00*o.A00 + v.A01*o.A10 + v.A02*o.A20,
		v.A10*o.A00 + v.A11*o.A10 + v.A12*o.A20,
		v.A20*o.A00 + v.A21*o.A10 + v.A22*o.A20,
		v.A00*o.A01 + v.A01*o.A11 + v.A02*o.A21,
		v.A10*o.A01 + v.A11*o.A11 + v.A12*o.A21,
		v.A20*o.A01 + v.A21*o.A11 + v.A22*o.A21,
	}
}

func (v Mat3) Mul3(o Mat3) Mat3 {
	return Mat3{
		v.A00*o.A00 + v.A01*o.A10 + v.A02*o.A20,
		v.A10*o.A00 + v.A11*o.A10 + v.A12*o.A20,
		v.A20*o.A00 + v.A21*o.A10 + v.A22*o.A20,
		v.A00*o.A01 + v.A01*o.A11 + v.A02*o.A21,
		v.A10*o.A01 + v.A11*o.A11 + v.A12*o.A21,
		v.A20*o.A01 + v.A21*o.A11 + v.A22*o.A21,
		v.A00*o.A02 + v.A01*o.A12 + v.A02*o.A22,
		v.A10*o.A02 + v.A11*o.A12 + v.A12*o.A22,
		v.A20*o.A02 + v.A21*o.A12 + v.A22*o.A22,
	}
}

func (v Mat3) Mul4(o Mat3x4) Mat3x4 {
	return Mat3x4{
		v.A00*o.A00 + v.A01*o.A10 + v.A02*o.A20,
		v.A10*o.A00 + v.A11*o.A10 + v.A12*o.A20,
		v.A20*o.A00 + v.A21*o.A10 + v.A22*o.A20,
		v.A00*o.A01 + v.A01*o.A11 + v.A02*o.A21,
		v.A10*o.A01 + v.A11*o.A11 + v.A12*o.A21,
		v.A20*o.A01 + v.A21*o.A11 + v.A22*o.A21,
		v.A00*o.A02 + v.A01*o.A12 + v.A02*o.A22,
		v.A10*o.A02 + v.A11*o.A12 + v.A12*o.A22,
		v.A20*o.A02 + v.A21*o.A12 + v.A22*o.A22,
		v.A00*o.A03 + v.A01*o.A13 + v.A02*o.A23,
		v.A10*o.A03 + v.A11*o.A13 + v.A12*o.A23,
		v.A20*o.A03 + v.A21*o.A13 + v.A22*o.A23,
	}
}

func (v Mat3) Int16() mathi16.Mat3 {
	return mathi16.Mat3{
		int16(v.A00),
		int16(v.A10),
		int16(v.A20),
		int16(v.A01),
		int16(v.A11),
		int16(v.A21),
		int16(v.A02),
		int16(v.A12),
		int16(v.A22),
	}
}

func Mat3FromInt16(v mathi16.Mat3) Mat3 {
	return Mat3{
		uint16(v.A00),
		uint16(v.A10),
		uint16(v.A20),
		uint16(v.A01),
		uint16(v.A11),
		uint16(v.A21),
		uint16(v.A02),
		uint16(v.A12),
		uint16(v.A22),
	}
}

func (v Mat3) Uint32() mathui32.Mat3 {
	return mathui32.Mat3{
		uint32(v.A00),
		uint32(v.A10),
		uint32(v.A20),
		uint32(v.A01),
		uint32(v.A11),
		uint32(v.A21),
		uint32(v.A02),
		uint32(v.A12),
		uint32(v.A22),
	}
}

func Mat3FromUint32(v mathui32.Mat3) Mat3 {
	return Mat3{
		uint16(v.A00),
		uint16(v.A10),
		uint16(v.A20),
		uint16(v.A01),
		uint16(v.A11),
		uint16(v.A21),
		uint16(v.A02),
		uint16(v.A12),
		uint16(v.A22),
	}
}

func (v Mat3) Int32() mathi32.Mat3 {
	return mathi32.Mat3{
		int32(v.A00),
		int32(v.A10),
		int32(v.A20),
		int32(v.A01),
		int32(v.A11),
		int32(v.A21),
		int32(v.A02),
		int32(v.A12),
		int32(v.A22),
	}
}

func Mat3FromInt32(v mathi32.Mat3) Mat3 {
	return Mat3{
		uint16(v.A00),
		uint16(v.A10),
		uint16(v.A20),
		uint16(v.A01),
		uint16(v.A11),
		uint16(v.A21),
		uint16(v.A02),
		uint16(v.A12),
		uint16(v.A22),
	}
}

func (v Mat3) Int() mathi.Mat3 {
	return mathi.Mat3{
		int(v.A00),
		int(v.A10),
		int(v.A20),
		int(v.A01),
		int(v.A11),
		int(v.A21),
		int(v.A02),
		int(v.A12),
		int(v.A22),
	}
}

func Mat3FromInt(v mathi.Mat3) Mat3 {
	return Mat3{
		uint16(v.A00),
		uint16(v.A10),
		uint16(v.A20),
		uint16(v.A01),
		uint16(v.A11),
		uint16(v.A21),
		uint16(v.A02),
		uint16(v.A12),
		uint16(v.A22),
	}
}

func (v Mat3) Int64() mathi64.Mat3 {
	return mathi64.Mat3{
		int64(v.A00),
		int64(v.A10),
		int64(v.A20),
		int64(v.A01),
		int64(v.A11),
		int64(v.A21),
		int64(v.A02),
		int64(v.A12),
		int64(v.A22),
	}
}

func Mat3FromInt64(v mathi64.Mat3) Mat3 {
	return Mat3{
		uint16(v.A00),
		uint16(v.A10),
		uint16(v.A20),
		uint16(v.A01),
		uint16(v.A11),
		uint16(v.A21),
		uint16(v.A02),
		uint16(v.A12),
		uint16(v.A22),
	}
}

func (v Mat3) Float32() math32.Mat3 {
	return math32.Mat3{
		float32(v.A00),
		float32(v.A10),
		float32(v.A20),
		float32(v.A01),
		float32(v.A11),
		float32(v.A21),
		float32(v.A02),
		float32(v.A12),
		float32(v.A22),
	}
}

func Mat3FromFloat32(v math32.Mat3) Mat3 {
	return Mat3{
		uint16(v.A00),
		uint16(v.A10),
		uint16(v.A20),
		uint16(v.A01),
		uint16(v.A11),
		uint16(v.A21),
		uint16(v.A02),
		uint16(v.A12),
		uint16(v.A22),
	}
}

func (v Mat3) Float64() math64.Mat3 {
	return math64.Mat3{
		float64(v.A00),
		float64(v.A10),
		float64(v.A20),
		float64(v.A01),
		float64(v.A11),
		float64(v.A21),
		float64(v.A02),
		float64(v.A12),
		float64(v.A22),
	}
}

func Mat3FromFloat64(v math64.Mat3) Mat3 {
	return Mat3{
		uint16(v.A00),
		uint16(v.A10),
		uint16(v.A20),
		uint16(v.A01),
		uint16(v.A11),
		uint16(v.A21),
		uint16(v.A02),
		uint16(v.A12),
		uint16(v.A22),
	}
}
