package main

var types = []Type{
	{P: 1, Go: "int8", Short: "i8", Bits: 8},
	{P: 2, Go: "uint16", Short: "ui16", Bits: 16, Unsigned: true},
	{P: 3, Go: "int16", Short: "i16", Bits: 16},
	{P: 4, Go: "uint32", Short: "ui32", Bits: 32, Unsigned: true},
	{P: 5, Go: "int32", Short: "i32", Bits: 32},
	{P: 6, Go: "int", Short: "i", Bits: 64},
	{P: 7, Go: "int64", Short: "i64", Bits: 64},
	{P: 8, Go: "float32", Short: "32", Bits: 32, Decimal: true},
	{P: 9, Go: "float64", Short: "64", Bits: 64, Decimal: true},
}

var components = []string{"x", "y", "z", "w"}

type Type struct {
	Go       string
	Short    string
	Decimal  bool
	Bits     int // bits is the size if known, if not known it's maximum size
	P        int // kinf od precision, can only convert to higher type
	Unsigned bool
}

type Component struct {
	I    int
	Name string
}

type ScalarModel struct {
	Type Type
}
