package main

import "fmt"

type MatModel struct {
	Type Type
	X    int
	Y    int
}

type MatComponent struct {
	I    int
	X    int
	Y    int
	Name string
}

func (vm MatModel) XRange() []int {
	xr := []int{}
	for x := 0; x < vm.X; x++ {
		xr = append(xr, x)
	}
	return xr
}

func (vm MatModel) YRange() []int {
	yr := []int{}
	for y := 0; y < vm.Y; y++ {
		yr = append(yr, y)
	}
	return yr
}

func (vm MatModel) Square() bool {
	return vm.X == vm.Y
}

func (vm MatModel) Vector() bool {
	return vm.X == 1
}

func (vm MatModel) Name() string {
	prefix := "Mat"
	if vm.X == 1 {
		prefix = "Vec"
	}
	name := fmt.Sprint(vm.Y)
	if vm.Y != vm.X && vm.X != 1 {
		name = fmt.Sprintf("%vx%v", vm.Y, vm.X)
	}
	return prefix + name
}

func (vm MatModel) OtherTypes() []Type {
	tps := []Type{}
	for _, t := range types {
		if t.Short != vm.Type.Short {
			tps = append(tps, t)
		}
	}
	return tps
}

func (vm MatModel) Components() []MatComponent {
	cmps := []MatComponent{}
	if vm.X == 1 {
		for y := 0; y < vm.Y; y++ {
			cmps = append(cmps, MatComponent{
				I: y,
				X: 1, Y: y,
				Name: components[y],
			})
		}
		return cmps
	}
	for x := 0; x < vm.X; x++ {
		for y := 0; y < vm.Y; y++ {
			cmps = append(cmps, MatComponent{
				I: x*vm.Y + y,
				X: x, Y: y,
				Name: fmt.Sprintf("a%v%v", y, x),
			})
		}
	}
	return cmps
}

func (vm MatModel) YVariations() []MatModel {
	cmps := []MatModel{}
	for i := 2; i <= 4; i++ {
		if i != vm.Y {
			vm.Y = i
			cmps = append(cmps, vm)
		}
	}
	return cmps
}

func (vm MatModel) Cross() []Cross {
	cmps := []Cross{}
	for i := 1; i <= 4; i++ {
		b := vm
		b.Y = vm.X
		b.X = i
		c := vm
		c.X = i
		cmps = append(cmps, Cross{
			A: vm,
			B: b,
			C: c,
		})
	}
	return cmps
}

type Cross struct {
	A, B, C MatModel
}
