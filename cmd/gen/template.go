package main

import (
	"golang.org/x/tools/imports"
)

// goImports takes the source and formats it and updates imports
func goImports(file, source string) (string, error) {
	result, err := imports.Process(file, []byte(source), &imports.Options{Comments: true})
	if err != nil {
		return "//  goimports failed, there are errors!\n" + source, nil
	}
	return string(result), nil
}
