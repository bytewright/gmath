package main

type BoxModel struct {
	Type Type
	D    int
}

func (vm BoxModel) Components() []Component {
	cmps := []Component{}
	for i := 0; i < vm.D; i++ {
		cmps = append(cmps, Component{I: i, Name: components[i]})
	}
	return cmps
}
