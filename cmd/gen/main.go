package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"strings"

	"gitlab.com/akabio/fmtid"
	"gitlab.com/akabio/gogen"
	"gitlab.com/akabio/gogen/parser"
)

func main() {
	err := process()
	if err != nil {
		log.Fatal(err)
	}
}

func process() error {
	fmt.Println("generate scalar math")
	for _, t := range types {
		err := parse("scalar.gg", fmt.Sprintf("math%v/scalar", t.Short), &ScalarModel{
			Type: t,
		})
		if err != nil {
			return err
		}
	}

	fmt.Println("generate box math")
	for _, t := range types {
		for _, d := range []int{2, 3} {
			err := parse("box.gg", fmt.Sprintf("math%v/box%v", t.Short, d), &BoxModel{
				D:    d,
				Type: t,
			})
			if err != nil {
				return err
			}
		}
	}

	fmt.Println("generate segment math")
	for _, t := range types {
		for _, d := range []int{2, 3} {
			err := parse("segment.gg", fmt.Sprintf("math%v/segment%v", t.Short, d), &SegmentModel{
				D:    d,
				Type: t,
			})
			if err != nil {
				return err
			}
		}
	}

	fmt.Println("generate ray math")
	for _, t := range types {
		if t.Decimal {
			for _, d := range []int{2, 3} {
				err := parse("ray.gg", fmt.Sprintf("math%v/ray%v", t.Short, d), &RayModel{
					D:    d,
					Type: t,
				})
				if err != nil {
					return err
				}
			}
		}
	}

	fmt.Println("generate triangle math")
	for _, t := range types {
		if t.Decimal {
			for _, d := range []int{2, 3} {
				err := parse("triangle.gg", fmt.Sprintf("math%v/triangle%v", t.Short, d), &TriangleModel{
					D:    d,
					Type: t,
				})
				if err != nil {
					return err
				}
			}
		}
	}

	fmt.Println("generate path math")
	for _, t := range types {
		for _, d := range []int{2, 3} {
			err := parse("path.gg", fmt.Sprintf("math%v/path%v", t.Short, d), &PathModel{
				D:    d,
				Type: t,
			})
			if err != nil {
				return err
			}
		}
	}

	fmt.Println("generate mat math")
	for _, t := range types {
		for _, x := range []int{1, 2, 3, 4} {
			for _, y := range []int{2, 3, 4} {
				mod := &MatModel{
					X:    x,
					Y:    y,
					Type: t,
				}

				err := parse("mat.gg",
					fmt.Sprintf("math%v/%v", t.Short, strings.ToLower(mod.Name())), mod)
				if err != nil {
					return err
				}
			}
		}
	}

	fmt.Println("generate other?? math")
	for _, t := range types {
		err := parse("other.gg", fmt.Sprintf("math%v/other", t.Short), &ScalarModel{
			Type: t,
		})
		if err != nil {
			return err
		}
	}

	return nil
}

var gCache = map[string]*gogen.AST{}

func parse(tpl string, target string, model interface{}) error {
	gg, has := gCache[tpl]
	if !has {
		vec, err := ioutil.ReadFile(filepath.Join("cmd/gen/", tpl))
		if err != nil {
			return err
		}

		gg, err = parser.Parse(string(vec), tpl, nil)
		if err != nil {
			return err
		}
		gCache[tpl] = gg
	}

	opts := []gogen.Option{}

	for name, fnc := range fmtid.ASCII.Formats {
		opts = append(opts, gogen.FilterOption(name, fnc))
	}

	result, err := gogen.Execute(gg, model, opts...)
	if err != nil {
		return err
	}

	result, err = goImports(target+".gen.go", result)
	if err != nil {
		return err
	}

	err = os.MkdirAll(filepath.Dir(target), 0o777)
	if err != nil {
		return err
	}

	return ioutil.WriteFile(target+".gen.go", []byte(result), 0o666)
}
