package mathi

type Box2 struct {
	From Vec2
	To   Vec2
}

// NewBox creates a new Orthothing in a normalized form,
// diagonals component always point up and right.
func NewBox2(from, to Vec2) Box2 {
	r := Box2{From: from, To: to}
	(&r).normalize()
	return r
}

func NewBox2FromSize(origin, size Vec2) Box2 {
	r := Box2{From: origin, To: origin.Add(size)}
	(&r).normalize()
	return r
}

func (r Box2) Center() Vec2 {
	return r.From.Add(r.To).Div(2)
}

func (r Box2) Size() Vec2 {
	return r.To.Sub(r.From).Abs()
}

func (r Box2) Translate(v Vec2) Box2 {
	return Box2{
		From: r.From.Add(v),
		To:   r.To.Add(v),
	}
}

// Contains checks if vec is inside, >= from and < to
func (r Box2) Contains(v Vec2) bool {
	r.normalize()
	return v.X >= r.From.X && v.X < r.To.X &&
		v.Y >= r.From.Y && v.Y < r.To.Y
}

// ContainsInclusive checks if vec is inside, >= from and <= to
func (r Box2) ContainsInclusive(v Vec2) bool {
	r.normalize()
	return v.X >= r.From.X && v.X <= r.To.X &&
		v.Y >= r.From.Y && v.Y <= r.To.Y
}

func (a Box2) Union(b Box2) Box2 {
	return Box2{
		From: a.From.Min(b.From),
		To:   a.To.Max(b.To),
	}
}

func (a Box2) Extend(v Vec2) Box2 {
	return Box2{
		From: a.From.Min(v),
		To:   a.To.Max(v),
	}
}

func (a Box2) Grow(v int) Box2 {
	a.normalize()

	a.From.X -= v
	a.To.X += v
	a.From.Y -= v
	a.To.Y += v
	return a
}

// Intersects checks if the two boxes overlap, they must both be normalized
func (a Box2) Intersects(b Box2) bool {
	return !(a.To.X <= b.From.X ||
		a.From.X >= b.To.X ||
		a.To.Y <= b.From.Y ||
		a.From.Y >= b.To.Y)
}

func (b Box2) Path() Path2 {
	p := Path2{}
	p = append(p,
		b.From,
		Vec2{b.To.X, b.From.Y},
		b.To,
		Vec2{b.From.X, b.To.Y},
	)
	return p
}

// Normalized returns the Box where the diagonal points up in each axis
func (r Box2) Normalized() Box2 {
	r.normalize()
	return r
}

// DistanceToVec gets the distance from the box' edges to the point.
// If the point is inside it returns 0
func (r Box2) DistanceToVec(point Vec2) int {
	over := point.Sub(r.To)
	under := r.From.Sub(point)
	return over.Max(under).Max(Vec2{}).Len()
}

// normalize changes the diagonal of the Box to point up in each axis
func (r *Box2) normalize() {
	if r.From.X > r.To.X {
		r.From.X, r.To.X = r.To.X, r.From.X
	}
	if r.From.Y > r.To.Y {
		r.From.Y, r.To.Y = r.To.Y, r.From.Y
	}
}
