package mathi

import "math"

const epsilon = 0.000001

func implSqrt(i int) int {
	return int(math.Sqrt(float64(i)))
}
