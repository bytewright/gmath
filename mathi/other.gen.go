package mathi

func Translate2(v Vec2) Mat3 {
	return Mat3{1, 0, 0, 0, 1, 0, v.X, v.Y, 1}
}

func Scale2(v Vec2) Mat3 {
	return Mat3{v.X, 0, 0, 0, v.Y, 0, 0, 0, 1}
}

func Translate3(v Vec3) Mat4 {
	return Mat4{1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, v.X, v.Y, v.Z, 1}
}

func Scale3(v Vec3) Mat4 {
	return Mat4{v.X, 0, 0, 0, 0, v.Y, 0, 0, 0, 0, v.Z, 0, 0, 0, 0, 1}
}
