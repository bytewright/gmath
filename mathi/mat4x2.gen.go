package mathi

import (
	"gitlab.com/bytewright/gmath/math32"
	"gitlab.com/bytewright/gmath/math64"
	"gitlab.com/bytewright/gmath/mathi64"
)

type Mat4x2 struct {
	A00 int
	A10 int
	A20 int
	A30 int
	A01 int
	A11 int
	A21 int
	A31 int
}

func (v Mat4x2) Sub(o Mat4x2) Mat4x2 {
	return Mat4x2{v.A00 - o.A00, v.A10 - o.A10, v.A20 - o.A20, v.A30 - o.A30, v.A01 - o.A01, v.A11 - o.A11, v.A21 - o.A21, v.A31 - o.A31}
}

func (v *Mat4x2) PSub(o Mat4x2) {
	v.A00 -= o.A00
	v.A10 -= o.A10
	v.A20 -= o.A20
	v.A30 -= o.A30
	v.A01 -= o.A01
	v.A11 -= o.A11
	v.A21 -= o.A21
	v.A31 -= o.A31
}

func (v Mat4x2) Add(o Mat4x2) Mat4x2 {
	return Mat4x2{v.A00 + o.A00, v.A10 + o.A10, v.A20 + o.A20, v.A30 + o.A30, v.A01 + o.A01, v.A11 + o.A11, v.A21 + o.A21, v.A31 + o.A31}
}

func (v *Mat4x2) PAdd(o Mat4x2) {
	v.A00 += o.A00
	v.A10 += o.A10
	v.A20 += o.A20
	v.A30 += o.A30
	v.A01 += o.A01
	v.A11 += o.A11
	v.A21 += o.A21
	v.A31 += o.A31
}

func (v Mat4x2) Div(d int) Mat4x2 {
	return Mat4x2{v.A00 / d, v.A10 / d, v.A20 / d, v.A30 / d, v.A01 / d, v.A11 / d, v.A21 / d, v.A31 / d}
}

func (v *Mat4x2) PDiv(d int) {
	v.A00 /= d
	v.A10 /= d
	v.A20 /= d
	v.A30 /= d
	v.A01 /= d
	v.A11 /= d
	v.A21 /= d
	v.A31 /= d
}

func (v Mat4x2) Mul(m int) Mat4x2 {
	return Mat4x2{v.A00 * m, v.A10 * m, v.A20 * m, v.A30 * m, v.A01 * m, v.A11 * m, v.A21 * m, v.A31 * m}
}

func (v *Mat4x2) PMul(m int) {
	v.A00 *= m
	v.A10 *= m
	v.A20 *= m
	v.A30 *= m
	v.A01 *= m
	v.A11 *= m
	v.A21 *= m
	v.A31 *= m
}

func (v Mat4x2) Negative() Mat4x2 {
	return Mat4x2{-v.A00, -v.A10, -v.A20, -v.A30, -v.A01, -v.A11, -v.A21, -v.A31}
}

func (v *Mat4x2) PNegative() {
	v.A00 = -v.A00
	v.A10 = -v.A10
	v.A20 = -v.A20
	v.A30 = -v.A30
	v.A01 = -v.A01
	v.A11 = -v.A11
	v.A21 = -v.A21
	v.A31 = -v.A31
}

func (v Mat4x2) Abs() Mat4x2 {
	if v.A00 < 0 {
		v.A00 = -v.A00
	}
	if v.A10 < 0 {
		v.A10 = -v.A10
	}
	if v.A20 < 0 {
		v.A20 = -v.A20
	}
	if v.A30 < 0 {
		v.A30 = -v.A30
	}
	if v.A01 < 0 {
		v.A01 = -v.A01
	}
	if v.A11 < 0 {
		v.A11 = -v.A11
	}
	if v.A21 < 0 {
		v.A21 = -v.A21
	}
	if v.A31 < 0 {
		v.A31 = -v.A31
	}

	return v
}

func (v *Mat4x2) PAbs() {
	if v.A00 < 0 {
		v.A00 = -v.A00
	}
	if v.A10 < 0 {
		v.A10 = -v.A10
	}
	if v.A20 < 0 {
		v.A20 = -v.A20
	}
	if v.A30 < 0 {
		v.A30 = -v.A30
	}
	if v.A01 < 0 {
		v.A01 = -v.A01
	}
	if v.A11 < 0 {
		v.A11 = -v.A11
	}
	if v.A21 < 0 {
		v.A21 = -v.A21
	}
	if v.A31 < 0 {
		v.A31 = -v.A31
	}

}

func (v Mat4x2) Mul1(o Vec2) Vec4 {
	return Vec4{
		v.A00*o.X + v.A01*o.Y,
		v.A10*o.X + v.A11*o.Y,
		v.A20*o.X + v.A21*o.Y,
		v.A30*o.X + v.A31*o.Y,
	}
}

func (v Mat4x2) Mul2(o Mat2) Mat4x2 {
	return Mat4x2{
		v.A00*o.A00 + v.A01*o.A10,
		v.A10*o.A00 + v.A11*o.A10,
		v.A20*o.A00 + v.A21*o.A10,
		v.A30*o.A00 + v.A31*o.A10,
		v.A00*o.A01 + v.A01*o.A11,
		v.A10*o.A01 + v.A11*o.A11,
		v.A20*o.A01 + v.A21*o.A11,
		v.A30*o.A01 + v.A31*o.A11,
	}
}

func (v Mat4x2) Mul3(o Mat2x3) Mat4x3 {
	return Mat4x3{
		v.A00*o.A00 + v.A01*o.A10,
		v.A10*o.A00 + v.A11*o.A10,
		v.A20*o.A00 + v.A21*o.A10,
		v.A30*o.A00 + v.A31*o.A10,
		v.A00*o.A01 + v.A01*o.A11,
		v.A10*o.A01 + v.A11*o.A11,
		v.A20*o.A01 + v.A21*o.A11,
		v.A30*o.A01 + v.A31*o.A11,
		v.A00*o.A02 + v.A01*o.A12,
		v.A10*o.A02 + v.A11*o.A12,
		v.A20*o.A02 + v.A21*o.A12,
		v.A30*o.A02 + v.A31*o.A12,
	}
}

func (v Mat4x2) Mul4(o Mat2x4) Mat4 {
	return Mat4{
		v.A00*o.A00 + v.A01*o.A10,
		v.A10*o.A00 + v.A11*o.A10,
		v.A20*o.A00 + v.A21*o.A10,
		v.A30*o.A00 + v.A31*o.A10,
		v.A00*o.A01 + v.A01*o.A11,
		v.A10*o.A01 + v.A11*o.A11,
		v.A20*o.A01 + v.A21*o.A11,
		v.A30*o.A01 + v.A31*o.A11,
		v.A00*o.A02 + v.A01*o.A12,
		v.A10*o.A02 + v.A11*o.A12,
		v.A20*o.A02 + v.A21*o.A12,
		v.A30*o.A02 + v.A31*o.A12,
		v.A00*o.A03 + v.A01*o.A13,
		v.A10*o.A03 + v.A11*o.A13,
		v.A20*o.A03 + v.A21*o.A13,
		v.A30*o.A03 + v.A31*o.A13,
	}
}

func (v Mat4x2) Int64() mathi64.Mat4x2 {
	return mathi64.Mat4x2{
		int64(v.A00),
		int64(v.A10),
		int64(v.A20),
		int64(v.A30),
		int64(v.A01),
		int64(v.A11),
		int64(v.A21),
		int64(v.A31),
	}
}

func Mat4x2FromInt64(v mathi64.Mat4x2) Mat4x2 {
	return Mat4x2{
		int(v.A00),
		int(v.A10),
		int(v.A20),
		int(v.A30),
		int(v.A01),
		int(v.A11),
		int(v.A21),
		int(v.A31),
	}
}

func (v Mat4x2) Float32() math32.Mat4x2 {
	return math32.Mat4x2{
		float32(v.A00),
		float32(v.A10),
		float32(v.A20),
		float32(v.A30),
		float32(v.A01),
		float32(v.A11),
		float32(v.A21),
		float32(v.A31),
	}
}

func Mat4x2FromFloat32(v math32.Mat4x2) Mat4x2 {
	return Mat4x2{
		int(v.A00),
		int(v.A10),
		int(v.A20),
		int(v.A30),
		int(v.A01),
		int(v.A11),
		int(v.A21),
		int(v.A31),
	}
}

func (v Mat4x2) Float64() math64.Mat4x2 {
	return math64.Mat4x2{
		float64(v.A00),
		float64(v.A10),
		float64(v.A20),
		float64(v.A30),
		float64(v.A01),
		float64(v.A11),
		float64(v.A21),
		float64(v.A31),
	}
}

func Mat4x2FromFloat64(v math64.Mat4x2) Mat4x2 {
	return Mat4x2{
		int(v.A00),
		int(v.A10),
		int(v.A20),
		int(v.A30),
		int(v.A01),
		int(v.A11),
		int(v.A21),
		int(v.A31),
	}
}
