package mathi

import (
	"gitlab.com/bytewright/gmath/math32"
	"gitlab.com/bytewright/gmath/math64"
	"gitlab.com/bytewright/gmath/mathi64"
)

type Mat2x3 struct {
	A00 int
	A10 int
	A01 int
	A11 int
	A02 int
	A12 int
}

func (v Mat2x3) Sub(o Mat2x3) Mat2x3 {
	return Mat2x3{v.A00 - o.A00, v.A10 - o.A10, v.A01 - o.A01, v.A11 - o.A11, v.A02 - o.A02, v.A12 - o.A12}
}

func (v *Mat2x3) PSub(o Mat2x3) {
	v.A00 -= o.A00
	v.A10 -= o.A10
	v.A01 -= o.A01
	v.A11 -= o.A11
	v.A02 -= o.A02
	v.A12 -= o.A12
}

func (v Mat2x3) Add(o Mat2x3) Mat2x3 {
	return Mat2x3{v.A00 + o.A00, v.A10 + o.A10, v.A01 + o.A01, v.A11 + o.A11, v.A02 + o.A02, v.A12 + o.A12}
}

func (v *Mat2x3) PAdd(o Mat2x3) {
	v.A00 += o.A00
	v.A10 += o.A10
	v.A01 += o.A01
	v.A11 += o.A11
	v.A02 += o.A02
	v.A12 += o.A12
}

func (v Mat2x3) Div(d int) Mat2x3 {
	return Mat2x3{v.A00 / d, v.A10 / d, v.A01 / d, v.A11 / d, v.A02 / d, v.A12 / d}
}

func (v *Mat2x3) PDiv(d int) {
	v.A00 /= d
	v.A10 /= d
	v.A01 /= d
	v.A11 /= d
	v.A02 /= d
	v.A12 /= d
}

func (v Mat2x3) Mul(m int) Mat2x3 {
	return Mat2x3{v.A00 * m, v.A10 * m, v.A01 * m, v.A11 * m, v.A02 * m, v.A12 * m}
}

func (v *Mat2x3) PMul(m int) {
	v.A00 *= m
	v.A10 *= m
	v.A01 *= m
	v.A11 *= m
	v.A02 *= m
	v.A12 *= m
}

func (v Mat2x3) Negative() Mat2x3 {
	return Mat2x3{-v.A00, -v.A10, -v.A01, -v.A11, -v.A02, -v.A12}
}

func (v *Mat2x3) PNegative() {
	v.A00 = -v.A00
	v.A10 = -v.A10
	v.A01 = -v.A01
	v.A11 = -v.A11
	v.A02 = -v.A02
	v.A12 = -v.A12
}

func (v Mat2x3) Abs() Mat2x3 {
	if v.A00 < 0 {
		v.A00 = -v.A00
	}
	if v.A10 < 0 {
		v.A10 = -v.A10
	}
	if v.A01 < 0 {
		v.A01 = -v.A01
	}
	if v.A11 < 0 {
		v.A11 = -v.A11
	}
	if v.A02 < 0 {
		v.A02 = -v.A02
	}
	if v.A12 < 0 {
		v.A12 = -v.A12
	}

	return v
}

func (v *Mat2x3) PAbs() {
	if v.A00 < 0 {
		v.A00 = -v.A00
	}
	if v.A10 < 0 {
		v.A10 = -v.A10
	}
	if v.A01 < 0 {
		v.A01 = -v.A01
	}
	if v.A11 < 0 {
		v.A11 = -v.A11
	}
	if v.A02 < 0 {
		v.A02 = -v.A02
	}
	if v.A12 < 0 {
		v.A12 = -v.A12
	}

}

func (v Mat2x3) Mul1(o Vec3) Vec2 {
	return Vec2{
		v.A00*o.X + v.A01*o.Y + v.A02*o.Z,
		v.A10*o.X + v.A11*o.Y + v.A12*o.Z,
	}
}

func (v Mat2x3) Mul2(o Mat3x2) Mat2 {
	return Mat2{
		v.A00*o.A00 + v.A01*o.A10 + v.A02*o.A20,
		v.A10*o.A00 + v.A11*o.A10 + v.A12*o.A20,
		v.A00*o.A01 + v.A01*o.A11 + v.A02*o.A21,
		v.A10*o.A01 + v.A11*o.A11 + v.A12*o.A21,
	}
}

func (v Mat2x3) Mul3(o Mat3) Mat2x3 {
	return Mat2x3{
		v.A00*o.A00 + v.A01*o.A10 + v.A02*o.A20,
		v.A10*o.A00 + v.A11*o.A10 + v.A12*o.A20,
		v.A00*o.A01 + v.A01*o.A11 + v.A02*o.A21,
		v.A10*o.A01 + v.A11*o.A11 + v.A12*o.A21,
		v.A00*o.A02 + v.A01*o.A12 + v.A02*o.A22,
		v.A10*o.A02 + v.A11*o.A12 + v.A12*o.A22,
	}
}

func (v Mat2x3) Mul4(o Mat3x4) Mat2x4 {
	return Mat2x4{
		v.A00*o.A00 + v.A01*o.A10 + v.A02*o.A20,
		v.A10*o.A00 + v.A11*o.A10 + v.A12*o.A20,
		v.A00*o.A01 + v.A01*o.A11 + v.A02*o.A21,
		v.A10*o.A01 + v.A11*o.A11 + v.A12*o.A21,
		v.A00*o.A02 + v.A01*o.A12 + v.A02*o.A22,
		v.A10*o.A02 + v.A11*o.A12 + v.A12*o.A22,
		v.A00*o.A03 + v.A01*o.A13 + v.A02*o.A23,
		v.A10*o.A03 + v.A11*o.A13 + v.A12*o.A23,
	}
}

func (v Mat2x3) Int64() mathi64.Mat2x3 {
	return mathi64.Mat2x3{
		int64(v.A00),
		int64(v.A10),
		int64(v.A01),
		int64(v.A11),
		int64(v.A02),
		int64(v.A12),
	}
}

func Mat2x3FromInt64(v mathi64.Mat2x3) Mat2x3 {
	return Mat2x3{
		int(v.A00),
		int(v.A10),
		int(v.A01),
		int(v.A11),
		int(v.A02),
		int(v.A12),
	}
}

func (v Mat2x3) Float32() math32.Mat2x3 {
	return math32.Mat2x3{
		float32(v.A00),
		float32(v.A10),
		float32(v.A01),
		float32(v.A11),
		float32(v.A02),
		float32(v.A12),
	}
}

func Mat2x3FromFloat32(v math32.Mat2x3) Mat2x3 {
	return Mat2x3{
		int(v.A00),
		int(v.A10),
		int(v.A01),
		int(v.A11),
		int(v.A02),
		int(v.A12),
	}
}

func (v Mat2x3) Float64() math64.Mat2x3 {
	return math64.Mat2x3{
		float64(v.A00),
		float64(v.A10),
		float64(v.A01),
		float64(v.A11),
		float64(v.A02),
		float64(v.A12),
	}
}

func Mat2x3FromFloat64(v math64.Mat2x3) Mat2x3 {
	return Mat2x3{
		int(v.A00),
		int(v.A10),
		int(v.A01),
		int(v.A11),
		int(v.A02),
		int(v.A12),
	}
}
